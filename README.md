[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_eps-firmware&metric=coverage)](https://sonarcloud.io/summary/new_code?id=sat-polsl_eps-firmware)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_eps-firmware&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=sat-polsl_eps-firmware)
[![Pipeline](https://gitlab.com/sat-polsl/eps/eps-firmware/badges/main/pipeline.svg)](https://gitlab.com/sat-polsl/eps/eps-firmware/-/pipelines?page=1&scope=all&ref=main)

# EPS-firmware

# Table of contents

- [Prerequisites](#prerequisites)
- [Setting up the project](#setting-up-the-project)
- [Reporitng an Issue](#reporting-an-issue)
- [Working with repsitory](#working-with-repository)
    - [Contribution flow](#contribution-flow)
    - [Commit message format](#commit-message-format)
- [Coding style](#coding-style)
- [EPS FRAM Memory](#eps-fram-memory)
    - [FRAM Content](#fram-content)
    - [Firmware slot](#firmware-slot)
    - [Programming](#programming)
- [Known issues](#known-issues)
- [Content overview](#contents-overview)
- [Useful links](#useful-links)

## Prerequisites

* Git
* Make/Ninja
* CMake
* STLink
* OpenOCD (at least 0.11)
* Python
* Clang-Format

### Windows

* Use Ninja

## Setting up the project

Clone the repo with `git clone https://gitlab.com/sat-polsl/eps/eps-firmware.git`.

In project's root directory:

1. Create `CMakeUserPresets.json` as described
   in [Wiki](https://sat-polsl.gitlab.io/wiki/sat-firmware-launcher-wiki/#/CMake-Presets) with
   required [Cache Variables](#cache-variables). 
   `CMakeUserPresets.json.template` can be used as template.
2. Configure project: `cmake --preset <user-defined-preset>`.
3. Build and test project with CMake Presets or manually:
    1. CMake Presets:
        2. Run `cmake --build --preset <debug|release>` to build all.
        3. Run: `ctest --preset <debug/test|release/test>` to run tests.
    2. Manually:
        1. Enter `<build-dir>` and run:
        2. Run `ninja` to build all.
        3. Run `ctest` to run tests.

### Cache Variables

| Variable           | Required | Description                                                                            |
|--------------------|:--------:|----------------------------------------------------------------------------------------| 
| `CPM_SOURCE_CACHE` |    No    | Path to [CPM](https://sat-polsl.gitlab.io/wiki/sat-firmware-launcher-wiki/#/CPM) cache |

## Reporting an issue

Bugs are tracked with [GitLab Issue Board](https://gitlab.com/sat-polsl/eps/eps-firmware/-/boards)

Explain the problem and include additional details to help maintainers reproduce the problem.

## Working with repository

### Contribution flow

1. Branch out from `main` branch and name your branch `eps<issue_number>-<shortdescription>`.
2. Make commits to your branch.
3. Make sure to pass all tests and add new if needed.
4. Push your work to remote branch
5. Submit a Merge Request to `main` branch

### Commit message format

Use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)

## Coding style

Use `snake_case` for everything except template arguments which are named with `CamelCase`

## EPS FRAM memory

### FRAM Content

| Field                | Offset | Length | Description                                         |
|----------------------|--------|--------|-----------------------------------------------------|
| Boot counter         | 0      | 4      | Boot counter written after successful firmware load |
| Boot attempt counter | 4      | 4      | Attempted boots counter                             |
| #1 Firmware slot     | 2048   | 522240 | Firmware slot                                       |

### Firmware slot

| Field    | Offset | Length | Description             |
|----------|--------|--------|-------------------------|
| Magic A  | 0      | 4      | First magic number      |
| Magic B  | 4      | 4      | Second magic number     |
| CRC-32   | 8      | 4      | CRC-32 of Firmware data |
| Length   | 12     | 4      | Length of Firmware data |
| Firmware | 16     | Length | Firmware data           | 

### Programming

[Argon board](https://github.com/alicjamusial/argon) with [Argon driver](https://github.com/alicjamusial/argon-driver)
is used for programming EPS FRAM.

1. Create slot image: `ninja firmware_image.slot` will create slot image in `<build>/firmware/eps/firmware_image.slot`
2. Program FRAM: `argon_cli --serial <serial> write -s <slot address> -f firmware_image.slot`

## Known issues

1. Linux:
    * If your package manager does not have OpenOCD 0.11, you have to download the newest release
      from [git](https://github.com/openocd-org/openocd/releases).
2. MacOS:
    * To run tests on your host, you have to
      compile [QEMU](https://gitlab.com/sat-polsl/software/qemu/-/tree/sat-master) manually or let us know how to
      crosscompile it on CI

## Contents overview

The files inside the project are presented as follows:

* **cmake**: directory with CMake scripts, functions and macros.
* **docs**: directory with documentation.
* **include**: header files.
* **libs/external**: external libraries,
* **firmware**: firmware source files.
* **test**: directory with test project.
* **.clang-format**: config for code formatting.
* **.gdbinit**: debugger automatic setup.
* **.gitignore**: files to ignore by the version system.
* **.gitlab-ci.yml**: build config for GitLab CI pipelines.
* **.sonar.properties**: Sonarcloud config.
* **CMakeLists.txt**: main CMake build system script.

## Useful links

- [Doxygen](https://sat-polsl.gitlab.io/eps/eps-firmware/)
