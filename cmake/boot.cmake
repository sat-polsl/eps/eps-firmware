function(target_boot_slot TARGET)
    set(TARGET_FILE_DIR $<TARGET_FILE_DIR:${TARGET}>)
    set(TARGET_FILE_NAME $<TARGET_FILE_BASE_NAME:${TARGET}>)
    set(TARGET_ELF_FILE ${TARGET_FILE_DIR}/${TARGET_FILE_NAME}.elf)
    set(TARGET_BIN_FILE ${TARGET_FILE_DIR}/${TARGET_FILE_NAME}.bin)
    set(TARGET_SLOT_FILE ${TARGET_FILE_DIR}/${TARGET_FILE_NAME}.slot)
    add_custom_target(${TARGET}.slot
        COMMAND ${CMAKE_OBJCOPY} -O binary ${TARGET_ELF_FILE} ${TARGET_BIN_FILE}
        COMMAND ${PYTHON} ${CMAKE_SOURCE_DIR}/tools/create_slot.py ${TARGET_BIN_FILE} ${TARGET_SLOT_FILE}
        DEPENDS ${TARGET}
        )
endfunction()

function(add_boot_image TARGET TARGET_IMAGE)
    set(OPTIONS)
    set(ONE_VALUE)
    set(MULTI_VALUE LDS)
    cmake_parse_arguments(PARSE_ARGV 2 ADD_BOOT_IMAGE "${OPTIONS}" "${ONE_VALUE}" "${MULTI_VALUE}")

    message(${ADD_BOOT_IMAGE_LDS})

    list(TRANSFORM ADD_BOOT_IMAGE_LDS PREPEND "-T")

    get_target_property(TARGET_SOURCES ${TARGET} SOURCES)
    get_target_property(TARGET_INCLUDE_DIRS ${TARGET} INCLUDE_DIRECTORIES)
    get_target_property(TARGET_LIBS ${TARGET} LINK_LIBRARIES)

    add_executable(${TARGET_IMAGE})
    target_sources(${TARGET_IMAGE} PRIVATE ${TARGET_SOURCES})
    target_include_directories(${TARGET_IMAGE} PUBLIC ${TARGET_INCLUDE_DIRS})
    target_link_libraries(${TARGET_IMAGE} PRIVATE ${TARGET_LIBS})
    target_link_options(${TARGET_IMAGE}
        PRIVATE
        ${ADD_BOOT_IMAGE_LDS}
        -specs=nano.specs
        LINKER:-print-memory-usage # Print RAM and ROM usage
        LINKER:-Map=${TARGET_IMAGE}.map # Generate a memory map
        )
endfunction()
