#pragma once
#include "bootloader/flash_loader/flash_loader.h"
#include "devices.h"
#include "drivers.h"
#include "peripherals.h"
#include "satext/noncopyable.h"

namespace bootloader {
/**
 * @addtogroup bootloader
 * @{
 */

/**
 * @brief Bootloader application class
 * Boot process:
 * 1. Initialize peripherals.
 * 2. If application won't be started within 30 seconds, MCU will reset due to watchdog.
 * 3. Read boot attempt counter from FRAM, increment it, write back to FRAM.
 * 4. Try to load image from FRAM.
 *     - Read boot slot info from FRAM and verify magic numbers, stop image load attempt if values
 * are mismatched.
 *     - On successful verification do:
 *         - erase FLASH page.
 *         - write that page with data from FRAM.
 *         - proceed with programing FLASH until whole length (written in boot slot info) of
 * Application program is flashed.
 *     - if flash procedure fails, stop boot process.
 *     - after successfully programing FLASH, check CRC of program written in FLASH.
 * 5. Read boot counter from FRAM, increment it, write back to FRAM.
 * 6. Deinitialize peripherals.
 * 7. Jump to Application.
 */
class application : private satext::noncopyable {
public:
    application() = default;
    [[noreturn]] void run();

    peripherals::peripherals& peripherals();
    drivers::drivers& drivers();
    devices::devices& devices();

private:
    /**
     * @brief Boots application
     */
    void boot() const;

    /**
     * @brief Reads boot counter from FRAM, increments it, and writes it back
     */
    void increment_boot_counter();

    /**
     * @brief Reads boot attempt counter from FRAM, increments it, and writes it back
     */
    void increment_boot_attempt_counter();

    /**
     * @brief Offset of application address relative to FLASH memory base
     */
    static constexpr std::uint32_t app_address_offset = 32_KiB;

    /**
     * @brief Absolute address of application address
     */
    static constexpr std::uint32_t app_address =
        spl::peripherals::flash::flash::memory_base + app_address_offset;

    peripherals::peripherals peripherals_{};
    drivers::drivers drivers_{peripherals_};
    devices::devices devices_{peripherals_, drivers_};

    flash_loader::flash_loader<spl::peripherals::flash::flash,
                               decltype(devices_.fram()),
                               spl::peripherals::crc::crc,
                               app_address_offset>
        flash_loader_{peripherals_.flash(), devices_.fram(), peripherals_.crc()};
};

inline application bootloader;

/** @} */
} // namespace bootloader
