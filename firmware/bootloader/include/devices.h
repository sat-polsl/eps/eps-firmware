#pragma once
#include "drivers.h"
#include "peripherals.h"
#include "satext/noncopyable.h"

#include "devices/fram/fram.h"

namespace bootloader::devices {
class devices : private satext::noncopyable {
public:
    using fram_type = ::devices::fram::fram<spl::peripherals::gpio::gpio,                   //
                                            spl::drivers::spi::spi_dma_lite_vectored::spi3, //
                                            ::devices::fram::memory_id::cy15b104q>;         //

    devices(bootloader::peripherals::peripherals& peripherals,
            bootloader::drivers::drivers& drivers);

    fram_type& fram();

private:
    fram_type fram_;
};
} // namespace bootloader::devices
