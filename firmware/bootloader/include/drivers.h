#pragma once
#include "peripherals.h"
#include "satext/noncopyable.h"
#include "spl/drivers/spi/spi.h"

namespace bootloader::drivers {
class drivers : private satext::noncopyable {
public:
    explicit drivers(bootloader::peripherals::peripherals& peripherals);

    void initialize();

    spl::drivers::spi::spi_dma_lite_vectored::spi3& spi3();

private:
    spl::drivers::spi::spi_dma_lite_vectored::spi3 spi3_;
};
} // namespace bootloader::drivers
