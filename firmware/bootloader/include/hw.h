#pragma once
#include "satext/units.h"
#include "spl/peripherals/gpio/gpio.h"

namespace hw {
struct led {
    static constexpr auto port = spl::peripherals::gpio::port::c;
    static constexpr auto pin = spl::peripherals::gpio::pin13;
};

struct spi3 {
    struct mosi {
        static constexpr auto port = spl::peripherals::gpio::port::c;
        static constexpr auto pin = spl::peripherals::gpio::pin12;
        static constexpr auto af = spl::peripherals::gpio::af6;
    };
    struct miso {
        static constexpr auto port = spl::peripherals::gpio::port::c;
        static constexpr auto pin = spl::peripherals::gpio::pin11;
        static constexpr auto af = spl::peripherals::gpio::af6;
    };
    struct clk {
        static constexpr auto port = spl::peripherals::gpio::port::c;
        static constexpr auto pin = spl::peripherals::gpio::pin10;
        static constexpr auto af = spl::peripherals::gpio::af6;
    };
};

struct fram {
    struct chip_select {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin15;
    };
    using spi = spi3;
};

constexpr std::uint32_t watchdog_period_ms = 30'000;

} // namespace hw
