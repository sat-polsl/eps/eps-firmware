#pragma once
#include "hw.h"
#include "satext/noncopyable.h"
#include "spl/peripherals/crc/crc.h"
#include "spl/peripherals/dma/dma.h"
#include "spl/peripherals/flash/flash.h"
#include "spl/peripherals/gpio/gpio.h"
#include "spl/peripherals/iwdg/iwdg.h"
#include "spl/peripherals/nvic/nvic.h"
#include "spl/peripherals/rcc/rcc.h"
#include "spl/peripherals/spi/spi.h"

namespace bootloader::peripherals {
class peripherals : private satext::noncopyable {
public:
    peripherals() = default;

    void initialize();
    void deinitialize() const;

    spl::peripherals::gpio::gpio& led();
    spl::peripherals::spi::spi& spi3();
    spl::peripherals::dma::dma& dma2();
    spl::peripherals::rcc::rcc& rcc();
    spl::peripherals::nvic::nvic& nvic();
    spl::peripherals::flash::flash& flash();
    spl::peripherals::crc::crc& crc();
    spl::peripherals::iwdg::iwdg& iwdg();

    spl::peripherals::gpio::gpio& fram_chip_select();

private:
    spl::peripherals::gpio::gpio led_{hw::led::port, hw::led::pin};
    spl::peripherals::gpio::gpio fram_chip_select_{hw::fram::chip_select::port,
                                                   hw::fram::chip_select::pin};

    spl::peripherals::gpio::gpio spi3_mosi{hw::spi3::mosi::port, hw::spi3::mosi::pin};
    spl::peripherals::gpio::gpio spi3_miso{hw::spi3::miso::port, hw::spi3::miso::pin};
    spl::peripherals::gpio::gpio spi3_clk{hw::spi3::clk::port, hw::spi3::clk::pin};

    spl::peripherals::spi::spi spi3_{spl::peripherals::spi::instance::spi3};
    spl::peripherals::dma::dma dma2_{spl::peripherals::dma::instance::dma2};

    spl::peripherals::crc::crc crc_;

    [[no_unique_address]] spl::peripherals::rcc::rcc rcc_;
    [[no_unique_address]] spl::peripherals::nvic::nvic nvic_;
    [[no_unique_address]] spl::peripherals::flash::flash flash_;
    [[no_unique_address]] spl::peripherals::iwdg::iwdg iwdg_;
};
} // namespace bootloader::peripherals
