#include "bootloader.h"
#include <array>
#include <cstdint>
#include "boot_parameters/boot_parameters.h"
#include "fram_map/fram_map.h"
#include "hw.h"
#include "libopencm3/cm3/scb.h"
#include "satext/struct.h"

using namespace satext::struct_literals;

namespace bootloader {
void application::run() {
    peripherals_.initialize();
    drivers_.initialize();

    increment_boot_attempt_counter();
    if (flash_loader_.load_slot(fram_map::boot_slots[0]) == flash_loader::status::ok) {
        increment_boot_counter();
        peripherals_.deinitialize();
        boot();
    }

    while (true) {}
}

void application::increment_boot_counter() {
    auto& fram = devices().fram();

    std::array<std::byte, 4> buffer{};

    fram.read(fram_map::boot_counter_offset, buffer);

    satext::unpack("<I"_fmt, buffer).map([&buffer](auto& result) {
        auto [boot_counter] = result;
        boot_counter++;
        boot_parameters::boot_parameters.boot_counter = boot_counter;
        satext::pack_to("<I"_fmt, buffer, boot_counter);
    });

    fram.write_enable();
    fram.write(fram_map::boot_counter_offset, buffer);
    fram.write_disable();
}

void application::increment_boot_attempt_counter() {
    auto& fram = devices().fram();

    std::array<std::byte, 4> buffer{};

    fram.read(fram_map::boot_attempt_counter_offset, buffer);

    satext::unpack("<I"_fmt, buffer).map([&buffer](auto& result) {
        auto [boot_counter] = result;
        boot_counter++;
        boot_parameters::boot_parameters.attempt_boot_counter = boot_counter;
        satext::pack_to("<I"_fmt, buffer, boot_counter);
    });

    fram.write_enable();
    fram.write(fram_map::boot_attempt_counter_offset, buffer);
    fram.write_disable();
}

void application::boot() const {
    SCB_VTOR = app_address;

    uint32_t sp = *((uint32_t*)app_address);
    uint32_t pc = *((uint32_t*)app_address + 1);

    asm volatile("ldr r0, %0\n"
                 "ldr r1, %1\n"
                 "MSR msp, r0\n"
                 "MSR psp, r0\n"
                 "mov pc, r1\n" ::"m"(sp),
                 "m"(pc));
}

peripherals::peripherals& application::peripherals() { return peripherals_; }

drivers::drivers& application::drivers() { return drivers_; }

devices::devices& application::devices() { return devices_; }

} // namespace bootloader
