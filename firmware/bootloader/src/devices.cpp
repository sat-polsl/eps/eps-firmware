#include "devices.h"

namespace bootloader::devices {
devices::devices(bootloader::peripherals::peripherals& peripherals,
                 bootloader::drivers::drivers& drivers) :
    fram_{drivers.spi3(), peripherals.fram_chip_select()} {}

devices::fram_type& devices::fram() { return fram_; }

} // namespace bootloader::devices
