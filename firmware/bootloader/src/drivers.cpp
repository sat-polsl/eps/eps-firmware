#include "drivers.h"

namespace bootloader::drivers {
drivers::drivers(bootloader::peripherals::peripherals& peripherals) :
    spi3_{peripherals.spi3(), peripherals.dma2(), peripherals.nvic()} {}

void drivers::initialize() { spi3_.initialize(); }

spl::drivers::spi::spi_dma_lite_vectored::spi3& drivers::spi3() { return spi3_; }

} // namespace bootloader::drivers
