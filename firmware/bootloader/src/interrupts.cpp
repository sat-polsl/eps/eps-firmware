#include "bootloader.h"

extern "C" void dma2_channel1_isr() { bootloader::bootloader.drivers().spi3().isr_rx_handler(); }

extern "C" void dma2_channel2_isr() { bootloader::bootloader.drivers().spi3().isr_tx_handler(); }
