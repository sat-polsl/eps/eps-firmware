#include "peripherals.h"

using namespace spl::peripherals;

namespace bootloader::peripherals {
void peripherals::initialize() {
    rcc_.enable_clock(rcc::peripheral::gpioc)
        .enable_clock(rcc::peripheral::gpioa)
        .enable_clock(rcc::peripheral::dma2)
        .enable_clock(rcc::peripheral::spi3)
        .enable_clock(rcc::peripheral::crc);

    crc_.reset();
    crc_.set_reverse_input(true);
    crc_.set_reverse_output(true);
    crc_.set_final_xor_value(0xffffffff);

    led_.setup(gpio::mode::output, gpio::pupd::pullup);

    fram_chip_select_.setup(gpio::mode::output, gpio::pupd::pullup);
    fram_chip_select_.set();

    spi3_miso.setup(gpio::mode::af, gpio::pupd::pullup, hw::spi3::miso::af);
    spi3_mosi.setup(gpio::mode::af, gpio::pupd::pullup, hw::spi3::mosi::af);
    spi3_clk.setup(gpio::mode::af, gpio::pupd::pullup, hw::spi3::clk::af);

    spi3_.set_clock_mode(spl::peripherals::spi::clock_mode::mode3);

    nvic_.set_priority(nvic::irq::dma2_channel1, 5);
    nvic_.set_priority(nvic::irq::dma2_channel2, 5);

    iwdg_.initialize();
    iwdg_.set_period_ms(hw::watchdog_period_ms);
}

void peripherals::deinitialize() const {
    rcc_.pulse_reset(rcc::peripheral::gpioc)
        .pulse_reset(rcc::peripheral::gpioa)
        .pulse_reset(rcc::peripheral::dma2)
        .pulse_reset(rcc::peripheral::spi3)
        .pulse_reset(rcc::peripheral::crc)
        .enable_clock(rcc::peripheral::gpioc)
        .enable_clock(rcc::peripheral::gpioa)
        .enable_clock(rcc::peripheral::dma2)
        .enable_clock(rcc::peripheral::spi3)
        .enable_clock(rcc::peripheral::crc);
}

spl::peripherals::gpio::gpio& peripherals::led() { return led_; }

spl::peripherals::rcc::rcc& peripherals::rcc() { return rcc_; }

spl::peripherals::nvic::nvic& peripherals::nvic() { return nvic_; }

spl::peripherals::spi::spi& peripherals::spi3() { return spi3_; }

spl::peripherals::dma::dma& peripherals::dma2() { return dma2_; }

spl::peripherals::gpio::gpio& peripherals::fram_chip_select() { return fram_chip_select_; }

spl::peripherals::flash::flash& peripherals::flash() { return flash_; }

spl::peripherals::crc::crc& peripherals::crc() { return crc_; }

spl::peripherals::iwdg::iwdg& peripherals::iwdg() { return iwdg_; }

} // namespace bootloader::peripherals
