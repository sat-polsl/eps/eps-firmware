#pragma once
#include "communication/decoder.h"
#include "communication/encoder.h"
#include "communication/message_handler.h"
#include "power_service/power_service.h"
#include "spl/peripherals/can/enums.h"
#include "telemetry.h"

#include "communication/can/decoders/echo_message_decoder.h"
#include "communication/can/decoders/get_telemetry_request_decoder.h"
#include "communication/can/decoders/power_output_off_decoder.h"
#include "communication/can/decoders/power_output_on_decoder.h"
#include "communication/can/decoders/power_watchdog_disable_decoder.h"
#include "communication/can/decoders/power_watchdog_enable_decoder.h"
#include "communication/can/decoders/power_watchdog_kick_decoder.h"
#include "communication/can/decoders/power_watchdog_set_timeout_decoder.h"

#include "communication/can/encoders/echo_message_encoder.h"
#include "communication/can/encoders/get_telemetry_response_encoder.h"
#include "communication/can/encoders/power_output_off_encoder.h"
#include "communication/can/encoders/power_output_on_encoder.h"
#include "communication/can/encoders/power_watchdog_disable_encoder.h"
#include "communication/can/encoders/power_watchdog_enable_encoder.h"
#include "communication/can/encoders/power_watchdog_kick_encoder.h"
#include "communication/can/encoders/power_watchdog_set_timeout_encoder.h"

#include "communication/can/handlers/echo_message_handler.h"
#include "communication/can/handlers/get_telemetry_handler.h"
#include "communication/can/handlers/power_output_off_handler.h"
#include "communication/can/handlers/power_output_on_handler.h"
#include "communication/can/handlers/power_watchdog_disable_handler.h"
#include "communication/can/handlers/power_watchdog_enable_handler.h"
#include "communication/can/handlers/power_watchdog_kick_handler.h"
#include "communication/can/handlers/power_watchdog_set_timeout_handler.h"

namespace eps::communication {

class communication {
public:
    using decoder =
        ::communication::decoder<spl::peripherals::can::message&,
                                 spl::peripherals::can::concepts::filter,
                                 ::communication::can::decoders::echo_message_decoder,
                                 ::communication::can::decoders::power_output_on_decoder,
                                 ::communication::can::decoders::power_output_off_decoder,
                                 ::communication::can::decoders::power_watchdog_enable_decoder,
                                 ::communication::can::decoders::power_watchdog_disable_decoder,
                                 ::communication::can::decoders::power_watchdog_set_timeout_decoder,
                                 ::communication::can::decoders::power_watchdog_kick_decoder,
                                 ::communication::can::decoders::get_telemetry_request_decoder>;
    using encoder =
        ::communication::encoder<spl::peripherals::can::message&,
                                 std::uint32_t,
                                 ::communication::can::encoders::echo_message_encoder,
                                 ::communication::can::encoders::power_output_on_encoder,
                                 ::communication::can::encoders::power_output_off_encoder,
                                 ::communication::can::encoders::power_watchdog_enable_encoder,
                                 ::communication::can::encoders::power_watchdog_disable_encoder,
                                 ::communication::can::encoders::power_watchdog_set_timeout_encoder,
                                 ::communication::can::encoders::power_watchdog_kick_encoder,
                                 ::communication::can::encoders::get_telemetry_response_encoder>;
    using message_handler = ::communication::message_handler<
        spl::peripherals::can::message&,
        decoder,
        encoder,
        ::communication::can::handlers::echo_message_handler,
        ::communication::can::handlers::power_output_on_handler<power_service::power_service>,
        ::communication::can::handlers::power_output_off_handler<power_service::power_service>,
        ::communication::can::handlers::power_watchdog_enable_handler<power_service::power_service>,
        ::communication::can::handlers::power_watchdog_disable_handler<
            power_service::power_service>,
        ::communication::can::handlers::power_watchdog_set_timeout_handler<
            power_service::power_service>,
        ::communication::can::handlers::power_watchdog_kick_handler<power_service::power_service>,
        ::communication::can::handlers::get_telemetry_handler<::telemetry::telemetry_state>>;

    communication(power_service::power_service& power_service, ::telemetry::telemetry_state& state);

    inline message_handler& handler() { return handler_; }

    satext::expected<spl::peripherals::can::message, ::communication::status>
    handle(const spl::peripherals::can::message& msg);

private:
    message_handler handler_;
};

/** @} */

} // namespace eps::communication
