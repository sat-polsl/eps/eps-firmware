#pragma once
#include "drivers.h"
#include "peripherals.h"
#include "satext/noncopyable.h"

#include "devices/adc/adc.h"
#include "devices/fram/fram.h"
#include "devices/lm75b/lm75b.h"
#include "devices/mcp73871/mcp73871.h"

namespace eps::devices {
class devices : private satext::noncopyable {
public:
    using lm75b_type = ::devices::lm75b::lm75b<spl::drivers::i2c::i2c::i2c1>;
    using bms_type = ::devices::mcp73871::mcp73871<spl::peripherals::gpio::gpio>;
    using adc_current_type = ::devices::adc::
        adc<spl::peripherals::gpio::gpio, spl::drivers::spi::spi_dma_vectored::spi2, 12, 12>;
    using adc_voltage_type = ::devices::adc::
        adc<spl::peripherals::gpio::gpio, spl::drivers::spi::spi_dma_vectored::spi2, 7, 8>;
    using fram_type = ::devices::fram::fram<spl::peripherals::gpio::gpio,
                                            spl::drivers::spi::spi_dma_vectored::spi3,
                                            ::devices::fram::memory_id::cy15b104q>;

    devices(eps::peripherals::peripherals& peripherals, eps::drivers::drivers& drivers);

    void initialize();

    lm75b_type& lm75b_3v3();
    lm75b_type& lm75b_5v();
    lm75b_type& lm75b_12v();
    lm75b_type& lm75b_cell1();
    lm75b_type& lm75b_cell2();
    lm75b_type& lm75b_cell3();

    bms_type& bms1();
    bms_type& bms2();
    bms_type& bms3();

    adc_current_type& adc_current();
    adc_voltage_type& adc_voltage();

    fram_type& fram();

private:
    lm75b_type lm75b_3v3_;
    lm75b_type lm75b_5v_;
    lm75b_type lm75b_12v_;
    lm75b_type lm75b_cell1_;
    lm75b_type lm75b_cell2_;
    lm75b_type lm75b_cell3_;

    bms_type bms1_;
    bms_type bms2_;
    bms_type bms3_;

    adc_current_type adc_current_;
    adc_voltage_type adc_voltage_;

    fram_type fram_;
};
} // namespace eps::devices
