#pragma once
#include "peripherals.h"
#include "satext/noncopyable.h"
#include "spl/drivers/can/can.h"
#include "spl/drivers/dma/dma.h"
#include "spl/drivers/i2c/i2c.h"
#include "spl/drivers/spi/spi.h"
#include "spl/drivers/uart/uart.h"

namespace eps::drivers {
class drivers : private satext::noncopyable {
public:
    explicit drivers(eps::peripherals::peripherals& peripherals);

    void initialize();
    void initialize_rtos_context();

    spl::drivers::uart::uart3& terminal_uart();
    spl::drivers::i2c::i2c::i2c1& i2c1();
    spl::drivers::spi::spi_dma_vectored::spi2& spi2();
    spl::drivers::spi::spi_dma_vectored::spi3& spi3();
    spl::drivers::can::can1& can();
    spl::drivers::dma::dma& dma1();
    spl::drivers::dma::dma& dma2();

private:
    spl::drivers::dma::dma dma1_;
    spl::drivers::dma::dma dma2_;
    spl::drivers::uart::uart3 terminal_uart_;
    spl::drivers::i2c::i2c::i2c1 i2c1_;
    spl::drivers::spi::spi_dma_vectored::spi2 spi2_;
    spl::drivers::spi::spi_dma_vectored::spi3 spi3_;
    spl::drivers::can::can1 can_;
};
} // namespace eps::drivers
