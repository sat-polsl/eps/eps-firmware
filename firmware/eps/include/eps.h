#pragma once
#include "communication.h"
#include "devices.h"
#include "drivers.h"
#include "fram_map/fram_map.h"
#include "peripherals.h"
#include "power_configuration/power_configuration.h"
#include "power_controller/power_controller.h"
#include "power_service/power_service.h"
#include "satext/noncopyable.h"
#include "satext/units.h"
#include "satos/thread.h"
#include "telemetry.h"
#include "terminal/terminal.h"
#include "terminal/threads/terminal.h"
#include "threads/can_service.h"

namespace eps {
class application;
using application_base = satos::thread<application, satos::thread_priority::normal_0, 2_KiB, "eps">;

class application : private satext::noncopyable, public application_base {
    friend application_base;

public:
    application() = default;
    [[noreturn]] void run();

    peripherals::peripherals& peripherals();
    drivers::drivers& drivers();
    devices::devices& devices();
    telemetry::telemetry& telemetry();

    auto& terminal();
    communication::communication& communication();
    power_service::power_service& power_service();

private:
    [[noreturn]] void thread_function();

    peripherals::peripherals peripherals_{};
    drivers::drivers drivers_{peripherals_};
    devices::devices devices_{peripherals_, drivers_};
    telemetry::telemetry telemetry_{devices_};

    using terminal_uart = decltype(drivers_.terminal_uart());
    std::array<char, 128> terminal_buffer_{};
    std::array<std::string_view, 10> terminal_tokens_{};
    terminal::terminal<terminal_uart> terminal_{
        drivers_.terminal_uart(), terminal_buffer_, terminal_tokens_, "eps>", "EPS terminal"};

    power_configuration::storage power_configuration_storage_{
        [this](std::size_t offset, std::span<std::byte> buffer) {
            return devices_.fram()
                .read(fram_map::power_configuration_offset + offset, buffer)
                .has_value();
        },
        [this](std::size_t offset, std::span<const std::byte> buffer) {
            return devices_.fram().write(fram_map::power_configuration_offset + offset, buffer);
        }};
    power_configuration::power_configuration power_configuration_{power_configuration_storage_,
                                                                  peripherals_.crc()};

    power_controller::configuration power_controller_configuration_{
        .bus_3v3 = peripherals_.enable_3v3(),
        .bus_5v = peripherals_.enable_5v(),
        .bus_12v = peripherals_.enable_12v(),
        .v3v3_1 = peripherals_.enable_3v3_l1(),
        .v3v3_2 = peripherals_.enable_3v3_l2(),
        .v3v3_3 = peripherals_.enable_3v3_l3(),
        .v3v3_4 = peripherals_.enable_3v3_l4(),
        .v5_1 = peripherals_.enable_5v_l1(),
        .v5_2 = peripherals_.enable_5v_l2(),
        .v12 = peripherals_.enable_12v_l1(),
        .heater = peripherals_.heater()};
    power_controller::power_controller power_controller_{
        power_controller_configuration_, telemetry_.state(), eps::telemetry::id::output_state};
    power_service::power_service power_service_{power_controller_};

    communication::communication communication_{power_service_, telemetry_.state()};

    terminal::threads::terminal<terminal_uart> terminal_thread_{terminal_};
    threads::can_service can_service_{drivers_.can(), communication_};
};

inline auto& application::terminal() { return terminal_; }

inline application eps;

} // namespace eps
