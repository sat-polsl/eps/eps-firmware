#pragma once
#include "satext/units.h"
#include "spl/peripherals/can/enums.h"
#include "spl/peripherals/can/types.h"
#include "spl/peripherals/gpio/gpio.h"
#include "spl/peripherals/rcc/enums.h"
#include "spl/peripherals/sysclk/configuration.h"

namespace hw {
struct led {
    static constexpr auto port = spl::peripherals::gpio::port::c;
    static constexpr auto pin = spl::peripherals::gpio::pin13;
};

struct terminal_uart {
    static constexpr auto tx_port = spl::peripherals::gpio::port::c;
    static constexpr auto tx_pin = spl::peripherals::gpio::pin10;
    static constexpr auto rx_port = spl::peripherals::gpio::port::c;
    static constexpr auto rx_pin = spl::peripherals::gpio::pin11;
    static constexpr std::uint32_t baudrate = 115200;
};

struct i2c1 {
    static constexpr auto sda_port = spl::peripherals::gpio::port::b;
    static constexpr auto sda_pin = spl::peripherals::gpio::pin9;
    static constexpr auto scl_port = spl::peripherals::gpio::port::b;
    static constexpr auto scl_pin = spl::peripherals::gpio::pin8;
};

struct i2c_addresses {
    static constexpr auto temperature_3v3 = 0x4b;
    static constexpr auto temperature_5v = 0x4c;
    static constexpr auto temperature_12v = 0x4d;
    static constexpr auto temperature_cell1 = 0x48;
    static constexpr auto temperature_cell2 = 0x49;
    static constexpr auto temperature_cell3 = 0x4a;
};

struct spi2 {
    static constexpr auto port = spl::peripherals::gpio::port::b;
    static constexpr auto miso_pin = spl::peripherals::gpio::pin14;
    static constexpr auto mosi_pin = spl::peripherals::gpio::pin15;
    static constexpr auto sck_pin = spl::peripherals::gpio::pin13;
};

struct spi3 {
    struct mosi {
        static constexpr auto port = spl::peripherals::gpio::port::c;
        static constexpr auto pin = spl::peripherals::gpio::pin12;
        static constexpr auto af = spl::peripherals::gpio::af6;
    };
    struct miso {
        static constexpr auto port = spl::peripherals::gpio::port::c;
        static constexpr auto pin = spl::peripherals::gpio::pin11;
        static constexpr auto af = spl::peripherals::gpio::af6;
    };
    struct clk {
        static constexpr auto port = spl::peripherals::gpio::port::c;
        static constexpr auto pin = spl::peripherals::gpio::pin10;
        static constexpr auto af = spl::peripherals::gpio::af6;
    };
};

struct fram {
    struct chip_select {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin15;
    };
    using spi = spi3;
};

struct enable_3v3 {
    static constexpr auto port = spl::peripherals::gpio::port::a;
    static constexpr auto pin = spl::peripherals::gpio::pin0;
};

struct enable_3v3_l1 {
    static constexpr auto port = spl::peripherals::gpio::port::b;
    static constexpr auto pin = spl::peripherals::gpio::pin6;
};

struct enable_3v3_l2 {
    static constexpr auto port = spl::peripherals::gpio::port::b;
    static constexpr auto pin = spl::peripherals::gpio::pin5;
};

struct enable_3v3_l3 {
    static constexpr auto port = spl::peripherals::gpio::port::b;
    static constexpr auto pin = spl::peripherals::gpio::pin4;
};

struct enable_3v3_l4 {
    static constexpr auto port = spl::peripherals::gpio::port::b;
    static constexpr auto pin = spl::peripherals::gpio::pin7;
};

struct enable_5v {
    static constexpr auto port = spl::peripherals::gpio::port::c;
    static constexpr auto pin = spl::peripherals::gpio::pin2;
};

struct mode_5v {
    static constexpr auto port = spl::peripherals::gpio::port::c;
    static constexpr auto pin = spl::peripherals::gpio::pin3;
};

struct enable_5v_l1 {
    static constexpr auto port = spl::peripherals::gpio::port::c;
    static constexpr auto pin = spl::peripherals::gpio::pin1;
};

struct enable_5v_l2 {
    static constexpr auto port = spl::peripherals::gpio::port::c;
    static constexpr auto pin = spl::peripherals::gpio::pin0;
};

struct enable_12v {
    static constexpr auto port = spl::peripherals::gpio::port::a;
    static constexpr auto pin = spl::peripherals::gpio::pin3;
};

struct enable_12v_l1 {
    static constexpr auto port = spl::peripherals::gpio::port::a;
    static constexpr auto pin = spl::peripherals::gpio::pin2;
};

struct heater {
    static constexpr auto port = spl::peripherals::gpio::port::a;
    static constexpr auto pin = spl::peripherals::gpio::pin1;
};

struct can {
    static constexpr auto tx_port = spl::peripherals::gpio::port::a;
    static constexpr auto tx_pin = spl::peripherals::gpio::pin12;
    static constexpr auto tx_af = spl::peripherals::gpio::af9;
    static constexpr auto rx_port = spl::peripherals::gpio::port::a;
    static constexpr auto rx_pin = spl::peripherals::gpio::pin11;
    static constexpr auto rx_af = spl::peripherals::gpio::af9;
    static constexpr auto timings =
        spl::peripherals::can::timings{.sjw = 1, .ts1 = 8, .ts2 = 1, .brp = 5};
};

struct adc_current {
    struct select {
        static constexpr auto port = spl::peripherals::gpio::port::c;
        static constexpr auto pin = spl::peripherals::gpio::pin7;
    };
};

struct adc_voltage {
    struct select {
        static constexpr auto port = spl::peripherals::gpio::port::c;
        static constexpr auto pin = spl::peripherals::gpio::pin6;
    };
};

struct bms1 {
    struct charge_enable {
        static constexpr auto port = spl::peripherals::gpio::port::b;
        static constexpr auto pin = spl::peripherals::gpio::pin2;
    };
    struct power_good {
        static constexpr auto port = spl::peripherals::gpio::port::b;
        static constexpr auto pin = spl::peripherals::gpio::pin10;
    };
    struct stat1 {
        static constexpr auto port = spl::peripherals::gpio::port::c;
        static constexpr auto pin = spl::peripherals::gpio::pin9;
    };
    struct stat2 {
        static constexpr auto port = spl::peripherals::gpio::port::b;
        static constexpr auto pin = spl::peripherals::gpio::pin11;
    };
};

struct bms2 {
    struct charge_enable {
        static constexpr auto port = spl::peripherals::gpio::port::c;
        static constexpr auto pin = spl::peripherals::gpio::pin4;
    };
    struct power_good {
        static constexpr auto port = spl::peripherals::gpio::port::c;
        static constexpr auto pin = spl::peripherals::gpio::pin5;
    };
    struct stat1 {
        static constexpr auto port = spl::peripherals::gpio::port::b;
        static constexpr auto pin = spl::peripherals::gpio::pin1;
    };
    struct stat2 {
        static constexpr auto port = spl::peripherals::gpio::port::b;
        static constexpr auto pin = spl::peripherals::gpio::pin0;
    };
};

struct bms3 {
    struct charge_enable {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin4;
    };
    struct power_good {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin5;
    };
    struct stat1 {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin7;
    };
    struct stat2 {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin6;
    };
};

static constexpr spl::peripherals::sysclk::configuration sysclk_cfg{
    .sysclk_src = spl::peripherals::rcc::clock_source::pll,
    .sysclk_hz = 50_MHz,
    .pll_clock_src = spl::peripherals::rcc::pll_clock_source::hse,
    .pll_input_clock_hz = 16_MHz,
    .pll_input_prescaler = spl::peripherals::rcc::pll_input_prescaler::div4,
    .pll_vco_multiplier = 50,
    .pll_output_prescaler = spl::peripherals::rcc::pll_prescaler::div4};

constexpr auto sysclock = sysclk_cfg.sysclk_hz;
constexpr auto kernel_freq = 100_Hz;
constexpr std::uint32_t watchdog_period_ms = 2000;
} // namespace hw
