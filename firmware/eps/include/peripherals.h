#pragma once
#include "hw.h"
#include "satext/noncopyable.h"
#include "spl/peripherals/can/can.h"
#include "spl/peripherals/crc/crc.h"
#include "spl/peripherals/dma/dma.h"
#include "spl/peripherals/flash/flash.h"
#include "spl/peripherals/gpio/gpio.h"
#include "spl/peripherals/i2c/i2c.h"
#include "spl/peripherals/iwdg/iwdg.h"
#include "spl/peripherals/nvic/nvic.h"
#include "spl/peripherals/rcc/rcc.h"
#include "spl/peripherals/spi/spi.h"
#include "spl/peripherals/sysclk/sysclk.h"
#include "spl/peripherals/uart/uart.h"

namespace eps::peripherals {
class peripherals : private satext::noncopyable {
public:
    enum class pinmux { fram, terminal };

    peripherals() = default;

    void initialize();

    spl::peripherals::rcc::rcc& rcc();
    spl::peripherals::nvic::nvic& nvic();
    spl::peripherals::gpio::gpio& led();
    spl::peripherals::gpio::gpio& terminal_uart_tx_gpio();
    spl::peripherals::gpio::gpio& terminal_uart_rx_gpio();
    spl::peripherals::uart::uart& terminal_uart();
    spl::peripherals::i2c::i2c& i2c1();
    spl::peripherals::spi::spi& spi2();
    spl::peripherals::spi::spi& spi3();
    spl::peripherals::can::can& can();
    spl::peripherals::iwdg::iwdg& iwdg();
    spl::peripherals::flash::flash& flash();
    spl::peripherals::dma::dma& dma1();
    spl::peripherals::dma::dma& dma2();
    spl::peripherals::crc::crc& crc();

    spl::peripherals::gpio::gpio& enable_3v3();
    spl::peripherals::gpio::gpio& enable_5v();
    spl::peripherals::gpio::gpio& mode_5v();
    spl::peripherals::gpio::gpio& enable_12v();
    spl::peripherals::gpio::gpio& enable_3v3_l1();
    spl::peripherals::gpio::gpio& enable_3v3_l2();
    spl::peripherals::gpio::gpio& enable_3v3_l3();
    spl::peripherals::gpio::gpio& enable_3v3_l4();
    spl::peripherals::gpio::gpio& enable_5v_l1();
    spl::peripherals::gpio::gpio& enable_5v_l2();
    spl::peripherals::gpio::gpio& enable_12v_l1();
    spl::peripherals::gpio::gpio& heater();

    spl::peripherals::gpio::gpio& bms1_charge_enable();
    spl::peripherals::gpio::gpio& bms1_power_good();
    spl::peripherals::gpio::gpio& bms1_stat1();
    spl::peripherals::gpio::gpio& bms1_stat2();
    spl::peripherals::gpio::gpio& bms2_charge_enable();
    spl::peripherals::gpio::gpio& bms2_power_good();
    spl::peripherals::gpio::gpio& bms2_stat1();
    spl::peripherals::gpio::gpio& bms2_stat2();
    spl::peripherals::gpio::gpio& bms3_charge_enable();
    spl::peripherals::gpio::gpio& bms3_power_good();
    spl::peripherals::gpio::gpio& bms3_stat1();
    spl::peripherals::gpio::gpio& bms3_stat2();

    spl::peripherals::gpio::gpio& adc_current_select();
    spl::peripherals::gpio::gpio& adc_voltage_select();
    spl::peripherals::gpio::gpio& fram_select();

    void set_pinmux(pinmux mux) const;

private:
    void initialize_power_gpios() const;
    void initialize_bms_gpios() const;

    spl::peripherals::gpio::gpio led_{hw::led::port, hw::led::pin};
    spl::peripherals::gpio::gpio enable_3v3_{hw::enable_3v3::port, hw::enable_3v3::pin};
    spl::peripherals::gpio::gpio enable_5v_{hw::enable_5v::port, hw::enable_5v::pin};
    spl::peripherals::gpio::gpio enable_12v_{hw::enable_12v::port, hw::enable_12v::pin};
    spl::peripherals::gpio::gpio mode_5v_{hw::mode_5v::port, hw::mode_5v::pin};
    spl::peripherals::gpio::gpio enable_3v3_l1_{hw::enable_3v3_l1::port, hw::enable_3v3_l1::pin};
    spl::peripherals::gpio::gpio enable_3v3_l2_{hw::enable_3v3_l2::port, hw::enable_3v3_l2::pin};
    spl::peripherals::gpio::gpio enable_3v3_l3_{hw::enable_3v3_l3::port, hw::enable_3v3_l3::pin};
    spl::peripherals::gpio::gpio enable_3v3_l4_{hw::enable_3v3_l4::port, hw::enable_3v3_l4::pin};
    spl::peripherals::gpio::gpio enable_5v_l1_{hw::enable_5v_l1::port, hw::enable_5v_l1::pin};
    spl::peripherals::gpio::gpio enable_5v_l2_{hw::enable_5v_l2::port, hw::enable_5v_l2::pin};
    spl::peripherals::gpio::gpio enable_12v_l1_{hw::enable_12v_l1::port, hw::enable_12v_l1::pin};
    spl::peripherals::gpio::gpio heater_{hw::heater::port, hw::heater::pin};

    spl::peripherals::gpio::gpio bms1_charge_enable_{hw::bms1::charge_enable::port,
                                                     hw::bms1::charge_enable::pin};
    spl::peripherals::gpio::gpio bms1_power_good_{hw::bms1::power_good::port,
                                                  hw::bms1::power_good::pin};
    spl::peripherals::gpio::gpio bms1_stat1_{hw::bms1::stat1::port, hw::bms1::stat1::pin};
    spl::peripherals::gpio::gpio bms1_stat2_{hw::bms1::stat2::port, hw::bms1::stat2::pin};

    spl::peripherals::gpio::gpio bms2_charge_enable_{hw::bms2::charge_enable::port,
                                                     hw::bms2::charge_enable::pin};
    spl::peripherals::gpio::gpio bms2_power_good_{hw::bms2::power_good::port,
                                                  hw::bms2::power_good::pin};
    spl::peripherals::gpio::gpio bms2_stat1_{hw::bms2::stat1::port, hw::bms2::stat1::pin};
    spl::peripherals::gpio::gpio bms2_stat2_{hw::bms2::stat2::port, hw::bms2::stat2::pin};

    spl::peripherals::gpio::gpio bms3_charge_enable_{hw::bms3::charge_enable::port,
                                                     hw::bms3::charge_enable::pin};
    spl::peripherals::gpio::gpio bms3_power_good_{hw::bms3::power_good::port,
                                                  hw::bms3::power_good::pin};
    spl::peripherals::gpio::gpio bms3_stat1_{hw::bms3::stat1::port, hw::bms3::stat1::pin};
    spl::peripherals::gpio::gpio bms3_stat2_{hw::bms3::stat2::port, hw::bms3::stat2::pin};

    spl::peripherals::gpio::gpio terminal_uart_tx_gpio_{hw::terminal_uart::tx_port,
                                                        hw::terminal_uart::tx_pin};
    spl::peripherals::gpio::gpio terminal_uart_rx_gpio_{hw::terminal_uart::rx_port,
                                                        hw::terminal_uart::rx_pin};
    spl::peripherals::uart::uart terminal_uart_{spl::peripherals::uart::instance::uart3};

    spl::peripherals::gpio::gpio i2c1_sda_gpio_{hw::i2c1::sda_port, hw::i2c1::sda_pin};
    spl::peripherals::gpio::gpio i2c1_scl_gpio_{hw::i2c1::scl_port, hw::i2c1::scl_pin};
    spl::peripherals::i2c::i2c i2c1_{spl::peripherals::i2c::instance::i2c1};

    spl::peripherals::gpio::gpio spi2_miso_{hw::spi2::port, hw::spi2::miso_pin};
    spl::peripherals::gpio::gpio spi2_mosi_{hw::spi2::port, hw::spi2::mosi_pin};
    spl::peripherals::gpio::gpio spi2_sck_{hw::spi2::port, hw::spi2::sck_pin};
    spl::peripherals::spi::spi spi2_{spl::peripherals::spi::instance::spi2};

    spl::peripherals::gpio::gpio spi3_miso_{hw::spi3::miso::port, hw::spi3::miso::pin};
    spl::peripherals::gpio::gpio spi3_mosi_{hw::spi3::mosi::port, hw::spi3::mosi::pin};
    spl::peripherals::gpio::gpio spi3_sck_{hw::spi3::clk::port, hw::spi3::clk::pin};
    spl::peripherals::spi::spi spi3_{spl::peripherals::spi::instance::spi3};

    spl::peripherals::gpio::gpio fram_select_{hw::fram::chip_select::port,
                                              hw::fram::chip_select::pin};

    spl::peripherals::gpio::gpio adc_current_select_{hw::adc_current::select::port,
                                                     hw::adc_current::select::pin};
    spl::peripherals::gpio::gpio adc_voltage_select_{hw::adc_voltage::select::port,
                                                     hw::adc_voltage::select::pin};

    spl::peripherals::gpio::gpio can_tx_gpio_{hw::can::tx_port, hw::can::tx_pin};
    spl::peripherals::gpio::gpio can_rx_gpio_{hw::can::rx_port, hw::can::rx_pin};
    spl::peripherals::can::can can_{spl::peripherals::can::instance::can1};

    spl::peripherals::dma::dma dma1_{spl::peripherals::dma::instance::dma1};
    spl::peripherals::dma::dma dma2_{spl::peripherals::dma::instance::dma2};

    spl::peripherals::crc::crc crc_;

    [[no_unique_address]] spl::peripherals::rcc::rcc rcc_;
    [[no_unique_address]] spl::peripherals::nvic::nvic nvic_;
    [[no_unique_address]] spl::peripherals::iwdg::iwdg iwdg_;
    [[no_unique_address]] spl::peripherals::flash::flash flash_;

    spl::peripherals::sysclk::sysclk<decltype(rcc_), decltype(flash_)> sysclk_{rcc_, flash_};
};
} // namespace eps::peripherals
