#pragma once
#include <cstdint>
#include "devices.h"
#include "satext/units.h"
#include "satos/mutex.h"
#include "telemetry/samplers/adc_sampler.h"
#include "telemetry/samplers/temperature_sampler.h"
#include "telemetry/samplers/uptime_sampler.h"
#include "telemetry/telemetry_service.h"
#include "telemetry/telemetry_state.h"

namespace eps::telemetry {

struct id {
    static constexpr ::telemetry::telemetry_id boot_counter{0};
    static constexpr ::telemetry::telemetry_id uptime{1};
    static constexpr ::telemetry::telemetry_id temperature_3v3{2};
    static constexpr ::telemetry::telemetry_id temperature_5v{3};
    static constexpr ::telemetry::telemetry_id temperature_12v{4};
    static constexpr ::telemetry::telemetry_id temperature_cell1{5};
    static constexpr ::telemetry::telemetry_id temperature_cell2{6};
    static constexpr ::telemetry::telemetry_id temperature_cell3{7};
    static constexpr ::telemetry::telemetry_id v3v3_l1{8};
    static constexpr ::telemetry::telemetry_id i3v3_l1{9};
    static constexpr ::telemetry::telemetry_id v3v3_l2{10};
    static constexpr ::telemetry::telemetry_id i3v3_l2{11};
    static constexpr ::telemetry::telemetry_id v3v3_l3{12};
    static constexpr ::telemetry::telemetry_id i3v3_l3{13};
    static constexpr ::telemetry::telemetry_id v3v3_l4{14};
    static constexpr ::telemetry::telemetry_id i3v3_l4{15};
    static constexpr ::telemetry::telemetry_id v5v_l1{16};
    static constexpr ::telemetry::telemetry_id i5v_l1{17};
    static constexpr ::telemetry::telemetry_id v5v_l2{18};
    static constexpr ::telemetry::telemetry_id i5v_l2{19};
    static constexpr ::telemetry::telemetry_id v12v{20};
    static constexpr ::telemetry::telemetry_id i12v{21};
    static constexpr ::telemetry::telemetry_id vbms{22};
    static constexpr ::telemetry::telemetry_id ibms{23};
    static constexpr ::telemetry::telemetry_id vmppt{24};
    static constexpr ::telemetry::telemetry_id imppt1{25};
    static constexpr ::telemetry::telemetry_id imppt2{26};
    static constexpr ::telemetry::telemetry_id output_state{27};
};

using telemetry_state = ::telemetry::telemetry_state;
namespace samplers = ::telemetry::samplers;

class telemetry {
public:
    explicit telemetry(devices::devices& devices);
    void initialize();
    void start();

    telemetry_state& state();

private:
    devices::devices& devices_;

    template<typename T, ::telemetry::telemetry_id Id>
    using temperature_sampler = samplers::temperature_sampler<T, telemetry_state, Id>;

    [[no_unique_address]] samplers::uptime_sampler<telemetry_state, id::uptime> uptime_sampler_{};

    temperature_sampler<decltype(devices_.lm75b_3v3()), id::temperature_3v3>
        temperature_3v3_sampler_{devices_.lm75b_3v3()};

    temperature_sampler<decltype(devices_.lm75b_5v()), id::temperature_5v> temperature_5v_sampler_{
        devices_.lm75b_5v()};

    temperature_sampler<decltype(devices_.lm75b_12v()), id::temperature_12v>
        temperature_12v_sampler_{devices_.lm75b_12v()};

    temperature_sampler<decltype(devices_.lm75b_cell1()), id::temperature_cell1>
        temperature_cell1_sampler_{devices_.lm75b_cell1()};

    temperature_sampler<decltype(devices_.lm75b_cell2()), id::temperature_cell2>
        temperature_cell2_sampler_{devices_.lm75b_cell2()};

    temperature_sampler<decltype(devices_.lm75b_cell3()), id::temperature_cell3>
        temperature_cell3_sampler_{devices_.lm75b_cell3()};

    ::telemetry::samplers::adc_sampler<std::remove_reference_t<decltype(devices_.adc_voltage())>,
                                       std::remove_reference_t<decltype(devices_.adc_current())>,
                                       telemetry_state,
                                       ::telemetry::samplers::adc_sampler_config{
                                           .v3v3_l1 = id::v3v3_l1,
                                           .i3v3_l1 = id::i3v3_l1,
                                           .v3v3_l2 = id::v3v3_l2,
                                           .i3v3_l2 = id::i3v3_l2,
                                           .v3v3_l3 = id::v3v3_l3,
                                           .i3v3_l3 = id::i3v3_l3,
                                           .v3v3_l4 = id::v3v3_l4,
                                           .i3v3_l4 = id::i3v3_l4,
                                           .v5v_l1 = id::v5v_l1,
                                           .i5v_l1 = id::i5v_l1,
                                           .v5v_l2 = id::v5v_l2,
                                           .i5v_l2 = id::i5v_l2,
                                           .v12v = id::v12v,
                                           .i12v = id::i12v,
                                           .vbms = id::vbms,
                                           .ibms = id::ibms,
                                           .vmppt = id::vmppt,
                                           .imppt1 = id::imppt1,
                                           .imppt2 = id::imppt2}>
        adc_sampler_{devices_.adc_voltage(), devices_.adc_current()};

    ::telemetry::telemetry_value_buffer<40> telemetry_value_buffer_{};
    telemetry_state state_{telemetry_value_buffer_, {}};

    ::telemetry::telemetry_service<2_KiB,
                                   telemetry_state,
                                   decltype(uptime_sampler_),
                                   decltype(temperature_3v3_sampler_),
                                   decltype(temperature_5v_sampler_),
                                   decltype(temperature_12v_sampler_),
                                   decltype(temperature_cell1_sampler_),
                                   decltype(temperature_cell2_sampler_),
                                   decltype(temperature_cell3_sampler_),
                                   decltype(adc_sampler_)>
        telemetry_service_{state_};
};

} // namespace eps::telemetry
