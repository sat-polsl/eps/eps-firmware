#pragma once
#include "communication.h"
#include "satext/units.h"
#include "satos/thread.h"
#include "spl/drivers/can/can.h"

namespace eps::threads {
class can_service;
using can_service_base = satos::thread<can_service, satos::thread_priority::normal_0, 4_KiB, "can">;

class can_service : public can_service_base {
    friend can_service_base;

public:
    can_service(spl::drivers::can::can1& can, communication::communication& communication);

private:
    [[noreturn]] void thread_function();

    static constexpr auto timeout = satos::chrono::milliseconds{500};
    spl::drivers::can::can1& can_;
    communication::communication& communication_;
};
} // namespace eps::threads
