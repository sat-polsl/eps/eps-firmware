#include <array>
#include <span>
#include <tuple>
#include "converters/adc/current_adc.h"
#include "converters/adc/voltage_adc.h"
#include "eps.h"
#include "satext/charconv.h"
#include "satext/overload.h"
#include "satext/ranges/enumerate.h"
#include "satext/type_traits.h"
#include "terminal/terminal.h"

using namespace satos::chrono_literals;
using namespace std::literals::string_view_literals;
using namespace converters::adc;

void print_adc_reset_usage(auto& terminal) { terminal.print("usage: adc_reset <1|2>\n"); }

const terminal::command adc_reset("adc_reset", [](std::span<std::string_view> args) {
    auto& terminal = eps::eps.terminal();
    if (args.size() != 1) {
        print_adc_reset_usage(terminal);
        return;
    }

    std::uint8_t adc_index{};
    if (auto [_, e] = satext::from_chars(args[0], adc_index);
        e != std::errc{} && (adc_index == 1 || adc_index == 2)) {
        print_adc_reset_usage(terminal);
        return;
    }

    if (adc_index == 1) {
        eps::eps.devices().adc_current().reset();
    } else if (adc_index == 2) {
        eps::eps.devices().adc_voltage().reset();
    }
});

void print_adc_all_usage(auto& terminal) { terminal.print("usage: adc_mv <1|2>\n"); }

const terminal::command adc_mv("adc_mv", [](std::span<std::string_view> args) {
    auto& terminal = eps::eps.terminal();
    if (args.size() != 1) {
        print_adc_all_usage(terminal);
        return;
    }

    std::uint8_t adc_index{};
    if (auto [_, e] = satext::from_chars(args[0], adc_index);
        e != std::errc{} && (adc_index == 1 || adc_index == 2)) {
        print_adc_all_usage(terminal);
        return;
    }

    std::variant<
        std::monostate,
        std::reference_wrapper<std::remove_reference_t<decltype(eps::eps.devices().adc_current())>>,
        std::reference_wrapper<std::remove_reference_t<decltype(eps::eps.devices().adc_voltage())>>>
        adc{};
    if (adc_index == 1) {
        adc = eps::eps.devices().adc_current();
    } else if (adc_index == 2) {
        adc = eps::eps.devices().adc_voltage();
    }

    std::visit(
        satext::overload([](std::monostate) {},
                         [](auto& adc) {
                             adc.get()
                                 .read_all()
                                 .map_error([](auto status) {
                                     eps::eps.terminal().print("Status: {}\n",
                                                               satext::to_underlying_type(status));
                                 })
                                 .map([](auto result) {
                                     for (auto&& [i, v] : satext::ranges::enumerate(result)) {
                                         eps::eps.terminal().print("A{}: {}\n", i, v);
                                     }
                                 });
                         }),
        adc);
});

constexpr std::array<std::string_view, 7> voltage_channels{
    "12v"sv, "5v L1"sv, "3.3 L1"sv, "5v L2"sv, "3.3 L2"sv, "3.3 L3"sv, "3.3 L4"sv};

const terminal::command adc_voltage("adc_voltage", [](std::span<std::string_view>) {
    auto& adc = eps::eps.devices().adc_voltage();
    adc.read_all()
        .map_error([](auto status) {
            eps::eps.terminal().print("Status: {}\n", satext::to_underlying_type(status));
        })
        .map([](auto result) {
            auto converted = convert_voltage_adc(result);
            for (auto&& [i, v] : satext::ranges::enumerate(converted)) {
                eps::eps.terminal().print("{}: {} mV\n", voltage_channels[i], v);
            }
        });
});

constexpr std::array<std::tuple<std::string_view, std::string_view>, 12> current_channels{{
    {"BMS"sv, "mA"sv},
    {"MPPT2"sv, "mA"sv},
    {"MPPT1"sv, "mA"sv},
    {"BMS"sv, "mV"sv},
    {"MPPT"sv, "mV"sv},
    {"12v"sv, "mA"sv},
    {"5v L1"sv, "mA"sv},
    {"5v L2"sv, "mA"sv},
    {"3.3v L2"sv, "mA"sv},
    {"3.3v L1"sv, "mA"sv},
    {"3.3v L4"sv, "mA"sv},
    {"3.3v L3"sv, "mA"sv},
}};

const terminal::command adc_current("adc_current", [](std::span<std::string_view>) {
    auto& adc = eps::eps.devices().adc_current();
    adc.read_all()
        .map_error([](auto status) {
            eps::eps.terminal().print("Status: {}\n", satext::to_underlying_type(status));
        })
        .map([](auto result) {
            auto converted = convert_current_adc(result);
            for (auto&& [i, v] : satext::ranges::enumerate(converted)) {
                auto [channel, unit] = current_channels[i];
                eps::eps.terminal().print("{}: {} {}\n", channel, v, unit);
            }
        });
});
