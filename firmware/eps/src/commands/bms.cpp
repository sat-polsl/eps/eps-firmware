#include "eps.h"
#include "satext/charconv.h"
#include "terminal/terminal.h"

const terminal::command bms("bms", [](std::span<std::string_view>) {
    auto& terminal = eps::eps.terminal();
    auto& bms1 = eps::eps.devices().bms1();
    auto& bms2 = eps::eps.devices().bms2();
    auto& bms3 = eps::eps.devices().bms3();

    terminal.print("BMS1:\n");
    terminal.print("  PG: {}\n", bms1.power_good());
    terminal.print("  S1: {}\n", bms1.stat1());
    terminal.print("  S2: {}\n", bms1.stat2());
    terminal.print("BMS2:\n");
    terminal.print("  PG: {}\n", bms2.power_good());
    terminal.print("  S1: {}\n", bms2.stat1());
    terminal.print("  S2: {}\n", bms2.stat2());
    terminal.print("BMS3:\n");
    terminal.print("  PG: {}\n", bms3.power_good());
    terminal.print("  S1: {}\n", bms3.stat1());
    terminal.print("  S2: {}\n", bms3.stat2());
});

std::add_pointer_t<decltype(eps::eps.devices().bms1())> get_bms(std::uint8_t index) {
    if (index == 1) {
        return &eps::eps.devices().bms1();
    } else if (index == 2) {
        return &eps::eps.devices().bms2();
    } else if (index == 3) {
        return &eps::eps.devices().bms3();
    } else {
        return nullptr;
    }
}

const terminal::command bms_enable("bms_enable", [](std::span<std::string_view> args) {
    if (args.size() != 1) {
        eps::eps.terminal().print("usage: bms_enable <bms>\n");
        return;
    }

    std::uint8_t bms_index{};

    if (auto [_, e] = satext::from_chars(args[0], bms_index); e != std::errc{}) {
        eps::eps.terminal().print("usage: bms_enable <bms>\n");
        return;
    }

    auto* bms = get_bms(bms_index);

    if (bms == nullptr) {
        eps::eps.terminal().print("usage: bms_enable <bms>\n");
        return;
    }

    bms->charge_enable();
});

const terminal::command bms_disable("bms_disable", [](std::span<std::string_view> args) {
    if (args.size() != 1) {
        eps::eps.terminal().print("usage: bms_disable <bms>\n");
        return;
    }

    std::uint8_t bms_index{};

    if (auto [_, e] = satext::from_chars(args[0], bms_index); e != std::errc{}) {
        eps::eps.terminal().print("usage: bms_disable <bms>\n");
        return;
    }

    auto* bms = get_bms(bms_index);

    if (bms == nullptr) {
        eps::eps.terminal().print("usage: bms_disable <bms>\n");
        return;
    }

    bms->charge_disable();
});
