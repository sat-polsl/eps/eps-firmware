#include "boot_parameters/boot_parameters.h"
#include "eps.h"
#include "terminal/terminal.h"

const terminal::command boot_params("boot_params", [](std::span<std::string_view>) {
    eps::eps.terminal().print("Boot counter: {}\n", boot_parameters::boot_parameters.boot_counter);
    eps::eps.terminal().print("Attempt boot counter: {}\n",
                              boot_parameters::boot_parameters.attempt_boot_counter);
});
