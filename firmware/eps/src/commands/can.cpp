#include <charconv>
#include "eps.h"
#include "satext/struct.h"
#include "satext/type_traits.h"
#include "satos/chrono.h"
#include "terminal/terminal.h"

using namespace satos::chrono_literals;
using namespace satext::struct_literals;

static void print_can_write_usage(auto& terminal) {
    terminal.print("usage: can_write <hex id> <size> <hex data as u64>\n");
}

const terminal::command can_write("can_write", [](std::span<std::string_view> args) {
    auto& terminal = eps::eps.terminal();
    if (args.size() != 3) {
        print_can_write_usage(terminal);
        return;
    }

    std::uint32_t id{};
    std::uint8_t size{};
    std::uint64_t data{};
    if (auto [_, e] = std::from_chars(args[0].data(), args[0].data() + args[0].size(), id, 16);
        e != std::errc{}) {
        print_can_write_usage(terminal);
        return;
    }

    if (auto [_, e] = std::from_chars(args[1].data(), args[1].data() + args[1].size(), size);
        e != std::errc{}) {
        print_can_write_usage(terminal);
        return;
    }

    if (auto [_, e] = std::from_chars(args[2].data(), args[2].data() + args[2].size(), data, 16);
        e != std::errc{}) {
        print_can_write_usage(terminal);
        return;
    }

    auto msg = spl::peripherals::can::message{.id = id, .size = size, .is_extended = true};
    if (satext::pack_to("Q"_fmt, msg.data, data)) {
        if (eps::eps.drivers().can().write(msg, 1_s) == spl::drivers::can::status::timeout) {
            terminal.print("timeout\n");
        }
    } else {
        terminal.print("error\n");
    }
});

static void print_can_read_usage(auto& terminal) {
    terminal.print("usage: can_read <fifo> <timeout ms>\n");
}

const terminal::command can_read("can_read", [](std::span<std::string_view> args) {
    auto& terminal = eps::eps.terminal();
    if (args.size() != 2) {
        print_can_read_usage(terminal);
        return;
    }

    std::uint32_t fifo_number{};
    std::uint32_t timeout{};
    if (auto [_, e] = std::from_chars(args[0].data(), args[0].data() + args[0].size(), fifo_number);
        e != std::errc{}) {
        print_can_read_usage(terminal);
        return;
    }

    if (auto [_, e] = std::from_chars(args[1].data(), args[1].data() + args[1].size(), timeout);
        e != std::errc{}) {
        print_can_read_usage(terminal);
        return;
    }

    auto fifo =
        (fifo_number == 0) ? spl::peripherals::can::rx_fifo0 : spl::peripherals::can::rx_fifo1;
    eps::eps.drivers()
        .can()
        .read(fifo, satos::chrono::milliseconds(timeout))
        .map([&terminal](spl::peripherals::can::message msg) {
            terminal.print("Id: {}\n", msg.id);
            terminal.print("Filter: {}\n", satext::to_underlying_type(msg.matched_filter));
            for (std::uint8_t i = 0; i < msg.size; i++) {
                terminal.print("Data[{}]: {}\n", i, msg.data[i]);
            }
        })
        .map_error([&terminal](spl::drivers::can::status status) {
            terminal.print("Error: {}\n", satext::to_underlying_type(status));
        });
});

static void print_can_filter_usage(auto& terminal) {
    terminal.print("usage: can_filter <filter id> <hex id> <hex mask> <fifo>\n");
}

const terminal::command can_filter("can_filter", [](std::span<std::string_view> args) {
    auto& terminal = eps::eps.terminal();
    if (args.size() != 4) {
        print_can_filter_usage(terminal);
        return;
    }

    std::uint8_t filter{};
    std::uint32_t id{};
    std::uint32_t mask{};
    std::uint8_t fifo{};
    if (auto [_, e] = std::from_chars(args[0].data(), args[0].data() + args[0].size(), filter);
        e != std::errc{} || filter > 27) {
        print_can_filter_usage(terminal);
        return;
    }

    if (auto [_, e] = std::from_chars(args[1].data(), args[1].data() + args[1].size(), id, 16);
        e != std::errc{}) {
        print_can_filter_usage(terminal);
        return;
    }

    if (auto [_, e] = std::from_chars(args[2].data(), args[2].data() + args[2].size(), mask, 16);
        e != std::errc{}) {
        print_can_filter_usage(terminal);
        return;
    }

    if (auto [_, e] = std::from_chars(args[3].data(), args[3].data() + args[3].size(), fifo);
        e != std::errc{} || fifo > 1) {
        print_can_filter_usage(terminal);
        return;
    }

    eps::eps.peripherals().can().setup_filter(spl::peripherals::can::concepts::filter{filter},
                                              id,
                                              mask,
                                              spl::peripherals::can::concepts::rx_fifo{fifo},
                                              true);
});
