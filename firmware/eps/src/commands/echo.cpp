#include "eps.h"
#include "terminal/terminal.h"

const terminal::command echo("echo", [](std::span<std::string_view> args) {
    for (std::size_t i = 0; i < args.size(); i++) {
        eps::eps.terminal().print("{}: {}\n", i, args[i]);
    }
});
