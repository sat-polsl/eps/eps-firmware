#include "eps.h"
#include "satext/charconv.h"
#include "satext/constexpr_map.h"
#include "terminal/terminal.h"

using namespace std::literals::string_view_literals;

static constexpr satext::constexpr_map<std::string_view, power_service::power_output, 7> output_lut{
    {"3v1"sv, power_service::power_output::v3v3_1},
    {"3v2"sv, power_service::power_output::v3v3_2},
    {"3v3"sv, power_service::power_output::v3v3_3},
    {"3v4"sv, power_service::power_output::v3v3_4},
    {"5v1"sv, power_service::power_output::v5_1},
    {"5v2"sv, power_service::power_output::v5_2},
    {"12v"sv, power_service::power_output::v12}};

void print_outputs(decltype(eps::eps.terminal())& terminal) {
    for (auto&& [name, _] : output_lut) {
        terminal.print("{}, ", name);
    }
    terminal.print("\n");
}

const terminal::command ps_enable("ps_enable", [](std::span<std::string_view> args) {
    if (args.size() != 1 || !output_lut.contains(args[0])) {
        eps::eps.terminal().print("usage: ps_enable <output>, where <output>:\n");
        print_outputs(eps::eps.terminal());
        return;
    }

    eps::eps.power_service().enable(output_lut[args[0]]);
});

const terminal::command ps_disable("ps_disable", [](std::span<std::string_view> args) {
    if (args.size() != 1 || !output_lut.contains(args[0])) {
        eps::eps.terminal().print("usage: ps_disable <output>, where <output>:\n");
        print_outputs(eps::eps.terminal());
        return;
    }

    eps::eps.power_service().disable(output_lut[args[0]]);
});

const terminal::command ps_is_enabled("ps_state", [](std::span<std::string_view>) {
    auto& ps = eps::eps.power_service();
    for (auto&& [name, output] : output_lut) {
        eps::eps.terminal().print("{}: {}\n", name, ps.is_enabled(output));
    }
});

const terminal::command ps_kick("ps_kick", [](std::span<std::string_view> args) {
    if (args.size() != 1 || !output_lut.contains(args[0])) {
        eps::eps.terminal().print("usage: ps_kick <output>, where <output>:\n");
        print_outputs(eps::eps.terminal());
        return;
    }

    eps::eps.power_service().kick_watchdog(output_lut[args[0]]);
});

const terminal::command ps_enable_wdt("ps_enable_wdt", [](std::span<std::string_view> args) {
    if (args.size() != 1 || !output_lut.contains(args[0])) {
        eps::eps.terminal().print("usage: ps_enable_wdt <output>, where <output>:\n");
        print_outputs(eps::eps.terminal());
        return;
    }

    eps::eps.power_service().enable_watchdog(output_lut[args[0]]);
});

const terminal::command ps_disable_wdt("ps_disable_wdt", [](std::span<std::string_view> args) {
    if (args.size() != 1 || !output_lut.contains(args[0])) {
        eps::eps.terminal().print("usage: ps_disable_wdt <output>, where <output>:\n");
        print_outputs(eps::eps.terminal());
        return;
    }

    eps::eps.power_service().disable_watchdog(output_lut[args[0]]);
});

const terminal::command ps_set_timeout("ps_set_timeout", [](std::span<std::string_view> args) {
    if (args.size() != 2 || !output_lut.contains(args[0])) {
        eps::eps.terminal().print(
            "usage: ps_set_timeout <output> <timeout_seconds>, where <output>:\n");
        print_outputs(eps::eps.terminal());
        return;
    }

    std::uint32_t timeout;

    if (auto [_, e] = satext::from_chars(args[1], timeout); e != std::errc{}) {
        eps::eps.terminal().print(
            "usage: ps_set_timeout <output> <timeout_seconds>, where <output>:\n");
        print_outputs(eps::eps.terminal());
        return;
    }

    eps::eps.power_service().set_watchdog_timeout(output_lut[args[0]],
                                                  satos::chrono::seconds{timeout});
});
