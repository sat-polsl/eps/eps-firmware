#include "eps.h"
#include "satext/charconv.h"
#include "satos/clock.h"
#include "satos/thread.h"
#include "terminal/terminal.h"

const terminal::command sleep_test("sleep_test", [](std::span<std::string_view> args) {
    if (args.size() != 2) {
        eps::eps.terminal().print("usage: sleep_test <sleep_ms> <repetitions>\n");
        return;
    }

    std::uint32_t sleep_ms{};
    std::uint32_t repetitions{};

    if (auto [_, e] = satext::from_chars(args[0], sleep_ms); e != std::errc{} && sleep_ms < 10) {
        eps::eps.terminal().print("usage: sleep_test <sleep_ms> <repetitions>\n");
        return;
    }

    if (auto [_, e] = satext::from_chars(args[1], repetitions); e != std::errc{}) {
        eps::eps.terminal().print("usage: sleep_test <sleep_ms> <repetitions>\n");
        return;
    }

    eps::eps.terminal().print("Uptime: {}\n", satos::clock::now().time_since_epoch().count());

    for (std::uint32_t i = 0; i < repetitions; i++) {
        satos::this_thread::sleep_for(satos::chrono::milliseconds(sleep_ms));
        eps::eps.terminal().print("Uptime: {}\n", satos::clock::now().time_since_epoch().count());
    }
});
