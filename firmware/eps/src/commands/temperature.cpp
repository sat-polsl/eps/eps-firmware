#include "eps.h"
#include "terminal/terminal.h"

const terminal::command temperature("temperature", [](std::span<std::string_view>) {
    auto& devices = eps::eps.devices();

    devices.lm75b_3v3()
        .read_temperature()
        .map([](auto value) { eps::eps.terminal().print("3V3: {}\n", value); })
        .map_error([](auto error) {
            eps::eps.terminal().print("3V3 error: {}\n", satext::to_underlying_type(error));
        });
    devices.lm75b_5v()
        .read_temperature()
        .map([](auto value) { eps::eps.terminal().print("5V: {}\n", value); })
        .map_error([](auto error) {
            eps::eps.terminal().print("5V error: {}\n", satext::to_underlying_type(error));
        });
    devices.lm75b_12v()
        .read_temperature()
        .map([](auto value) { eps::eps.terminal().print("12V: {}\n", value); })
        .map_error([](auto error) {
            eps::eps.terminal().print("12 error: {}\n", satext::to_underlying_type(error));
        });
    devices.lm75b_cell1()
        .read_temperature()
        .map([](auto value) { eps::eps.terminal().print("Cell1: {}\n", value); })
        .map_error([](auto error) {
            eps::eps.terminal().print("Cell1 error: {}\n", satext::to_underlying_type(error));
        });
    devices.lm75b_cell2()
        .read_temperature()
        .map([](auto value) { eps::eps.terminal().print("Cell2: {}\n", value); })
        .map_error([](auto error) {
            eps::eps.terminal().print("Cell2 error: {}\n", satext::to_underlying_type(error));
        });
    devices.lm75b_cell3()
        .read_temperature()
        .map([](auto value) { eps::eps.terminal().print("Cell3: {}\n", value); })
        .map_error([](auto error) {
            eps::eps.terminal().print("Cell3 error: {}\n", satext::to_underlying_type(error));
        });
});
