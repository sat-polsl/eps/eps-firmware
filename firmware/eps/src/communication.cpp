#include "communication.h"

namespace eps::communication {

using spl::peripherals::can::message;
using spl::peripherals::can::concepts::filter;
using namespace ::communication::can;

communication::communication(power_service::power_service& power_service,
                             ::telemetry::telemetry_state& state) :
    handler_{decoder{[](const message& msg, filter f) -> std::optional<message> {
                 if (msg.matched_filter == f) {
                     return msg;
                 } else {
                     return std::nullopt;
                 }
             }},
             encoder{[](const message& msg, std::uint32_t id) -> std::optional<message> {
                 auto result = msg;
                 result.is_extended = true;
                 result.id = id;
                 return result;
             }},
             handlers::echo_message_handler{},
             handlers::power_output_on_handler{power_service},
             handlers::power_output_off_handler{power_service},
             handlers::power_watchdog_enable_handler{power_service},
             handlers::power_watchdog_disable_handler{power_service},
             handlers::power_watchdog_set_timeout_handler{power_service},
             handlers::power_watchdog_kick_handler{power_service},
             handlers::get_telemetry_handler{state}} {}

satext::expected<spl::peripherals::can::message, ::communication::status>
communication::handle(const spl::peripherals::can::message& msg) {
    spl::peripherals::can::message result{};
    auto status = handler_.handle(msg, result);
    if (status == ::communication::status::ok) {
        return result;
    } else {
        return satext::unexpected{status};
    }
}
} // namespace eps::communication
