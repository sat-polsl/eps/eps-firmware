#include "devices.h"
#include "hw.h"
#include "satos/chrono.h"

using namespace satos::chrono_literals;

namespace eps::devices {
devices::devices(eps::peripherals::peripherals& peripherals, eps::drivers::drivers& drivers) :
    lm75b_3v3_(drivers.i2c1(), hw::i2c_addresses::temperature_3v3, 1_s),
    lm75b_5v_(drivers.i2c1(), hw::i2c_addresses::temperature_5v, 1_s),
    lm75b_12v_(drivers.i2c1(), hw::i2c_addresses::temperature_12v, 1_s),
    lm75b_cell1_(drivers.i2c1(), hw::i2c_addresses::temperature_cell1, 1_s),
    lm75b_cell2_(drivers.i2c1(), hw::i2c_addresses::temperature_cell2, 1_s),
    lm75b_cell3_(drivers.i2c1(), hw::i2c_addresses::temperature_cell3, 1_s),
    bms1_{::devices::mcp73871::configuration<spl::peripherals::gpio::gpio>{
        .charge_enable = peripherals.bms1_charge_enable(),
        .power_good = peripherals.bms1_power_good(),
        .stat1 = peripherals.bms1_stat1(),
        .stat2 = peripherals.bms1_stat2()}},
    bms2_{::devices::mcp73871::configuration<spl::peripherals::gpio::gpio>{
        .charge_enable = peripherals.bms2_charge_enable(),
        .power_good = peripherals.bms2_power_good(),
        .stat1 = peripherals.bms2_stat1(),
        .stat2 = peripherals.bms2_stat2()}},
    bms3_{::devices::mcp73871::configuration<spl::peripherals::gpio::gpio>{
        .charge_enable = peripherals.bms3_charge_enable(),
        .power_good = peripherals.bms3_power_good(),
        .stat1 = peripherals.bms3_stat1(),
        .stat2 = peripherals.bms3_stat2()}},
    adc_current_{drivers.spi2(), peripherals.adc_current_select(), 1_s},
    adc_voltage_{drivers.spi2(), peripherals.adc_voltage_select(), 1_s},
    fram_{drivers.spi3(), peripherals.fram_select()} {}

void devices::initialize() {
    adc_current_.reset();
    adc_current_.initialize();
    adc_voltage_.reset();
    adc_voltage_.initialize();
}

devices::lm75b_type& devices::lm75b_3v3() { return lm75b_3v3_; }

devices::lm75b_type& devices::lm75b_5v() { return lm75b_5v_; }

devices::lm75b_type& devices::lm75b_12v() { return lm75b_12v_; }

devices::lm75b_type& devices::lm75b_cell1() { return lm75b_cell1_; }

devices::lm75b_type& devices::lm75b_cell2() { return lm75b_cell2_; }

devices::lm75b_type& devices::lm75b_cell3() { return lm75b_cell3_; }

devices::bms_type& devices::bms1() { return bms1_; }

devices::bms_type& devices::bms2() { return bms2_; }

devices::bms_type& devices::bms3() { return bms3_; }

devices::adc_current_type& devices::adc_current() { return adc_current_; }

devices::adc_voltage_type& devices::adc_voltage() { return adc_voltage_; }

devices::fram_type& devices::fram() { return fram_; }

} // namespace eps::devices
