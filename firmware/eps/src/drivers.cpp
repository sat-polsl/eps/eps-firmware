#include "drivers.h"

namespace eps::drivers {
drivers::drivers(eps::peripherals::peripherals& peripherals) :
    dma1_(peripherals.dma1(), peripherals.nvic()),
    dma2_(peripherals.dma2(), peripherals.nvic()),
    terminal_uart_{peripherals.terminal_uart(), peripherals.nvic()},
    i2c1_{peripherals.i2c1(), peripherals.nvic()},
    spi2_{peripherals.spi2(), dma1_},
    spi3_{peripherals.spi3(), dma2_},
    can_{peripherals.can(), peripherals.nvic()} {}

void drivers::initialize() {
    spi2_.initialize();
    spi3_.initialize();
}

void drivers::initialize_rtos_context() {
    dma1_.initialize();
    dma2_.initialize();
}

spl::drivers::uart::uart3& drivers::terminal_uart() { return terminal_uart_; }

spl::drivers::i2c::i2c::i2c1& drivers::i2c1() { return i2c1_; }

spl::drivers::spi::spi_dma_vectored::spi2& drivers::spi2() { return spi2_; }

spl::drivers::can::can1& drivers::can() { return can_; }

spl::drivers::dma::dma& drivers::dma1() { return dma1_; }

spl::drivers::spi::spi_dma_vectored::spi3& drivers::spi3() { return spi3_; }

spl::drivers::dma::dma& drivers::dma2() { return dma2_; }

} // namespace eps::drivers
