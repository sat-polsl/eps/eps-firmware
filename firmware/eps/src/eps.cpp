#include "eps.h"
#include "boot_parameters/boot_parameters.h"
#include "hw.h"
#include "satos/kernel.h"

namespace eps {

using namespace satos::chrono_literals;

void application::run() {
    peripherals_.initialize();
    drivers_.initialize();
    telemetry_.initialize();

    satos::kernel::initialize(hw::sysclock, hw::kernel_freq);

    satos::kernel::set_idle_handler([]() { asm volatile("wfi"); });

    can_service_.start();
    telemetry_.start();
    this->start();

    satos::kernel::start();

    for (;;) {}
}

peripherals::peripherals& application::peripherals() { return peripherals_; }

drivers::drivers& application::drivers() { return drivers_; }

devices::devices& application::devices() { return devices_; }

telemetry::telemetry& application::telemetry() { return telemetry_; }

power_service::power_service& application::power_service() { return power_service_; }

communication::communication& application::communication() { return communication_; }

void application::thread_function() {
    drivers_.initialize_rtos_context();
    devices_.initialize();

    power_service::configuration power_configuration{};
    if (devices_.fram().probe()) {
        power_configuration_.initialize();
        power_configuration = power_configuration_.get();
    } else {
        peripherals_.set_pinmux(eps::peripherals::peripherals::pinmux::terminal);
        terminal_thread_.start();
    }

    power_service_.initialize(power_configuration);

    telemetry_.state().write(eps::telemetry::id::boot_counter,
                             ::boot_parameters::boot_parameters.boot_counter);

    for (;;) {
        peripherals().iwdg().kick();
        peripherals().led().toggle();
        satos::this_thread::sleep_for(1_s);
    }
}

} // namespace eps
