#include "eps.h"

extern "C" void usart3_isr(void) { eps::eps.drivers().terminal_uart().isr_handler(); }

extern "C" void i2c1_ev_isr(void) { eps::eps.drivers().i2c1().isr_ev_handler(); }

extern "C" void can1_tx_isr(void) { eps::eps.drivers().can().tx_isr_handler(); }

extern "C" void can1_rx0_isr(void) { eps::eps.drivers().can().rx0_isr_handler(); }

extern "C" void can1_rx1_isr(void) { eps::eps.drivers().can().rx1_isr_handler(); }

extern "C" void dma1_channel4_isr() {
    eps::eps.drivers().dma1().isr_handler(spl::peripherals::nvic::irq::dma1_channel4,
                                          spl::peripherals::dma::channel4);
}

extern "C" void dma1_channel5_isr() {
    eps::eps.drivers().dma1().isr_handler(spl::peripherals::nvic::irq::dma1_channel5,
                                          spl::peripherals::dma::channel5);
}

extern "C" void dma2_channel1_isr() {
    eps::eps.drivers().dma2().isr_handler(spl::peripherals::nvic::irq::dma2_channel1,
                                          spl::peripherals::dma::channel1);
}

extern "C" void dma2_channel2_isr() {
    eps::eps.drivers().dma2().isr_handler(spl::peripherals::nvic::irq::dma2_channel2,
                                          spl::peripherals::dma::channel2);
}
