#include "peripherals.h"
#include "communication/can/identifiers.h"

using namespace spl::peripherals;
using namespace ::communication::can::identifiers;

namespace eps::peripherals {

void peripherals::initialize() {
    sysclk_.setup<hw::sysclk_cfg>();

    rcc_.enable_clock(rcc::peripheral::gpioa)
        .enable_clock(rcc::peripheral::gpiob)
        .enable_clock(rcc::peripheral::gpioc)
        .enable_clock(rcc::peripheral::uart3)
        .enable_clock(rcc::peripheral::i2c1)
        .enable_clock(rcc::peripheral::spi2)
        .enable_clock(rcc::peripheral::spi3)
        .enable_clock(rcc::peripheral::dma1)
        .enable_clock(rcc::peripheral::dma2)
        .enable_clock(rcc::peripheral::can1)
        .enable_clock(rcc::peripheral::crc);

    crc_.reset();
    crc_.set_reverse_input(true);
    crc_.set_reverse_output(true);
    crc_.set_final_xor_value(0xffffffff);

    led_.setup(gpio::mode::output, gpio::pupd::pullup);

    terminal_uart_tx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, gpio::af7);
    terminal_uart_rx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, gpio::af7);
    terminal_uart_.set_baudrate(hw::terminal_uart::baudrate).set_mode(uart::mode::rx_tx).enable();

    can_tx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, hw::can::tx_af);
    can_rx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, hw::can::rx_af);
    can_.initialize(hw::can::timings, can::options{});

    can_.setup_filter(echo::filter, echo::request_id, can::concepts::id_mask, can::rx_fifo0, true);
    can_.setup_filter(power_output_on::filter,
                      power_output_on::request_id,
                      can::concepts::id_mask,
                      can::rx_fifo0,
                      true);
    can_.setup_filter(power_output_off::filter,
                      power_output_off::request_id,
                      can::concepts::id_mask,
                      can::rx_fifo0,
                      true);
    can_.setup_filter(power_watchdog_enable::filter,
                      power_watchdog_enable::request_id,
                      can::concepts::id_mask,
                      can::rx_fifo0,
                      true);
    can_.setup_filter(power_watchdog_disable::filter,
                      power_watchdog_disable::request_id,
                      can::concepts::id_mask,
                      can::rx_fifo0,
                      true);
    can_.setup_filter(power_watchdog_set_timeout::filter,
                      power_watchdog_set_timeout::request_id,
                      can::concepts::id_mask,
                      can::rx_fifo0,
                      true);
    can_.setup_filter(power_watchdog_kick::filter,
                      power_watchdog_kick::request_id,
                      can::concepts::id_mask,
                      can::rx_fifo0,
                      true);
    can_.setup_filter(get_telemetry::filter,
                      get_telemetry::request_id,
                      can::concepts::id_mask,
                      can::rx_fifo0,
                      true);

    i2c1_scl_gpio_.setup(gpio::mode::af, gpio::pupd::none, gpio::af4);
    i2c1_scl_gpio_.set_output_type(gpio::output_type::open_drain);
    i2c1_sda_gpio_.setup(gpio::mode::af, gpio::pupd::none, gpio::af4);
    i2c1_sda_gpio_.set_output_type(gpio::output_type::open_drain);
    i2c1_.set_speed(spl::peripherals::i2c::speed::i2c_100k, 4);

    spi2_.set_baudrate_prescaler(spi::baudrate_prescaler::div256);
    spi2_sck_.setup(gpio::mode::af, gpio::pupd::pullup, gpio::af5);
    spi2_miso_.setup(gpio::mode::af, gpio::pupd::pullup, gpio::af5);
    spi2_mosi_.setup(gpio::mode::af, gpio::pupd::pullup, gpio::af5);

    set_pinmux(pinmux::fram);
    spi3_.set_clock_mode(spi::clock_mode::mode3);
    spi3_.set_baudrate_prescaler(spi::baudrate_prescaler::div4);

    fram_select_.set();
    adc_current_select_.set();
    adc_voltage_select_.set();
    adc_current_select_.setup(gpio::mode::output, gpio::pupd::pullup);
    adc_voltage_select_.setup(gpio::mode::output, gpio::pupd::pullup);
    fram_select_.setup(gpio::mode::output, gpio::pupd::pullup);

    initialize_power_gpios();
    initialize_bms_gpios();

    nvic_.set_priority(nvic::irq::uart3, 5).enable_irq(nvic::irq::uart3);
    nvic_.set_priority(nvic::irq::i2c1_ev, 5).enable_irq(nvic::irq::i2c1_ev);
    nvic_.set_priority(nvic::irq::can1_tx, 5).enable_irq(nvic::irq::can1_tx);
    nvic_.set_priority(nvic::irq::can1_rx0, 5).enable_irq(nvic::irq::can1_rx0);
    nvic_.set_priority(nvic::irq::dma1_channel4, 5).enable_irq(nvic::irq::dma1_channel4);
    nvic_.set_priority(nvic::irq::dma1_channel5, 5).enable_irq(nvic::irq::dma1_channel5);
    nvic_.set_priority(nvic::irq::dma2_channel1, 5).enable_irq(nvic::irq::dma2_channel1);
    nvic_.set_priority(nvic::irq::dma2_channel2, 5).enable_irq(nvic::irq::dma2_channel2);

    iwdg_.initialize();
    iwdg_.set_period_ms(hw::watchdog_period_ms);
}

void peripherals::initialize_power_gpios() const {
    enable_3v3_.clear();
    enable_5v_.clear();
    enable_12v_.clear();
    mode_5v_.clear();
    enable_3v3_l1_.set();
    enable_3v3_l2_.set();
    enable_3v3_l3_.set();
    enable_3v3_l4_.set();
    enable_5v_l1_.clear();
    enable_5v_l2_.clear();
    enable_12v_l1_.clear();
    heater_.clear();
    enable_3v3_.setup(gpio::mode::output, gpio::pupd::pullup);
    enable_12v_.setup(gpio::mode::output, gpio::pupd::pullup);
    enable_5v_.setup(gpio::mode::output, gpio::pupd::pullup);
    mode_5v_.setup(gpio::mode::output, gpio::pupd::pullup);
    enable_3v3_l1_.setup(gpio::mode::output, gpio::pupd::pullup);
    enable_3v3_l2_.setup(gpio::mode::output, gpio::pupd::pullup);
    enable_3v3_l3_.setup(gpio::mode::output, gpio::pupd::pullup);
    enable_3v3_l4_.setup(gpio::mode::output, gpio::pupd::pullup);
    enable_5v_l1_.setup(gpio::mode::output, gpio::pupd::pullup);
    enable_5v_l2_.setup(gpio::mode::output, gpio::pupd::pullup);
    enable_12v_l1_.setup(gpio::mode::output, gpio::pupd::pullup);
    heater_.setup(gpio::mode::output, gpio::pupd::pullup);
}

void peripherals::initialize_bms_gpios() const {
    bms1_charge_enable_.set();
    bms1_charge_enable_.setup(gpio::mode::output, gpio::pupd::none);
    bms1_power_good_.setup(gpio::mode::input, gpio::pupd::pullup);
    bms1_stat1_.setup(gpio::mode::input, gpio::pupd::pullup);
    bms1_stat2_.setup(gpio::mode::input, gpio::pupd::pullup);

    bms2_charge_enable_.set();
    bms2_charge_enable_.setup(gpio::mode::output, gpio::pupd::none);
    bms2_power_good_.setup(gpio::mode::input, gpio::pupd::pullup);
    bms2_stat1_.setup(gpio::mode::input, gpio::pupd::pullup);
    bms2_stat2_.setup(gpio::mode::input, gpio::pupd::pullup);

    bms3_charge_enable_.set();
    bms3_charge_enable_.setup(gpio::mode::output, gpio::pupd::none);
    bms3_power_good_.setup(gpio::mode::input, gpio::pupd::pullup);
    bms3_stat1_.setup(gpio::mode::input, gpio::pupd::pullup);
    bms3_stat2_.setup(gpio::mode::input, gpio::pupd::pullup);
}

spl::peripherals::gpio::gpio& peripherals::led() { return led_; }

spl::peripherals::uart::uart& peripherals::terminal_uart() { return terminal_uart_; }

spl::peripherals::gpio::gpio& peripherals::terminal_uart_tx_gpio() {
    return terminal_uart_tx_gpio_;
}

spl::peripherals::gpio::gpio& peripherals::terminal_uart_rx_gpio() {
    return terminal_uart_rx_gpio_;
}

spl::peripherals::i2c::i2c& peripherals::i2c1() { return i2c1_; }

spl::peripherals::spi::spi& peripherals::spi2() { return spi2_; }

spl::peripherals::rcc::rcc& peripherals::rcc() { return rcc_; }

spl::peripherals::nvic::nvic& peripherals::nvic() { return nvic_; }

spl::peripherals::can::can& peripherals::can() { return can_; }

spl::peripherals::gpio::gpio& peripherals::enable_3v3() { return enable_3v3_; }

spl::peripherals::gpio::gpio& peripherals::enable_5v() { return enable_5v_; }

spl::peripherals::gpio::gpio& peripherals::enable_12v() { return enable_12v_; }

spl::peripherals::gpio::gpio& peripherals::mode_5v() { return mode_5v_; }

spl::peripherals::gpio::gpio& peripherals::enable_3v3_l1() { return enable_3v3_l1_; }

spl::peripherals::gpio::gpio& peripherals::enable_3v3_l2() { return enable_3v3_l2_; }

spl::peripherals::gpio::gpio& peripherals::enable_3v3_l3() { return enable_3v3_l3_; }

spl::peripherals::gpio::gpio& peripherals::enable_3v3_l4() { return enable_3v3_l4_; }

spl::peripherals::gpio::gpio& peripherals::enable_5v_l1() { return enable_5v_l1_; }

spl::peripherals::gpio::gpio& peripherals::enable_5v_l2() { return enable_5v_l2_; }

spl::peripherals::gpio::gpio& peripherals::enable_12v_l1() { return enable_12v_l1_; }

spl::peripherals::gpio::gpio& peripherals::adc_current_select() { return adc_current_select_; }

spl::peripherals::gpio::gpio& peripherals::adc_voltage_select() { return adc_voltage_select_; }

spl::peripherals::iwdg::iwdg& peripherals::iwdg() { return iwdg_; }

spl::peripherals::flash::flash& peripherals::flash() { return flash_; }

spl::peripherals::gpio::gpio& peripherals::bms1_charge_enable() { return bms1_charge_enable_; }

spl::peripherals::gpio::gpio& peripherals::bms1_power_good() { return bms1_power_good_; }

spl::peripherals::gpio::gpio& peripherals::bms1_stat1() { return bms1_stat1_; }

spl::peripherals::gpio::gpio& peripherals::bms1_stat2() { return bms1_stat2_; }

spl::peripherals::gpio::gpio& peripherals::bms2_charge_enable() { return bms2_charge_enable_; }

spl::peripherals::gpio::gpio& peripherals::bms2_power_good() { return bms2_power_good_; }

spl::peripherals::gpio::gpio& peripherals::bms2_stat1() { return bms2_stat1_; }

spl::peripherals::gpio::gpio& peripherals::bms2_stat2() { return bms2_stat2_; }

spl::peripherals::gpio::gpio& peripherals::bms3_charge_enable() { return bms3_charge_enable_; }

spl::peripherals::gpio::gpio& peripherals::bms3_power_good() { return bms3_power_good_; }

spl::peripherals::gpio::gpio& peripherals::bms3_stat1() { return bms3_stat1_; }

spl::peripherals::gpio::gpio& peripherals::bms3_stat2() { return bms3_stat2_; }

spl::peripherals::dma::dma& peripherals::dma1() { return dma1_; }

spl::peripherals::gpio::gpio& peripherals::heater() { return heater_; }

spl::peripherals::spi::spi& peripherals::spi3() { return spi3_; }

spl::peripherals::dma::dma& peripherals::dma2() { return dma2_; }

spl::peripherals::gpio::gpio& peripherals::fram_select() { return fram_select_; }

void peripherals::set_pinmux(peripherals::pinmux mux) const {
    if (mux == pinmux::terminal) {
        terminal_uart_tx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, gpio::af7);
        terminal_uart_rx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, gpio::af7);
    } else if (mux == pinmux::fram) {
        spi3_miso_.setup(gpio::mode::af, gpio::pupd::pullup, hw::spi3::miso::af);
        spi3_mosi_.setup(gpio::mode::af, gpio::pupd::pullup, hw::spi3::mosi::af);
        spi3_sck_.setup(gpio::mode::af, gpio::pupd::pullup, hw::spi3::clk::af);
    }
}

spl::peripherals::crc::crc& peripherals::crc() { return crc_; }
} // namespace eps::peripherals
