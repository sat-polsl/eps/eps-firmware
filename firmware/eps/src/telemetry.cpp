#include "telemetry.h"
#include "satos/chrono.h"

using namespace satos::chrono_literals;

namespace eps::telemetry {

telemetry::telemetry(devices::devices& devices) : devices_{devices} {}

void telemetry::initialize() {
    telemetry_service_.add_sampler(uptime_sampler_, 1_s);
    telemetry_service_.add_sampler(temperature_3v3_sampler_, 15_s);
    telemetry_service_.add_sampler(temperature_5v_sampler_, 15_s);
    telemetry_service_.add_sampler(temperature_12v_sampler_, 15_s);
    telemetry_service_.add_sampler(temperature_cell1_sampler_, 15_s);
    telemetry_service_.add_sampler(temperature_cell2_sampler_, 15_s);
    telemetry_service_.add_sampler(temperature_cell3_sampler_, 15_s);
    telemetry_service_.add_sampler(adc_sampler_, 100_ms);
}

void telemetry::start() { telemetry_service_.start(); }

::telemetry::telemetry_state& telemetry::state() { return state_; }

} // namespace eps::telemetry
