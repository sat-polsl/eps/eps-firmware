#include "threads/can_service.h"

namespace eps::threads {

can_service::can_service(spl::drivers::can::can1& can,
                         communication::communication& communication) :
    can_{can},
    communication_{communication} {}

void can_service::thread_function() {
    for (;;) {
        auto result = can_.read(spl::peripherals::can::rx_fifo0, timeout);
        result.map([this](const auto& received) {
            communication_.handle(received).map(
                [this](const auto& response) { can_.write(response, timeout); });
        });
    }
}

} // namespace eps::threads
