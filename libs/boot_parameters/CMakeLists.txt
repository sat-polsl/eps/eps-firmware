set(TARGET boot_parameters)

add_library(${TARGET} STATIC)

target_include_directories(${TARGET} PUBLIC
    include
    )

target_sources(${TARGET} PRIVATE
    src/boot_parameters.cpp
    )

target_link_directories(${TARGET}
    INTERFACE
    ld
    )
