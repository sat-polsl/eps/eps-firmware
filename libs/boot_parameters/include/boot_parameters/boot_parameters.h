#pragma once
#include <cstdint>

namespace boot_parameters {
/**
 * @defgroup boot_parameters Boot parameters
 * @ingroup bootloader
 * @brief Variables shared between Bootloader and Application through shared RAM
 * Both Bootloader and Application has to link to boot_parameters.ld to ensure common address for
 * boot parameters
 * @{
 */

/**
 * @brief Boot parameters struct
 */
struct parameters {
    /**
     * @brief Number of successful boots
     */
    std::uint32_t boot_counter;

    /**
     * @brief Number of boot attempts
     */
    std::uint32_t attempt_boot_counter;
};

/**
 * @brief Boot parameters variable shared between Bootloader and Application
 */
extern parameters boot_parameters;

/** @} */
} // namespace boot_parameters
