#pragma once
#include <cstdint>

namespace bootloader::utils {

/**
 * @defgroup bootloader_utils Bootloader utility
 * @ingroup bootloader
 * @{
 */

/**
 * @brief Boot slot descriptor
 */
struct boot_slot {
    /**
     * @brief Address of boot slot in FRAM memory
     */
    std::uint32_t address;
    std::uint32_t magic_a;
    std::uint32_t magic_b;
};

/** @} */

} // namespace bootloader::utils
