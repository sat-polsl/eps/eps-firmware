#pragma once
#include "communication/can/identifiers.h"
#include "communication/can/messages/get_telemetry_request.h"
#include "communication/decoder_base.h"
#include "spl/peripherals/can/concepts/enums.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can::decoders {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Get telemetry decoder.
 */
class get_telemetry_request_decoder
    : public communication::decoder_base<communication::can::messages::get_telemetry_request,
                                         spl::peripherals::can::concepts::filter,
                                         communication::can::identifiers::get_telemetry::filter> {
public:
    /**
     * @brief Decodes get telemetry message.
     * @param msg CAN message.
     * @return Get telemetry message on success, status otherwise.
     */
    static decoded_return_type decode(const spl::peripherals::can::concepts::message& msg);
};

/** @} */
} // namespace communication::can::decoders
