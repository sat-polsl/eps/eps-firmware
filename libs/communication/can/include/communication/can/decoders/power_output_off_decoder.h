#pragma once
#include "communication/can/identifiers.h"
#include "communication/can/messages/power_output_off_message.h"
#include "communication/decoder_base.h"
#include "spl/peripherals/can/concepts/enums.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can::decoders {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power output off message decoder
 */
class power_output_off_decoder : public communication::decoder_base<
                                     communication::can::messages::power_output_off_message,
                                     spl::peripherals::can::concepts::filter,
                                     communication::can::identifiers::power_output_off::filter> {
public:
    /**
     * @brief Decodes echo message
     * @param msg CAN message
     * @return echo message on success, status otherwise
     */
    static decoded_return_type decode(const spl::peripherals::can::concepts::message& msg);
};

/** @} */
} // namespace communication::can::decoders
