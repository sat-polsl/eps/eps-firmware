#pragma once
#include "communication/can/identifiers.h"
#include "communication/can/messages/power_watchdog_disable_message.h"
#include "communication/decoder_base.h"
#include "spl/peripherals/can/concepts/enums.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can::decoders {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power watchdog disable message decoder
 */
class power_watchdog_disable_decoder
    : public communication::decoder_base<
          communication::can::messages::power_watchdog_disable_message,
          spl::peripherals::can::concepts::filter,
          communication::can::identifiers::power_watchdog_disable::filter> {
public:
    /**
     * @brief Decodes power watchdog disable message
     * @param msg CAN message
     * @return decoded message on success, status otherwise
     */
    static decoded_return_type decode(const spl::peripherals::can::concepts::message& msg);
};

/** @} */
} // namespace communication::can::decoders
