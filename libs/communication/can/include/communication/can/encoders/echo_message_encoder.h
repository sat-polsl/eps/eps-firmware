#pragma once
#include "communication/can/identifiers.h"
#include "communication/can/messages/echo_message.h"
#include "communication/encoder_base.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can::encoders {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Echo message encoder
 */
class echo_message_encoder
    : public communication::encoder_base<communication::can::messages::echo_message,
                                         std::uint32_t,
                                         communication::can::identifiers::echo::response_id> {
public:
    /**
     * Encodes echo message
     * @param msg echo message
     * @param result encoded CAN message
     * @return communication status
     */
    static communication::status encode(const communication::can::messages::echo_message& msg,
                                        spl::peripherals::can::concepts::message& result);
};

/** @} */

} // namespace communication::can::encoders
