#pragma once
#include "communication/can/identifiers.h"
#include "communication/can/messages/get_telemetry_response.h"
#include "communication/encoder_base.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can::encoders {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Get telemetry response encoder.
 */
class get_telemetry_response_encoder
    : public communication::encoder_base<
          communication::can::messages::get_telemetry_response,
          std::uint32_t,
          communication::can::identifiers::get_telemetry::response_id> {
public:
    /**
     * Encodes get telemetry response.
     * @param msg Get telemetry response.
     * @param result Encoded CAN message.
     * @return Communication status.
     */
    static communication::status
    encode(const communication::can::messages::get_telemetry_response& msg,
           spl::peripherals::can::concepts::message& result);
};

/** @} */

} // namespace communication::can::encoders
