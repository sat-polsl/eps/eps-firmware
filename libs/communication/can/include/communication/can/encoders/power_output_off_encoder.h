#pragma once
#include "communication/can/identifiers.h"
#include "communication/can/messages/power_output_off_message.h"
#include "communication/encoder_base.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can::encoders {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power output off response encoder
 */
class power_output_off_encoder
    : public communication::encoder_base<
          communication::can::messages::power_output_off_message,
          std::uint32_t,
          communication::can::identifiers::power_output_off::response_id> {
public:
    /**
     * Encodes power output off response
     * @param msg Power output off message
     * @param result Encoded CAN message
     * @return Communication status
     */
    static communication::status
    encode(const communication::can::messages::power_output_off_message& msg,
           spl::peripherals::can::concepts::message& result);
};

/** @} */

} // namespace communication::can::encoders
