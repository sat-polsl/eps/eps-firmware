#pragma once
#include "communication/can/identifiers.h"
#include "communication/can/messages/power_watchdog_disable_message.h"
#include "communication/encoder_base.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can::encoders {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power watchdog disable response encoder
 */
class power_watchdog_disable_encoder
    : public communication::encoder_base<
          communication::can::messages::power_watchdog_disable_message,
          std::uint32_t,
          communication::can::identifiers::power_watchdog_disable::response_id> {
public:
    /**
     * Encodes power watchdog disable response
     * @param msg Power watchdog disable message
     * @param result Encoded CAN message
     * @return Communication status
     */
    static communication::status
    encode(const communication::can::messages::power_watchdog_disable_message& msg,
           spl::peripherals::can::concepts::message& result);
};

/** @} */

} // namespace communication::can::encoders
