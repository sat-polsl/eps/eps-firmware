#pragma once
#include "communication/can/identifiers.h"
#include "communication/can/messages/power_watchdog_enable_message.h"
#include "communication/encoder_base.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can::encoders {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power watchdog enable response encoder
 */
class power_watchdog_enable_encoder
    : public communication::encoder_base<
          communication::can::messages::power_watchdog_enable_message,
          std::uint32_t,
          communication::can::identifiers::power_watchdog_enable::response_id> {
public:
    /**
     * Encodes power watchdog enable response
     * @param msg Power watchdog enable message
     * @param result Encoded CAN message
     * @return Communication status
     */
    static communication::status
    encode(const communication::can::messages::power_watchdog_enable_message& msg,
           spl::peripherals::can::concepts::message& result);
};

/** @} */

} // namespace communication::can::encoders
