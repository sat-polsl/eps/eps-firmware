#pragma once
#include "communication/can/identifiers.h"
#include "communication/can/messages/power_watchdog_kick_message.h"
#include "communication/encoder_base.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can::encoders {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power kick watchdog response encoder
 */
class power_watchdog_kick_encoder
    : public communication::encoder_base<
          communication::can::messages::power_watchdog_kick_message,
          std::uint32_t,
          communication::can::identifiers::power_watchdog_kick::response_id> {
public:
    /**
     * Encodes power kick watchdog response
     * @param msg Power kick watchdog message
     * @param result Encoded CAN message
     * @return Communication status
     */
    static communication::status
    encode(const communication::can::messages::power_watchdog_kick_message& msg,
           spl::peripherals::can::concepts::message& result);
};

/** @} */

} // namespace communication::can::encoders
