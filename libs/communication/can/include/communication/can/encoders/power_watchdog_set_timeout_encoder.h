#pragma once
#include "communication/can/identifiers.h"
#include "communication/can/messages/power_watchdog_set_timeout_message.h"
#include "communication/encoder_base.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can::encoders {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power set watchdog timeout response encoder
 */
class power_watchdog_set_timeout_encoder
    : public communication::encoder_base<
          communication::can::messages::power_watchdog_set_timeout_message,
          std::uint32_t,
          communication::can::identifiers::power_watchdog_set_timeout::response_id> {
public:
    /**
     * Encodes power set watchdog timeout response
     * @param msg Power set watchdog timeout message
     * @param result Encoded CAN message
     * @return Communication status
     */
    static communication::status
    encode(const communication::can::messages::power_watchdog_set_timeout_message& msg,
           spl::peripherals::can::concepts::message& result);
};

/** @} */

} // namespace communication::can::encoders
