#pragma once

namespace communication::can {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Message type enumeration
 */
enum class message_type { request = 0b01, response = 0b10, notification = 0b11 };

/**
 * @brief Message ID enumeration
 */
enum class message_id {
    echo = 0x01,
    power_output_on = 0x02,
    power_output_off = 0x03,
    power_watchdog_enable = 0x04,
    power_watchdog_disable = 0x05,
    power_watchdog_set_timeout = 0x06,
    power_watchdog_kick = 0x07,
    get_telemetry = 0x08
};

/** @} */

} // namespace communication::can
