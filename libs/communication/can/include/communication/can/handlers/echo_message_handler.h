#pragma once
#include "communication/can/messages/echo_message.h"

namespace communication::can::handlers {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Echo message handler
 */
class echo_message_handler {
public:
    /**
     * @brief Handles echo message
     * @param msg echo message
     * @return echo message
     */
    communication::can::messages::echo_message
    handle(const communication::can::messages::echo_message& msg) const {
        return msg;
    }
};

/** @} */

} // namespace communication::can::handlers
