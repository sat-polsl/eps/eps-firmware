#pragma once
#include "communication/can/messages/get_telemetry_request.h"
#include "communication/can/messages/get_telemetry_response.h"
#include "telemetry/telemetry_state_concept.h"

namespace communication::can::handlers {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Get telemetry handler.
 */
template<telemetry::telemetry_state_concept State>
class get_telemetry_handler {
public:
    explicit get_telemetry_handler(State& state) : state_{state} {}

    /**
     * @brief Handles get telemetry request.
     * @param msg Get telemetyr response.
     */
    [[nodiscard]] messages::get_telemetry_response
    handle(const messages::get_telemetry_request& msg) const {
        auto id = msg.id();
        return messages::get_telemetry_response{id, state_.read(id)};
    }

private:
    State& state_;
};

/** @} */

} // namespace communication::can::handlers
