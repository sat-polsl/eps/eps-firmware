#pragma once
#include "communication/can/messages/power_output_off_message.h"
#include "power_service/power_service_concept.h"

namespace communication::can::handlers {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power output off handler
 */
template<power_service::power_service_concept PowerService>
class power_output_off_handler {
public:
    explicit power_output_off_handler(PowerService& power_service) :
        power_service_{power_service} {}

    /**
     * @brief Handles power output off message
     * @param msg power output off message
     */
    communication::can::messages::power_output_off_message
    handle(const communication::can::messages::power_output_off_message& msg) const {
        power_service_.disable(msg.output());
        return msg;
    }

private:
    PowerService& power_service_;
};

/** @} */

} // namespace communication::can::handlers
