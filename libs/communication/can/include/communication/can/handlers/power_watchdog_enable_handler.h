#pragma once

#include "communication/can/messages/power_watchdog_enable_message.h"
#include "power_service/power_service_concept.h"

namespace communication::can::handlers {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power watchdog enable handler
 */
template<power_service::power_service_concept PowerService>
class power_watchdog_enable_handler {
public:
    /**
     * @brief Constructor
     * @param power_service Power service to handle
     */
    explicit power_watchdog_enable_handler(PowerService& power_service) :
        power_service_(power_service) {}

    /**
     * @brief Handles power watchdog enable message
     * @param msg power watchdog enable message
     */
    communication::can::messages::power_watchdog_enable_message
    handle(const communication::can::messages::power_watchdog_enable_message& msg) const {
        power_service_.enable_watchdog(msg.output());
        return msg;
    }

private:
    PowerService& power_service_;
};
/** @} */
} // namespace communication::can::handlers
