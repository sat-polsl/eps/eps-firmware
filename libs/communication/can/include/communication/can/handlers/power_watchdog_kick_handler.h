#pragma once

#include "communication/can/messages/power_watchdog_kick_message.h"
#include "power_service/power_service_concept.h"

namespace communication::can::handlers {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power watchdog kick handler
 */
template<power_service::power_service_concept PowerService>
class power_watchdog_kick_handler {
public:
    /**
     * @brief Constructor
     * @param power_service Power service to handle
     */
    explicit power_watchdog_kick_handler(PowerService& power_service) :
        power_service_(power_service) {}

    /**
     * @brief Handles power kick watchdog message
     * @param msg power kick watchdog message
     */
    communication::can::messages::power_watchdog_kick_message
    handle(const communication::can::messages::power_watchdog_kick_message& msg) const {
        power_service_.kick_watchdog(msg.output());
        return msg;
    }

private:
    PowerService& power_service_;
};
/** @} */
} // namespace communication::can::handlers
