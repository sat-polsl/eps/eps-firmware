#pragma once

#include "communication/can/messages/power_watchdog_set_timeout_message.h"
#include "power_service/power_service_concept.h"

namespace communication::can::handlers {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power watchdog set timeout handler
 */
template<power_service::power_service_concept PowerService>
class power_watchdog_set_timeout_handler {
public:
    /**
     * @brief Constructor
     * @param power_service Power service to handle
     */
    explicit power_watchdog_set_timeout_handler(PowerService& power_service) :
        power_service_(power_service) {}

    /**
     * @brief Handles power set watchdog timeout message
     * @param msg power set watchdog timeout message
     */
    communication::can::messages::power_watchdog_set_timeout_message
    handle(const communication::can::messages::power_watchdog_set_timeout_message& msg) const {
        power_service_.set_watchdog_timeout(msg.output(), msg.timeout());
        return msg;
    }

private:
    PowerService& power_service_;
};
/** @} */
} // namespace communication::can::handlers
