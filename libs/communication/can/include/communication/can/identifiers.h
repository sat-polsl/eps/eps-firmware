#pragma once
#include <cstdint>
#include "communication/can/enums.h"
#include "satext/type_traits.h"
#include "spl/peripherals/can/concepts/enums.h"

namespace communication::can::identifiers {

consteval auto create_message_id(communication::can::message_id id,
                                 communication::can::message_type type) {
    auto message_type_shift = 2;
    auto identifiers_shift = 8;
    std::uint32_t result(
        (satext::to_underlying_type(id) << message_type_shift | satext::to_underlying_type(type))
        << identifiers_shift);
    return result;
}

struct echo {
    static constexpr std::uint32_t request_id = create_message_id(
        communication::can::message_id::echo, communication::can::message_type::request);
    static constexpr std::uint32_t response_id = create_message_id(
        communication::can::message_id::echo, communication::can::message_type::response);
    static constexpr auto filter = spl::peripherals::can::concepts::filter{0};
};

struct power_output_on {
    static constexpr std::uint32_t request_id = create_message_id(
        communication::can::message_id::power_output_on, communication::can::message_type::request);
    static constexpr std::uint32_t response_id =
        create_message_id(communication::can::message_id::power_output_on,
                          communication::can::message_type::response);
    static constexpr auto filter = spl::peripherals::can::concepts::filter{1};
};

struct power_output_off {
    static constexpr std::uint32_t request_id =
        create_message_id(communication::can::message_id::power_output_off,
                          communication::can::message_type::request);
    static constexpr std::uint32_t response_id =
        create_message_id(communication::can::message_id::power_output_off,
                          communication::can::message_type::response);
    static constexpr auto filter = spl::peripherals::can::concepts::filter{2};
};

struct power_watchdog_enable {
    static constexpr std::uint32_t request_id =
        create_message_id(communication::can::message_id::power_watchdog_enable,
                          communication::can::message_type::request);
    static constexpr std::uint32_t response_id =
        create_message_id(communication::can::message_id::power_watchdog_enable,
                          communication::can::message_type::response);
    static constexpr auto filter = spl::peripherals::can::concepts::filter{3};
};

struct power_watchdog_disable {
    static constexpr std::uint32_t request_id =
        create_message_id(communication::can::message_id::power_watchdog_disable,
                          communication::can::message_type::request);
    static constexpr std::uint32_t response_id =
        create_message_id(communication::can::message_id::power_watchdog_disable,
                          communication::can::message_type::response);
    static constexpr auto filter = spl::peripherals::can::concepts::filter{4};
};

struct power_watchdog_set_timeout {
    static constexpr std::uint32_t request_id =
        create_message_id(communication::can::message_id::power_watchdog_set_timeout,
                          communication::can::message_type::request);
    static constexpr std::uint32_t response_id =
        create_message_id(communication::can::message_id::power_watchdog_set_timeout,
                          communication::can::message_type::response);
    static constexpr auto filter = spl::peripherals::can::concepts::filter{5};
};

struct power_watchdog_kick {
    static constexpr std::uint32_t request_id =
        create_message_id(communication::can::message_id::power_watchdog_kick,
                          communication::can::message_type::request);
    static constexpr std::uint32_t response_id =
        create_message_id(communication::can::message_id::power_watchdog_kick,
                          communication::can::message_type::response);
    static constexpr auto filter = spl::peripherals::can::concepts::filter{6};
};

struct get_telemetry {
    static constexpr std::uint32_t request_id = create_message_id(
        communication::can::message_id::get_telemetry, communication::can::message_type::request);
    static constexpr std::uint32_t response_id = create_message_id(
        communication::can::message_id::get_telemetry, communication::can::message_type::response);
    static constexpr auto filter = spl::peripherals::can::concepts::filter{7};
};

} // namespace communication::can::identifiers
