#pragma once
#include <array>
#include <cstddef>
#include <cstdint>

namespace communication::can::messages {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Echo message
 */
class echo_message {
public:
    /**
     * @brief Default constructor;
     */
    echo_message() = default;

    /**
     * @brief Constructor.
     * @param buffer Array with message.
     * @param size Message size.
     */
    echo_message(std::array<std::byte, 8> buffer, std::size_t size) :
        buffer_{buffer},
        size_{size} {}

    /**
     * @brief Returns array with message.
     * @return Array with message.
     */
    [[nodiscard]] inline std::array<std::byte, 8> buffer() const { return buffer_; }

    /**
     * @brief Returns message size.
     * @return Message size.
     */
    [[nodiscard]] inline std::size_t size() const { return size_; };

private:
    std::array<std::byte, 8> buffer_{};
    std::size_t size_{};
};
} // namespace communication::can::messages
