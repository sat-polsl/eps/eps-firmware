#pragma once
#include "telemetry/enums.h"

namespace communication::can::messages {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Get telemetry request message.
 */
class get_telemetry_request {
public:
    /**
     * @brief Default constructor.
     */
    get_telemetry_request() = default;

    /**
     * @brief Constructor.
     * @param id Telemetry ID.
     */
    explicit get_telemetry_request(telemetry::telemetry_id id) : id_{id} {}

    /**
     * @brief Returns telemetry ID.
     * @return Telemetry ID.
     */
    [[nodiscard]] inline telemetry::telemetry_id id() const { return id_; };

private:
    telemetry::telemetry_id id_;
};
} // namespace communication::can::messages
