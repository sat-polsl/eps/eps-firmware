#pragma once
#include <cstdint>
#include "telemetry/types.h"

namespace communication::can::messages {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Get telemetry response message.
 */
class get_telemetry_response {
public:
    /**
     * @brief Default constructor/
     */
    get_telemetry_response() = default;

    /**
     * @brief Constructor.
     * @param id Telemetry ID.
     * @param value Telemetry value.
     */
    explicit get_telemetry_response(telemetry::telemetry_id id, telemetry::telemetry_value value) :
        id_{id},
        value_{value} {}

    /**
     * @brief Returns telemetry ID.
     * @return Telemetry ID.
     */
    [[nodiscard]] inline telemetry::telemetry_id id() const { return id_; };

    /**
     * @brief Returns telemetry value.
     * @return Telemetry value.
     */
    [[nodiscard]] inline telemetry::telemetry_value value() const { return value_; };

private:
    telemetry::telemetry_id id_{};
    telemetry::telemetry_value value_{};
};
} // namespace communication::can::messages
