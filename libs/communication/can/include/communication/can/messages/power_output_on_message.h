#pragma once
#include <array>
#include <cstddef>
#include <cstdint>
#include "power_service/enums.h"

namespace communication::can::messages {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power output on message
 */
class power_output_on_message {
public:
    /**
     * @brief Default constructor;
     */
    power_output_on_message() = default;

    /**
     * @brief Constructor.
     * @param output Power output.
     */
    explicit power_output_on_message(power_service::power_output output) : output_{output} {}

    /**
     * @brief Returns power output.
     * @return Power output.
     */
    [[nodiscard]] inline power_service::power_output output() const { return output_; };

private:
    power_service::power_output output_;
};
} // namespace communication::can::messages
