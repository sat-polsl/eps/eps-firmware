#pragma once
#include "power_service/enums.h"

namespace communication::can::messages {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power kick watchdog message
 */

class power_watchdog_kick_message {
public:
    power_watchdog_kick_message() = default;

    explicit power_watchdog_kick_message(power_service::power_output output) : output_(output) {}

    /**
     * @brief Returns power output
     * @return Power output
     */
    [[nodiscard]] inline power_service::power_output output() const { return output_; }

private:
    power_service::power_output output_;
};
/** @} */
} // namespace communication::can::messages
