#pragma once
#include <cstddef>
#include "power_service/enums.h"
#include "satos/chrono.h"

namespace communication::can::messages {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief Power set watchdog timeout message
 */

class power_watchdog_set_timeout_message {
public:
    power_watchdog_set_timeout_message() = default;

    explicit power_watchdog_set_timeout_message(power_service::power_output output,
                                                satos::chrono::seconds watchdog_timeout) :
        output_(output),
        watchdog_timeout_(watchdog_timeout) {}

    /**
     * @brief Returns power output
     * @return Power output
     */
    [[nodiscard]] inline power_service::power_output output() const { return output_; }

    /**
     * @brief Returns set timeout value
     * @return timeout
     */
    [[nodiscard]] inline satos::chrono::seconds timeout() const { return watchdog_timeout_; }

private:
    power_service::power_output output_;
    satos::chrono::seconds watchdog_timeout_;
};
/** @} */
} // namespace communication::can::messages
