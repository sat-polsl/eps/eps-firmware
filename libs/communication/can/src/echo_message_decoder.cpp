#include "communication/can/decoders/echo_message_decoder.h"

namespace communication::can::decoders {

echo_message_decoder::decoder_base::decoded_return_type
echo_message_decoder::decode(const spl::peripherals::can::concepts::message& msg) {
    return communication::can::messages::echo_message{msg.data, msg.size};
}

} // namespace communication::can::decoders
