#include "communication/can/encoders/echo_message_encoder.h"

namespace communication::can::encoders {

communication::status
echo_message_encoder::encode(const messages::echo_message& msg,
                             spl::peripherals::can::concepts::message& result) {
    result.data = msg.buffer();
    result.size = static_cast<std::uint8_t>(msg.size());
    return communication::status::ok;
}

} // namespace communication::can::encoders
