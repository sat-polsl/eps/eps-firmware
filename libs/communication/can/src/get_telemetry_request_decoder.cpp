#include "communication/can/decoders/get_telemetry_request_decoder.h"
#include <cstddef>
#include "satext/struct.h"
#include "telemetry/enums.h"

namespace communication::can::decoders {

using namespace satext::struct_literals;

get_telemetry_request_decoder::decoded_return_type
get_telemetry_request_decoder::decode(const spl::peripherals::can::concepts::message& msg) {
    if (msg.size != 2) {
        return satext::unexpected{communication::status::decoder_error};
    }

    return satext::unpack(">H"_fmt, std::span(msg.data.data(), msg.size))
        .map_error([](auto) { return communication::status::decoder_error; })
        .map([](auto unpacked) {
            auto [id] = unpacked;
            return communication::can::messages::get_telemetry_request{telemetry::telemetry_id{id}};
        });
}

} // namespace communication::can::decoders
