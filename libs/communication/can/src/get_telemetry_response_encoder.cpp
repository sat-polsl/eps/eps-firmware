#include "communication/can/encoders/get_telemetry_response_encoder.h"
#include "telemetry/encode.h"

namespace communication::can::encoders {
communication::status get_telemetry_response_encoder::encode(
    const communication::can::messages::get_telemetry_response& msg,
    spl::peripherals::can::concepts::message& result) {

    result.size = telemetry::encode(msg.id(), msg.value(), result.data);
    return communication::status::ok;
}
} // namespace communication::can::encoders
