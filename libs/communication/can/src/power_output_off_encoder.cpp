#include "communication/can/encoders/power_output_off_encoder.h"

namespace communication::can::encoders {

communication::status
power_output_off_encoder::encode(const messages::power_output_off_message& msg,
                                 spl::peripherals::can::concepts::message& result) {
    result.data[0] = std::byte(msg.output());
    result.size = 1;
    return communication::status::ok;
}

} // namespace communication::can::encoders
