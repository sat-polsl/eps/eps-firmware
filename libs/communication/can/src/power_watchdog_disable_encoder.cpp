#include "communication/can/encoders/power_watchdog_disable_encoder.h"

namespace communication::can::encoders {

communication::status
power_watchdog_disable_encoder::encode(const messages::power_watchdog_disable_message& msg,
                                       spl::peripherals::can::concepts::message& result) {
    result.data[0] = std::byte(msg.output());
    result.size = 1;
    return communication::status::ok;
}

} // namespace communication::can::encoders
