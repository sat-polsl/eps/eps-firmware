#include "communication/can/decoders/power_watchdog_enable_decoder.h"

namespace communication::can::decoders {

power_watchdog_enable_decoder::decoded_return_type
power_watchdog_enable_decoder::decode(const spl::peripherals::can::concepts::message& msg) {
    if (msg.size != 1) {
        return satext::unexpected{communication::status::decoder_error};
    }
    return communication::can::messages::power_watchdog_enable_message{
        static_cast<power_service::power_output>(msg.data[0])};
}

} // namespace communication::can::decoders
