#include "communication/can/decoders/power_watchdog_set_timeout_decoder.h"

namespace communication::can::decoders {

power_watchdog_set_timeout_decoder::decoded_return_type
power_watchdog_set_timeout_decoder::decode(const spl::peripherals::can::concepts::message& msg) {
    constexpr std::uint8_t Byte_size = 8;

    if (msg.size != 3) {
        return satext::unexpected{communication::status::decoder_error};
    }

    satos::chrono::seconds watchdog_timeout((static_cast<std::uint32_t>(msg.data[1]) << Byte_size) |
                                            static_cast<std::uint32_t>(msg.data[2]));

    return communication::can::messages::power_watchdog_set_timeout_message{
        static_cast<power_service::power_output>(msg.data[0]), watchdog_timeout};
}

} // namespace communication::can::decoders
