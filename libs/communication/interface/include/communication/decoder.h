#pragma once
#include <concepts>
#include <optional>
#include <variant>
#include "communication/enums.h"
#include "communication/types.h"
#include "satext/expected.h"
#include "satext/inplace_function.h"

namespace communication {

// clang-format off
template<typename T, typename Buffer, typename Id>
concept decoder_concept = requires(Buffer buffer) {
    { T::decode(buffer) } -> std::same_as<typename T::decoded_return_type>;
} && std::equality_comparable_with<decltype(T::id), Id>;
// clang-format on

/**
 * @ingroup communication
 * @{
 */

/**
 * @brief Decoder container class
 * @tparam Buffer Buffer type
 * @tparam Id Id type
 * @tparam Decoders Specific decoders
 */
template<typename Buffer, typename Id, decoder_concept<Buffer, Id>... Decoders>
class decoder {
public:
    using Input = input_t<Buffer>;
    /**
     * @brief Constructor
     * @param id_compare function to compare buffer id with signature `std::optional<Buffer>(const
     * Buffer, Id)`
     */
    explicit decoder(
        satext::inplace_function<std::optional<std::remove_reference_t<Buffer>>(Input, Id)>
            id_compare) :
        id_compare_{id_compare} {}

    /**
     * @brief `std::variant` with decoded types
     */
    using decoded = std::variant<typename Decoders::type...>;

    /**
     * @brief Tries to decode message in given buffer
     * @param buffer buffer to decode
     * @return decoded type on success, status otherwise
     */
    inline satext::expected<decoded, communication::status> decode(Input buffer) const {
        return decode<Decoders...>(buffer);
    }

private:
    satext::inplace_function<std::optional<std::remove_reference_t<Buffer>>(Input, Id), 32>
        id_compare_;

    template<typename Head, typename... Tail>
    inline satext::expected<decoded, communication::status> decode(Input buffer) const {
        if (auto payload = id_compare_(buffer, Head::id); payload.has_value()) {
            return Head::decode(*payload);
        } else {
            if constexpr (sizeof...(Tail)) {
                return decode<Tail...>(buffer);
            } else {
                return satext::unexpected{communication::status::unsupported_message};
            }
        }
    }
};

/** @} */

} // namespace communication
