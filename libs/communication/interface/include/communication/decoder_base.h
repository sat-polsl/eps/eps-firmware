#pragma once
#include "communication/enums.h"
#include "satext/expected.h"

namespace communication {

/**
 * @ingroup communication
 * @{
 */

/**
 * @brief Decoder base
 * @tparam Type Message type
 * @tparam IdType Message Id type
 * @tparam Id Message Id value
 */
template<typename Type, typename IdType, auto Id>
class decoder_base {
public:
    /**
     * @brief Message type
     */
    using type = Type;

    /**
     * @brief Decoded return type
     */
    using decoded_return_type = satext::expected<type, communication::status>;

    /**
     * @brief Message Id
     */
    static constexpr IdType id{Id};
};

/** @} */
} // namespace communication
