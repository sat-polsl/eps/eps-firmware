#pragma once
#include <concepts>
#include <optional>
#include <variant>
#include "communication/enums.h"
#include "satext/expected.h"
#include "satext/inplace_function.h"
#include "satext/overload.h"

namespace communication {

// clang-format off
template<typename T, typename Buffer, typename Id>
concept encoder_concept = requires(typename T::type message, Buffer buffer) {
    { T::encode(message, buffer) } -> std::same_as<communication::status>;
} && std::equality_comparable_with<decltype(T::id), Id>;
// clang-format on

/**
 * @ingroup communication
 * @{
 */

/**
 * @brief Encoder container class
 * @tparam Buffer Buffer type
 * @tparam Id Id type
 * @tparam Decoders Specific encoders
 */
template<typename Buffer, typename Id, encoder_concept<Buffer, Id>... Encoders>
class encoder {
public:
    /**
     * @brief `std::variant` with encoded types
     */
    using encodable = std::variant<std::monostate, typename Encoders::type...>;

    /**
     * @brief Constructor
     * @param id_encoder
     */
    explicit encoder(
        satext::inplace_function<std::optional<std::remove_reference_t<Buffer>>(Buffer, Id)>
            id_encoder) :
        id_encoder_{id_encoder} {}

    /**
     * @brief Encodes single message
     * @param message Message to encode
     * @param buffer Buffer with encoded message
     * @return Communication status
     */
    inline communication::status encode(const encodable& message, Buffer buffer) {
        return encode<Encoders...>(message, buffer);
    }

private:
    satext::inplace_function<std::optional<std::remove_reference_t<Buffer>>(Buffer, Id), 32>
        id_encoder_;

    template<typename Head, typename... Tail>
    inline communication::status encode(const encodable& message, Buffer buffer) {
        if (auto* p = std::get_if<typename Head::type>(&message)) {
            if (auto payload = id_encoder_(buffer, Head::id); payload.has_value()) {
                auto status = Head::encode(*p, *payload);
                buffer = *payload;
                return status;
            } else {
                return communication::status::encoder_error;
            }
        } else {
            if constexpr (sizeof...(Tail)) {
                return encode<Tail...>(message, buffer);
            } else {
                return communication::status::unsupported_message;
            }
        }
    }
};

/** @} */

} // namespace communication
