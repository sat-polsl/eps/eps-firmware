#pragma once
#include "communication/enums.h"
#include "satext/expected.h"

namespace communication {

/**
 * @ingroup communication
 * @{
 */

/**
 * @brief Encoder base
 * @tparam Type Message type
 * @tparam IdType Message Id type
 * @tparam Id Message Id value
 */
template<typename Type, typename IdType, auto Id>
class encoder_base {
public:
    /**
     * @brief Message type
     */
    using type = Type;

    /**
     * @brief Message Id
     */
    static constexpr IdType id{Id};
};

/** @} */
} // namespace communication
