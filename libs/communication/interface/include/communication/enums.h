#pragma once

namespace communication {

/**
 * @ingroup communication
 * @{
 */

/**
 * @brief Communication status enumeration
 */
enum class status { ok, unsupported_message, encoder_error, decoder_error, void_handle };

/** @} */
} // namespace communication
