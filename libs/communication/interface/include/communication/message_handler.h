#pragma once
#include <optional>
#include "communication/enums.h"
#include "communication/types.h"
#include "satext/expected.h"
#include "satext/overload.h"

namespace communication {

/**
 * @ingroup communication
 * @{
 */

/**
 * @brief Message handler
 * @tparam Buffer Buffer type
 * @tparam Decoder Decoder
 * @tparam Encoder Encoder
 * @tparam MessageHandler Message handler
 */
template<typename Buffer, typename Decoder, typename Encoder, typename... Handlers>
class message_handler : public Handlers... {
public:
    using Input = input_t<Buffer>;
    /**
     * @brief Constructor.
     * @param decoder decoder
     * @param encoder encoder
     * @param message_handler message handler
     */
    message_handler(Decoder decoder, Encoder encoder, Handlers&&... handlers) :
        Handlers{std::move(handlers)}...,
        decoder_{decoder},
        encoder_{encoder} {}

    /**
     * @brief Handlers one message
     * @param input input buffer
     * @param output output buffer
     * @return communication status
     */
    communication::status handle(Input input, Buffer output) {
        auto result =
            decoder_.decode(input)
                .and_then([this](const auto& decoded_message) {
                    return std::visit(
                        satext::overload([this](const auto& message) {
                            typename Encoder::encodable handle_result{std::monostate{}};
                            if constexpr (std::is_same_v<decltype(handle(message)), void>) {
                                handle(message);
                            } else {
                                handle_result = handle(message);
                            }
                            return satext::expected<typename Encoder::encodable,
                                                    communication::status>{handle_result};
                        }),
                        decoded_message);
                })
                .map([this, &output](auto& message_to_encode) {
                    if (auto* p = std::get_if<std::monostate>(&message_to_encode)) {
                        return communication::status::void_handle;
                    }
                    return encoder_.encode(message_to_encode, output);
                });
        return result.value_or(result.error());
    }

    /**
     * Returns decoder
     * @return decoder
     */
    inline Decoder& decoder() { return decoder_; }

    /**
     * Returns encoder
     * @return encoder
     */
    inline Encoder& encoder() { return encoder_; }

private:
    using Handlers::handle...;

    Decoder decoder_;
    Encoder encoder_;
};
} // namespace communication
