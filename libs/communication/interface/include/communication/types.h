#pragma once
#include <type_traits>

namespace communication {
namespace detail {
template<typename T>
struct input {
    using type = T;
};

template<typename T>
struct input<T&> {
    using type = std::add_lvalue_reference_t<std::add_const_t<std::remove_reference_t<T>>>;
};
} // namespace detail

template<typename T>
using input_t = typename detail::input<T>::type;
} // namespace communication
