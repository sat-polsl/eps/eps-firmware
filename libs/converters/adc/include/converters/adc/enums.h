#pragma once
#include "devices/adc/enums.h"

namespace converters::adc {

/**
 * @ingroup adc_converters
 * @{
 */

/**
 * @brief Voltage ADC channels.
 */
struct voltage_channel {
    static constexpr auto v12 = devices::adc::channel::channel0;
    static constexpr auto v5_l1 = devices::adc::channel::channel1;
    static constexpr auto v5_l2 = devices::adc::channel::channel2;
    static constexpr auto v3v3_l1 = devices::adc::channel::channel3;
    static constexpr auto v3v3_l2 = devices::adc::channel::channel4;
    static constexpr auto v3v3_l3 = devices::adc::channel::channel5;
    static constexpr auto v3v3_l4 = devices::adc::channel::channel6;
};

/**
 * @brief Current ADC channels.
 */
struct current_channel {
    static constexpr auto ibms = devices::adc::channel::channel0;
    static constexpr auto imppt2 = devices::adc::channel::channel1;
    static constexpr auto imppt1 = devices::adc::channel::channel2;
    static constexpr auto vbms = devices::adc::channel::channel3;
    static constexpr auto vmmpt = devices::adc::channel::channel4;
    static constexpr auto i12 = devices::adc::channel::channel5;
    static constexpr auto i5_l1 = devices::adc::channel::channel6;
    static constexpr auto i5_l2 = devices::adc::channel::channel7;
    static constexpr auto i3v3_l2 = devices::adc::channel::channel8;
    static constexpr auto i3v3_l1 = devices::adc::channel::channel9;
    static constexpr auto i3v3_l4 = devices::adc::channel::channel10;
    static constexpr auto i3v3_l3 = devices::adc::channel::channel11;
};

/** @} */

} // namespace converters::adc
