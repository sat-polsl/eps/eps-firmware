#pragma once
#include <cstdint>
#include <span>
#include "devices/adc/enums.h"

namespace converters::adc {

/**
 * @ingroup adc_converters
 * @{
 */

/**
 * @brief Converts array of ADC reading to corresponding lines voltages/currents.
 * @param input readings buffer from ADC.
 * @return converted readings.
 */
std::span<std::uint16_t> convert_voltage_adc(std::span<std::uint16_t> input);

/**
 * @brief Converts single ADC channel reading to corresponding line voltages/currents.
 * @param input reading from ADC.
 * @param channel ADC channel.
 * @return converted reading.
 */
std::uint16_t convert_voltage_adc_channel(std::uint16_t input, devices::adc::channel channel);

/** @} */

} // namespace converters::adc
