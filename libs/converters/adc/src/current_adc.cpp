#include "converters/adc/current_adc.h"
#include <array>
#include "satext/ranges/enumerate.h"
#include "satext/type_traits.h"

namespace converters::adc {

constexpr std::size_t current_adc_channels = 12;
constexpr std::array<std::uint32_t, current_adc_channels> multiply_coefficient = {
    1, 1, 1, 166'000, 190'000, 1, 1, 1, 1, 1, 1, 1};

constexpr std::array<std::uint32_t, current_adc_channels> division_coefficient = {
    2, 2, 2, 66'000, 90'000, 2, 2, 2, 2, 2, 2, 2};

std::span<std::uint16_t> convert_current_adc(std::span<std::uint16_t> input) {
    for (auto&& [index, value] : satext::ranges::enumerate(input)) {
        value =
            static_cast<std::uint16_t>(static_cast<std::uint32_t>(value) *
                                       multiply_coefficient[index] / division_coefficient[index]);
    }
    return input;
}

std::uint16_t convert_current_adc_channel(std::uint16_t input, devices::adc::channel channel) {
    return static_cast<std::uint16_t>(static_cast<std::uint32_t>(input) *
                                      multiply_coefficient[satext::to_underlying_type(channel)] /
                                      division_coefficient[satext::to_underlying_type(channel)]);
}

} // namespace converters::adc
