#include "converters/adc/voltage_adc.h"
#include <array>
#include "satext/ranges/enumerate.h"
#include "satext/type_traits.h"

namespace converters::adc {

constexpr std::size_t voltage_adc_channels = 7;
constexpr std::array<std::uint32_t, voltage_adc_channels> multiply_coefficient = {
    120'000, 166'000, 254'000, 166'000, 254'000, 254'000, 254'000};

constexpr std::array<std::uint32_t, voltage_adc_channels> division_coefficient = {
    20'000, 66'000, 154'000, 66'000, 154'000, 154'000, 154'000};

std::span<std::uint16_t> convert_voltage_adc(std::span<std::uint16_t> input) {
    for (auto&& [index, value] : satext::ranges::enumerate(input)) {
        value =
            static_cast<std::uint16_t>(static_cast<std::uint32_t>(value) *
                                       multiply_coefficient[index] / division_coefficient[index]);
    }
    return input;
}

std::uint16_t convert_voltage_adc_channel(std::uint16_t input, devices::adc::channel channel) {
    return static_cast<std::uint16_t>(static_cast<std::uint32_t>(input) *
                                      multiply_coefficient[satext::to_underlying_type(channel)] /
                                      division_coefficient[satext::to_underlying_type(channel)]);
}

} // namespace converters::adc
