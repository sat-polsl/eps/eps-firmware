set(TARGET adc)

add_library(${TARGET} INTERFACE)

target_include_directories(${TARGET} INTERFACE
    include
    )

target_sources(${TARGET} PRIVATE
    include/devices/adc/adc.h
    )

target_link_libraries(${TARGET}
    INTERFACE
    spl::spi_concept
    spl::gpio_ll_concept
    satos::api
    satext::satext
    etl::etl
    )

add_mock_library(adc_mock
    INCLUDES
    include
    mock
    SOURCES
    mock/devices/adc/mock/adc.h
    )
