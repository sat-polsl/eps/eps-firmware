#pragma once
#include <algorithm>
#include <cstdint>
#include "devices/adc/enums.h"
#include "etl/power.h"
#include "satext/expected.h"
#include "satext/noncopyable.h"
#include "satext/type_traits.h"
#include "satos/clock.h"
#include "spl/drivers/spi/concepts/spi.h"
#include "spl/peripherals/gpio/concepts/gpio.h"

namespace devices::adc {

/**
 * @ingroup adc_device
 * @{
 */

/**
 * @brief Class for managing MAX116xx ADCs.
 *
 * [MAX11633
 * Datasheet](https://www.analog.com/media/en/technical-documentation/data-sheets/max11626-max11633.pdf)
 *
 * [MAX11639
 * Datasheet](https://www.analog.com/media/en/technical-documentation/data-sheets/max11638-max11643.pdf)
 * @tparam GPIO Gpio peripheral type.
 * @tparam SPI SPI driver type.
 * @tparam Channels Number of ADC channels.
 * @tparam Resolution ADC bit resolution.
 */
template<spl::peripherals::gpio::concepts::gpio GPIO,
         spl::drivers::spi::concepts::spi_vectored<GPIO> SPI,
         std::size_t Channels,
         std::size_t Resolution>
class adc : private satext::noncopyable {
public:
    static constexpr auto channels = Channels;
    static constexpr auto resolution = Resolution;

    /**
     * @brief Constructor.
     * @param spi Reference to SPI driver.
     * @param gpio Reference to ADC chip select GPIO.
     * @param timeout Communication timeout.
     */
    adc(SPI& spi, GPIO& gpio, satos::clock::duration timeout) :
        spi_{spi},
        gpio_{gpio},
        timeout_{timeout} {};

    /**
     * @brief Initializes ADC.
     */
    void initialize() {
        auto data = configuration_byte;
        std::array<std::span<std::byte>, 1> buffer{std::span(&data, 1)};
        spi_.transfer(gpio_, buffer, timeout_);
    }

    /**
     * @brief Resets ADC.
     */
    void reset() {
        auto data = reset_byte;
        std::array<std::span<std::byte>, 1> buffer{std::span(&data, 1)};
        spi_.transfer(gpio_, buffer, timeout_);
    }

    /**
     * @brief Reads raw reading from ADC channel.
     * @param ch Channel to read.
     * @return Raw ADC value on success, operation status otherwise.
     */
    satext::expected<std::uint16_t, spl::drivers::spi::status> read_channel_raw(channel ch) {
        std::array<std::byte, 3> data{};
        data[0] = conversion_byte | (std::byte(satext::to_underlying_type(ch)) << channel_shift);
        std::array<std::span<std::byte>, 1> buffer{data};

        if (auto status = spi_.transfer(gpio_, buffer, timeout_);
            status != spl::drivers::spi::status::ok) {
            return satext::unexpected{status};
        }

        auto result = static_cast<std::uint16_t>((static_cast<std::uint16_t>(data[1]) << 8) |
                                                 static_cast<std::uint16_t>(data[2]));

        return (result >> (max_resolution - resolution));
    }

    /**
     * @brief Reads raw readings from all ADC channel.
     * @return Array with raw ADC values on success, operation status otherwise.
     */
    satext::expected<std::array<std::uint16_t, channels>, spl::drivers::spi::status>
    read_all_raw() {
        auto data = create_conversion_buffer();

        std::array<std::span<std::byte>, 1> buffer{data};

        if (auto status = spi_.transfer(gpio_, buffer, timeout_);
            status != spl::drivers::spi::status::ok) {
            return satext::unexpected{status};
        }
        std::array<std::uint16_t, channels> result{};

        for (std::size_t i = 0; i < channels; i++) {
            result[i] = ((static_cast<std::uint16_t>(data[2 * i + 1]) << 8) |
                         static_cast<std::uint16_t>(data[2 * i + 2])) >>
                        (max_resolution - resolution);
        }

        return result;
    }

    /**
     * @brief Reads mV reading from ADC channel.
     * @param ch Channel to read.
     * @return ADC value in mV on success, operation status otherwise.
     */
    satext::expected<std::uint16_t, spl::drivers::spi::status> read_channel(channel ch) {
        return read_channel_raw(ch).map(raw_to_mv);
    }

    /**
     * @brief Reads mV readings from all ADC channel.
     * @return Array with ADC values in mV on success, operation status otherwise.
     */
    satext::expected<std::array<std::uint16_t, channels>, spl::drivers::spi::status> read_all() {
        return read_all_raw().map([](std::array<std::uint16_t, channels> values) {
            std::ranges::transform(values, values.begin(), raw_to_mv);
            return values;
        });
    }

private:
    SPI& spi_;
    GPIO& gpio_;
    satos::clock::duration timeout_;

    static constexpr std::byte configuration_byte{0x70};
    static constexpr std::byte reset_byte{0x18};
    static constexpr std::byte conversion_byte{0x86};
    static constexpr std::size_t channel_shift = 3;
    static constexpr std::uint32_t max_value = etl::power_v<2, resolution> - 1;
    static constexpr std::uint32_t reference_mv = 2500;
    static constexpr std::size_t max_resolution = 12;

    static_assert(Resolution <= max_resolution, "Resolution is over 12 bits.");

    static auto raw_to_mv(std::uint16_t value) {
        return static_cast<std::uint16_t>(value * reference_mv / max_value);
    }

    static consteval auto create_conversion_buffer() {
        std::array<std::byte, (channels * 2) + 1> result{};

        for (std::size_t i = 0; i < channels; i++) {
            result[2 * i] = conversion_byte | (std::byte(i) << channel_shift);
        }

        return result;
    }
};

/** @} */

} // namespace devices::adc
