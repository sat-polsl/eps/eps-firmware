#pragma once
#include <array>
#include <concepts>
#include "devices/adc/enums.h"
#include "satext/expected.h"
#include "spl/drivers/spi/enums.h"

namespace devices::adc {

// clang-format off
template<typename T>
concept adc_concept = requires(T adc, channel ch) {
    { adc.read_channel_raw(ch) } -> std::same_as<satext::expected<std::uint16_t, spl::drivers::spi::status>>;
    { adc.read_all_raw() } -> std::same_as<satext::expected<std::array<std::uint16_t, T::channels>, spl::drivers::spi::status>>;
    { adc.read_channel(ch) } -> std::same_as<satext::expected<std::uint16_t, spl::drivers::spi::status>>;
    { adc.read_all() } -> std::same_as<satext::expected<std::array<std::uint16_t, T::channels>, spl::drivers::spi::status>>;
};
// clang-format on

} // namespace devices::adc
