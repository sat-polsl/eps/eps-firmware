#pragma once

namespace devices::adc {

/**
 * @ingroup adc_device
 * @{
 */

/**
 * @brief ADC channels enumeration.
 */
enum class channel {
    channel0 = 0,
    channel1 = 1,
    channel2 = 2,
    channel3 = 3,
    channel4 = 4,
    channel5 = 5,
    channel6 = 6,
    channel7 = 7,
    channel8 = 8,
    channel9 = 9,
    channel10 = 10,
    channel11 = 11,
    channel12 = 12,
    channel13 = 13,
    channel14 = 14,
    channel15 = 15
};

/** @} */

} // namespace devices::adc
