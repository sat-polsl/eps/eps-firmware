#pragma once
#include <cstdint>
#include "gmock/gmock.h"
#include "devices/adc/adc_concept.h"
#include "satext/expected.h"

namespace devices::adc::mock {
template<std::size_t Channels>
struct adc {
    static constexpr auto channels = Channels;
    using result = satext::expected<std::uint16_t, spl::drivers::spi::status>;
    using array_result =
        satext::expected<std::array<std::uint16_t, channels>, spl::drivers::spi::status>;

    MOCK_METHOD(result, read_channel_raw, (devices::adc::channel));
    MOCK_METHOD(array_result, read_all_raw, ());
    MOCK_METHOD(result, read_channel, (devices::adc::channel));
    MOCK_METHOD(array_result, read_all, ());
};

static_assert(devices::adc::adc_concept<adc<8>>);
} // namespace devices::adc::mock
