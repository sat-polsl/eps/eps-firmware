#pragma once
#include <array>

namespace devices::fram {

/**
 * @addtogroup fram
 * @{
 */

/**
 * @brief FRAM Opcodes enumeration
 */
enum class opcode {
    read_id = 0x9f,
    write_enable = 0x06,
    write_disable = 0x04,
    write = 0x02,
    read = 0x03,
    read_status = 0x05
};

/**
 * @brief FRAM status register bits
 */
enum class status_register_bits { write_enabled = 0x02 };

/**
 * @brief FRAM memory ids
 */
struct memory_id {
    static constexpr std::array<std::byte, 9> cy15b104q = {std::byte{0x7f},
                                                           std::byte{0x7f},
                                                           std::byte{0x7f},
                                                           std::byte{0x7f},
                                                           std::byte{0x7f},
                                                           std::byte{0x7f},
                                                           std::byte{0xc2},
                                                           std::byte{0x26},
                                                           std::byte{0x08}};
};

/** @} */
} // namespace devices::fram
