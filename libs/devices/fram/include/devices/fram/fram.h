#pragma once
#include <variant>
#include "devices/fram/enums.h"
#include "devices/fram/status_register.h"
#include "satext/expected.h"
#include "spl/drivers/spi/concepts/spi.h"
#include "spl/peripherals/gpio/concepts/gpio.h"

namespace devices::fram {

/**
 * @addtogroup fram
 * @{
 */

/**
 * @brief Cypress FRAM driver
 * @tparam GPIO GPIO Peripheral
 * @tparam SPI SPI Vectored Driver
 * @tparam Id Memory ID
 */
template<spl::peripherals::gpio::concepts::gpio GPIO,
         spl::drivers::spi::concepts::spi_vectored<GPIO> SPI,
         std::array<std::byte, 9> Id>
class fram {
public:
    /**
     * @brief Constructor
     * @param spi_driver SPI Driver
     * @param chip_select Chip select
     */
    fram(SPI& spi_driver, GPIO& chip_select) : spi_{spi_driver}, chip_select_{chip_select} {}

    /**
     * @brief Probes memory
     * @return true if probe was successful, false otherwise
     */
    bool probe() {
        auto read_id_opcode = std::byte(opcode::read_id);
        std::array<std::byte, 9> id_buffer{};

        std::array<std::span<const std::byte>, 1> in = {std::span(&read_id_opcode, 1)};
        std::array<std::span<std::byte>, 1> out = {id_buffer};

        auto status = spi_.write_read(chip_select_, in, out);
        if (status != spl::drivers::spi::status::ok) {
            return false;
        }

        return id_buffer == Id;
    }

    /**
     * @brief Reads status register
     * @return status register value on success, unexpected otherwise
     */
    satext::expected<status_register, std::monostate> read_status() {
        auto read_status_opcode = std::byte(opcode::read_status);

        std::byte status_value;
        std::array<std::span<const std::byte>, 1> read_status_in = {
            std::span(&read_status_opcode, 1) //
        };
        std::array<std::span<std::byte>, 1> read_status_out = {
            std::span(&status_value, 1) //
        };

        if (auto spi_status = spi_.write_read(chip_select_, read_status_in, read_status_out);
            spi_status == spl::drivers::spi::status::ok) {
            return status_register{status_value};
        } else {
            return satext::unexpected{std::monostate{}};
        }
    }

    /**
     * @brief Enables write to memory
     * @return true if operation was successful, false otherwise
     */
    bool write_enable() {
        auto write_enable_opcode = std::byte(opcode::write_enable);

        std::array<std::span<const std::byte>, 1> write_enable_in = {
            std::span(&write_enable_opcode, 1) //
        };

        if (auto spi_status = spi_.write(chip_select_, write_enable_in);
            spi_status != spl::drivers::spi::status::ok) {
            return false;
        }

        auto fram_status = read_status();
        return fram_status.has_value() ? fram_status->write_enabled() : false;
    }

    /**
     * @brief Disables write to memory
     * @return true if operation was successful, false otherwise
     */
    bool write_disable() {
        auto write_disable_opcode = std::byte(opcode::write_disable);

        std::array<std::span<const std::byte>, 1> write_disable_in = {
            std::span(&write_disable_opcode, 1) //
        };

        if (auto spi_status = spi_.write(chip_select_, write_disable_in);
            spi_status != spl::drivers::spi::status::ok) {
            return false;
        }

        auto fram_status = read_status();
        return fram_status.has_value() ? !fram_status->write_enabled() : false;
    }

    /**
     * @brief Reads memory from given address
     * @param address Memory address
     * @param buffer Buffer where data will be stored
     * @return span with data if operation was successful, unexpected otherwise
     */
    satext::expected<std::span<const std::byte>, std::monostate> read(std::uint32_t address,
                                                                      std::span<std::byte> buffer) {
        std::array<std::byte, 4> read_command = {
            std::byte(opcode::read),           //
            std::byte((address >> 16) & 0x07), //
            std::byte((address >> 8) & 0xff),  //
            std::byte(address & 0xff)          //
        };

        std::array<std::span<const std::byte>, 1> in = {read_command};
        std::array<std::span<std::byte>, 1> out = {buffer};

        auto status = spi_.write_read(chip_select_, in, out);
        if (status == spl::drivers::spi::status::ok) {
            return buffer;
        } else {
            return satext::unexpected{std::monostate{}};
        }
    }

    /**
     * @brief Write data to memory at given address
     * @param address Memory address
     * @param buffer Buffer to write
     * @return true if operation was successful, false otherwise
     */
    bool write(std::uint32_t address, std::span<const std::byte> buffer) {
        std::array<std::byte, 4> write_command = {
            std::byte(opcode::write),          //
            std::byte((address >> 16) & 0x07), //
            std::byte((address >> 8) & 0xff),  //
            std::byte(address & 0xff)          //
        };

        std::array<std::span<const std::byte>, 2> in = {write_command, buffer};

        return spi_.write(chip_select_, in) == spl::drivers::spi::status::ok;
    }

private:
    SPI& spi_;
    GPIO& chip_select_;
};

/** @} */
} // namespace devices::fram
