#pragma once
#include <cstdint>
#include <span>
#include <variant>
#include "devices/fram/status_register.h"
#include "satext/expected.h"

namespace devices::fram {

// clang-format off
template<typename T>
concept fram_concept = requires(T fram, std::uint32_t address, std::span<std::byte> buffer,
                                std::span<const std::byte> const_buffer) {
    { fram.probe() } -> std::same_as<bool>;
    { fram.write_enable() } -> std::same_as<bool>;
    { fram.write_disable() } -> std::same_as<bool>;
    { fram.read_status() } -> std::same_as<satext::expected<status_register, std::monostate>>;
    { fram.read(address, buffer) } -> std::same_as<satext::expected<std::span<const std::byte>, std::monostate>>;
    { fram.write(address, const_buffer) } -> std::same_as<bool>;
};
// clang-format on

} // namespace devices::fram
