#pragma once
#include <cstddef>

namespace devices::fram {

/**
 * @addtogroup fram
 * @{
 */

/**
 * @brief Class holding FRAM status register value
 */
class status_register {
public:
    /**
     * @brief Contructor
     * @param reg raw status register value
     */
    explicit status_register(std::byte reg);

    /**
     * @brief Checks whether write operation is enabled
     * @return true if write operation is enabled, otherwise false
     */
    bool write_enabled() const;

private:
    std::byte register_;
};

/** @} */

} // namespace devices::fram
