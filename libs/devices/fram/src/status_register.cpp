#include "devices/fram/status_register.h"
#include "devices/fram/enums.h"

namespace devices::fram {

status_register::status_register(std::byte reg) : register_{reg} {}

bool status_register::write_enabled() const {
    return (register_ & std::byte(status_register_bits::write_enabled)) != std::byte{};
}

} // namespace devices::fram
