#pragma once

namespace devices::lm75b {

/**
 * @addtogroup lm75b
 * @{
 */

/**
 * @brief LM75B Pointer register
 */
enum class pointer_register {
    temperature = 0x00,
    configuration = 0x01,
    hysteresis = 0x02,
    os = 0x03
};

/**
 * @brief LM75B Configuration register fields
 */
enum class config_register { shutdown = 0x00, os_comp_int = 0x01, os_pol = 0x02 };

/**
 * @brief LM75B Overtemperature shutdown polarity
 */
enum class os_polarity : bool { active_low = false, active_high = true };

/**
 * @brief LM75B Overtemperature shutdown operation mode
 */
enum class os_mode : bool { comparator = false, interrupt = true };

/**
 * @brief LM75B Device operation mode
 */
enum class device_mode : bool { normal = false, shutdown = true };

/** @} */
} // namespace devices::lm75b
