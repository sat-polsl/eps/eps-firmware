#pragma once

#include "devices/lm75b/enums.h"
#include "satext/expected.h"
#include "satext/type_traits.h"
#include "spl/drivers/i2c/concepts/i2c.h"

namespace devices::lm75b {

/**
 * @addtogroup lm75b
 * @{
 */

/**
 * @brief LM75B thermometer driver
 * @tparam i2c I2C Driver
 */
template<spl::drivers::i2c::concepts::i2c I2C>
class lm75b {
public:
    /**
     * @brief Constructor
     * @param address Device I2C address
     * @param i2c_driver I2C Driver
     * @param timeout Timeout
     */
    lm75b(I2C& i2c, std::uint8_t address, satos::clock::duration timeout) :
        i2c_{i2c},
        address_{address},
        timeout_{timeout} {}

    /**
     * @brief Set the over-temperature shutdown polarity
     * @param os_polarity OS polarity - active high or active low
     * @return I2C status
     */
    bool set_os_polarity(os_polarity polarity) {
        return set_config_bit(config_register::os_pol, satext::to_underlying_type(polarity));
    }

    /**
     * @brief Set the over-temperature shutdown operation mode
     * @param mode OS mode - comparator or interrupt
     * @return I2C status
     */
    bool set_os_mode(os_mode mode) {
        return set_config_bit(config_register::os_comp_int, satext::to_underlying_type(mode));
    }

    /**
     * @brief Set the device operation mode
     * @param mode Device mode - normal or shutdown
     * @return I2C status
     */
    bool set_operation_mode(device_mode mode) {
        return set_config_bit(config_register::shutdown, satext::to_underlying_type(mode));
    }

    /**
     * @brief Read raw temperature data
     * @details Returns 11 bit temperature in degrees C, with 1/8 degree resolution. The 5 most
     * significant bits should be ignored.
     *
     * @return temperature if successful, unexpected otherwise
     */
    satext::expected<std::int16_t, spl::drivers::i2c::status> read_temperature_raw() {
        std::array<std::byte, 1> ptr{std::byte(pointer_register::temperature)};
        std::array<std::byte, 2> data{};

        return i2c_.write_read(address_, ptr, data, timeout_).map([](auto buffer) {
            return static_cast<std::int16_t>(static_cast<std::uint16_t>(buffer[0]) << 8 |
                                             static_cast<std::uint16_t>(buffer[1]));
        });
    }

    /**
     * @brief Read temperature
     * @details Returns temperature in degrees C * 10, with 0.1 degree resolution.
     *
     * @return temperature if successful, unexpected otherwise
     */
    satext::expected<std::int16_t, spl::drivers::i2c::status> read_temperature() {
        return read_temperature_raw().map(
            [](std::int16_t value) { return static_cast<std::int32_t>(value) * 10 / 256; });
    }

    /**
     * @brief Set the over-temperature shutdown threshold
     * @param threshold Set point in degrees C * 10, with 0.5 degree resolution
     * @return true if successful, false otherwise
     */
    bool set_os_threshold(std::int16_t threshold) {
        auto raw = static_cast<std::int32_t>(threshold) * 256 / 10;

        std::array<std::byte, 3> data{
            std::byte(pointer_register::os), std::byte((raw >> 8) & 0xff), std::byte(raw & 0xff)};
        return i2c_.write(address_, data, timeout_) == spl::drivers::i2c::status::ok;
    }

    /**
     * @brief Set the hysteresis
     * @param hysteresis Set point in degrees C * 10, with 0.5 degree resolution
     * @return true if successful, false otherwise
     */
    bool set_os_hysteresis(std::int16_t hysteresis) {
        auto raw = static_cast<std::int32_t>(hysteresis) * 256 / 10;

        std::array<std::byte, 3> data{std::byte(pointer_register::hysteresis),
                                      std::byte((raw >> 8) & 0xff),
                                      std::byte(raw & 0xff)};
        return i2c_.write(address_, data, timeout_) == spl::drivers::i2c::status::ok;
    }

private:
    bool set_config_bit(config_register bit, bool enabled) {
        std::array<std::byte, 2> buffer{std::byte(pointer_register::configuration)};

        return i2c_
            .write_read(
                address_, std::span(buffer.data(), 1), std::span(buffer.data() + 1, 1), timeout_)
            .map([this, &buffer, enabled, bit](std::span<const std::byte>) {
                if (enabled) {
                    buffer[1] |= std::byte(0x01 << satext::to_underlying_type(bit));
                } else {
                    buffer[1] &= ~std::byte(0x01 << satext::to_underlying_type(bit));
                }
                return i2c_.write(address_, buffer, timeout_) == spl::drivers::i2c::status::ok;
            })
            .value_or(false);
    }

    I2C& i2c_;
    std::uint8_t address_;
    satos::clock::duration timeout_;
};

} // namespace devices::lm75b
