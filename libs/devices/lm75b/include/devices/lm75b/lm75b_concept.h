#pragma once
#include <cstdint>
#include <variant>
#include "devices/lm75b/enums.h"
#include "satext/expected.h"
#include "spl/drivers/i2c/enums.h"

namespace devices::lm75b {

// clang-format off
template<typename T>
concept lm75b_concept = requires(T lm75b, os_polarity polarity, os_mode os, device_mode device, std::int16_t value) {
    { lm75b.set_os_polarity(polarity) } -> std::same_as<bool>;
    { lm75b.set_os_mode(os) } -> std::same_as<bool>;
    { lm75b.set_operation_mode(device) } -> std::same_as<bool>;
    { lm75b.set_os_threshold(value) } -> std::same_as<bool>;
    { lm75b.set_os_hysteresis(value) } -> std::same_as<bool>;
    { lm75b.read_temperature_raw() } -> std::same_as<satext::expected<std::int16_t, spl::drivers::i2c::status>>;
    { lm75b.read_temperature() } -> std::same_as<satext::expected<std::int16_t, spl::drivers::i2c::status>>;
};
// clang-format on

} // namespace devices::lm75b
