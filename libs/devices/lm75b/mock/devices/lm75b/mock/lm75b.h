#pragma once
#include <cstdint>
#include "gmock/gmock.h"
#include "devices/lm75b/lm75b_concept.h"
#include "satext/expected.h"

namespace devices::lm75b::mock {
struct lm75b {
    using read_temperature_result = satext::expected<std::int16_t, spl::drivers::i2c::status>;
    MOCK_METHOD(bool, set_os_polarity, (os_polarity));
    MOCK_METHOD(bool, set_os_mode, (os_mode));
    MOCK_METHOD(bool, set_operation_mode, (device_mode));
    MOCK_METHOD(bool, set_os_threshold, (std::int16_t));
    MOCK_METHOD(bool, set_os_hysteresis, (std::int16_t));
    MOCK_METHOD(read_temperature_result, read_temperature_raw, ());
    MOCK_METHOD(read_temperature_result, read_temperature, ());
};

static_assert(devices::lm75b::lm75b_concept<lm75b>);
} // namespace devices::lm75b::mock
