#pragma once
#include "spl/peripherals/gpio/concepts/gpio.h"

namespace devices::mcp73871 {

/**
 * @ingroup mcp73871
 * @{
 */

/**
 * @brief MCP73871 GPIO configuration.
 * @tparam GPIO GPIO type.
 */
template<spl::peripherals::gpio::concepts::gpio GPIO>
struct configuration {
    GPIO& charge_enable;
    GPIO& power_good;
    GPIO& stat1;
    GPIO& stat2;
};

/** @} */

} // namespace devices::mcp73871
