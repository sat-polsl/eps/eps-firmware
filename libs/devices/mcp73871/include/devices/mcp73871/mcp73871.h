#pragma once
#include "devices/mcp73871/configuration.h"
#include "spl/peripherals/gpio/concepts/gpio.h"

namespace devices::mcp73871 {

/**
 * @ingroup mcp73871
 * @{
 */

/**
 * @brief MCP73871 class.
 * @tparam GPIO GPIO type.
 */
template<spl::peripherals::gpio::concepts::gpio GPIO>
class mcp73871 {
public:
    /**
     * @brief Constructor.
     * @param cfg GPIO configuration.
     */
    explicit mcp73871(const configuration<GPIO>& cfg) :
        charge_enable_{cfg.charge_enable},
        power_good_{cfg.power_good},
        stat1_{cfg.stat1},
        stat2_{cfg.stat2} {}

    /**
     * @brief Checks Power Good.
     * @return true if power good, false if not good.
     */
    bool power_good() { return power_good_.read(); }

    /**
     * @brief Checks STAT1 signal.
     * @return true if high, false if low.
     */
    bool stat1() { return stat1_.read(); };

    /**
     * @brief Checks STAT2 signal.
     * @return true if high, false if low.
     */
    bool stat2() { return stat2_.read(); };

    /**
     * @brief Enables charge.
     */
    void charge_enable() { charge_enable_.set(); }

    /**
     * @brief Disables charge.
     */
    void charge_disable() { charge_enable_.clear(); }

private:
    GPIO& charge_enable_;
    GPIO& power_good_;
    GPIO& stat1_;
    GPIO& stat2_;
};

/** @} */

} // namespace devices::mcp73871
