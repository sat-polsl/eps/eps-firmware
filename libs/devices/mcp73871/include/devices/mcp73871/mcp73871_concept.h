#pragma once
#include <concepts>
#include <cstdint>
#include "devices/mcp73871/configuration.h"

namespace devices::mcp73871 {

// clang-format off
template<typename T>
concept mcp73871_concept = requires(T mcp) {
    { mcp.power_good() } -> std::same_as<bool>;
    { mcp.stat1() } -> std::same_as<bool>;
    { mcp.stat2() } -> std::same_as<bool>;
    { mcp.charge_enable() } -> std::same_as<void>;
    { mcp.charge_disable() } -> std::same_as<void>;
};
// clang-format on

} // namespace devices::mcp73871
