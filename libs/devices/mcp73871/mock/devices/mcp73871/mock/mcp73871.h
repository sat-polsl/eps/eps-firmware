#pragma once
#include <cstdint>
#include "gmock/gmock.h"
#include "devices/mcp73871/mcp73871_concept.h"

namespace devices::mcp73871::mock {
struct mcp73871 {
    MOCK_METHOD(bool, power_good, ());
    MOCK_METHOD(bool, stat1, ());
    MOCK_METHOD(bool, stat2, ());
    MOCK_METHOD(void, charge_enable, ());
    MOCK_METHOD(void, charge_disable, ());
};

static_assert(devices::mcp73871::mcp73871_concept<mcp73871>);
} // namespace devices::mcp73871::mock
