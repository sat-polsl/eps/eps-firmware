#pragma once
#include "bootloader/utils/boot_slot.h"
#include "devices/fram/fram_concept.h"
#include "satext/struct.h"
#include "satext/units.h"
#include "spl/peripherals/crc/concepts/crc.h"
#include "spl/peripherals/flash/concepts/flash.h"

namespace bootloader::flash_loader {

/**
 * @defgroup flash_loader Flash loader
 * @ingroup bootloader
 * @{
 */

/**
 * @brief Flash Loader status enumeration
 */
enum class status { ok, bad_magic, read_error, crc_error };

/**
 * @brief Flash Loader Class. Class responsible for loading program data from given slot in FRAM
 * memory to Flash memory pointed by FlashOffset and validating it with CRC32.
 * @tparam Flash Flash peripheral
 * @tparam FRAM FRAM device
 * @tparam FlashOffset Program offset in Flash memory
 */
template<::spl::peripherals::flash::concepts::flash Flash,
         ::devices::fram::fram_concept FRAM,
         ::spl::peripherals::crc::concepts::crc CRC,
         std::uint32_t FlashOffset>
class flash_loader {
public:
    /**
     * @brief Constructor.
     * @param flash Flash peripheral
     * @param fram FRAM device
     */
    flash_loader(Flash& flash, FRAM& fram, CRC& crc) : flash_{flash}, fram_{fram}, crc_{crc} {}

    /**
     * Loads and validates slot from FRAM memory to Flash memory.
     * @param boot_slot Boot slot data
     * @return operation status
     */
    status load_slot(const utils::boot_slot& boot_slot) {
        using namespace satext::struct_literals;

        std::array<std::byte, boot_header_size> boot_slot_header{};
        if (auto result = fram_.read(boot_slot.address, boot_slot_header); !result.has_value()) {
            return status::read_error;
        }

        flash_.unlock();

        auto result = satext::unpack("<IIII"_fmt, boot_slot_header)
                          .map([this, &boot_slot](auto& unpack_result) {
                              auto [magic_a, magic_b, crc_value, length] = unpack_result;
                              if (magic_a != boot_slot.magic_a || magic_b != boot_slot.magic_b) {
                                  return status::bad_magic;
                              }
                              load_flash_from_fram(boot_slot.address + boot_header_size, length);
                              return validate_image(crc_value, length);
                          })
                          .value_or(status::read_error);

        flash_.lock();
        return result;
    }

private:
    void load_flash_from_fram(std::uint32_t fram_offset, std::size_t size) {
        std::array<std::byte, Flash::page_size> buffer{};
        for (std::uint32_t offset{}; offset < size;) {
            auto read_size = std::min(buffer.size(), static_cast<std::size_t>(size - offset));
            offset += fram_.read(fram_offset + offset, std::span(buffer.data(), read_size))
                          .map([this, &buffer, &offset](std::span<const std::byte> read_result) {
                              auto flash_size = prepare_flash_buffer(
                                  std::span(buffer.data(), read_result.size()));
                              flash_.erase_page((FlashOffset + offset) / Flash::page_size);
                              flash_.program_buffer(Flash::memory_base + FlashOffset + offset,
                                                    std::span(buffer.data(), flash_size));
                              return flash_size;
                          })
                          .value_or(0);
        }
    }

    std::size_t prepare_flash_buffer(std::span<std::byte> buffer) const {
        std::size_t flash_size = buffer.size();
        if (auto remainder = flash_size % sizeof(uint64_t); remainder != 0) {
            auto bytes_to_fill = sizeof(uint64_t) - remainder;
            std::fill_n(buffer.data() + flash_size, bytes_to_fill, std::byte{0xff});
            flash_size += bytes_to_fill;
        }
        return flash_size;
    }

    status validate_image(std::uint32_t crc_value, std::uint32_t length) {
        if (crc_.feed({reinterpret_cast<std::byte*>(Flash::memory_base + FlashOffset), length}) ==
            crc_value) {
            return status::ok;
        } else {
            return status::crc_error;
        }
    }

    static constexpr std::size_t boot_header_size = 16;

    Flash& flash_;
    FRAM& fram_;
    CRC& crc_;
};

/** @} */

} // namespace bootloader::flash_loader
