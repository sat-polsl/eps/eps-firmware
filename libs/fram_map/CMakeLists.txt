set(TARGET fram_map)

add_library(${TARGET} INTERFACE)

target_include_directories(${TARGET} INTERFACE include)

target_link_libraries(${TARGET} INTERFACE
    satext::satext
    bootloader_utils
    )
