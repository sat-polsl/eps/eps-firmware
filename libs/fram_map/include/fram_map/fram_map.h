#pragma once
#include "bootloader/utils/boot_slot.h"
#include "satext/units.h"

namespace fram_map {

constexpr std::uint32_t boot_counter_offset = 0x00;
constexpr std::uint32_t boot_attempt_counter_offset = 0x04;

constexpr std::uint32_t power_configuration_offset = 0x100;

constexpr std::array<bootloader::utils::boot_slot, 1> boot_slots = {
    bootloader::utils::boot_slot{.address = 2_KiB, .magic_a = 0x746f6f62, .magic_b = 0x746f6c73}};

} // namespace fram_map
