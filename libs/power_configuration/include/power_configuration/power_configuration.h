#pragma once
#include <cstdint>
#include <span>
#include "power_configuration/serializer.h"
#include "power_configuration/storage.h"
#include "power_service/configuration.h"
#include "safe/safe.h"
#include "spl/peripherals/crc/crc.h"

namespace power_configuration {

class power_configuration {
public:
    power_configuration(storage& storage, spl::peripherals::crc::crc& crc);

    void initialize();

    power_service::configuration get() const;

    void set(const power_service::configuration& desc);

private:
    safe::safe<power_service::configuration, serializer, storage> safe_;
};

} // namespace power_configuration
