#pragma once
#include <cstddef>
#include <span>
#include "power_service/configuration.h"

namespace power_configuration {

struct serializer {
    static void serialize(const power_service::configuration& desc, std::span<std::byte> buffer);
    static void deserialize(std::span<const std::byte> buffer, power_service::configuration& desc);
};

} // namespace power_configuration
