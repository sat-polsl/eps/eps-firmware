#pragma once
#include <cstdint>
#include <span>
#include "satext/inplace_function.h"

namespace power_configuration {

class storage {
public:
    using read_callback = satext::inplace_function<bool(std::size_t, std::span<std::byte>)>;
    using write_callback = satext::inplace_function<bool(std::size_t, std::span<const std::byte>)>;

    storage(read_callback read, write_callback write);

    bool read(std::size_t offset, std::span<std::byte> output) const;

    bool write(std::size_t offset, std::span<const std::byte> input) const;

private:
    read_callback read_;
    write_callback write_;
};

} // namespace power_configuration
