#include "power_configuration/power_configuration.h"

namespace power_configuration {

power_configuration::power_configuration(storage& storage, spl::peripherals::crc::crc& crc) :
    safe_{storage, [&crc](std::span<const std::byte> buffer) {
              crc.reset();
              return crc.feed(buffer);
          }} {}

void power_configuration::initialize() { safe_.initialize(); }

power_service::configuration power_configuration::get() const { return safe_.get(); }

void power_configuration::set(const power_service::configuration& desc) { safe_.set(desc); }

} // namespace power_configuration
