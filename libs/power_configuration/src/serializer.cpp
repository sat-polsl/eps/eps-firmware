#include "power_configuration/serializer.h"
#include <cstdint>
#include "power_service/enums.h"
#include "satext/struct.h"
#include "satext/type_traits.h"
#include "satos/chrono.h"

namespace power_configuration {

using namespace satext::struct_literals;

using bool_array = std::array<bool, power_service::power_outputs_number>;

std::uint8_t serialize_bool_array_field(const bool_array& array,
                                        power_service::power_output output) {
    return static_cast<std::uint8_t>(array[satext::to_underlying_type(output)])
           << satext::to_underlying_type(output);
}

std::uint8_t serialize_bool_array(const bool_array& array) {
    return serialize_bool_array_field(array, power_service::power_output::v3v3_1) |
           serialize_bool_array_field(array, power_service::power_output::v3v3_2) |
           serialize_bool_array_field(array, power_service::power_output::v3v3_3) |
           serialize_bool_array_field(array, power_service::power_output::v3v3_4) |
           serialize_bool_array_field(array, power_service::power_output::v5_1) |
           serialize_bool_array_field(array, power_service::power_output::v5_2) |
           serialize_bool_array_field(array, power_service::power_output::v12);
}

std::uint16_t serialize_watchdog_timeout(const power_service::configuration& desc,
                                         power_service::power_output output) {
    return desc.watchdog_timeout[satext::to_underlying_type(output)].count();
}

void serializer::serialize(const power_service::configuration& desc, std::span<std::byte> buffer) {
    auto enable_on_startup = serialize_bool_array(desc.enable_on_startup);
    auto locked = serialize_bool_array(desc.locked);
    auto watchdog_enabled = serialize_bool_array(desc.watchdog_enabled);

    satext::pack_to("<BBB7H"_fmt,
                    buffer,
                    enable_on_startup,
                    locked,
                    watchdog_enabled,
                    serialize_watchdog_timeout(desc, power_service::power_output::v3v3_1),
                    serialize_watchdog_timeout(desc, power_service::power_output::v3v3_2),
                    serialize_watchdog_timeout(desc, power_service::power_output::v3v3_3),
                    serialize_watchdog_timeout(desc, power_service::power_output::v3v3_4),
                    serialize_watchdog_timeout(desc, power_service::power_output::v5_1),
                    serialize_watchdog_timeout(desc, power_service::power_output::v5_2),
                    serialize_watchdog_timeout(desc, power_service::power_output::v12));
}

void deserialize_bool_array(std::uint8_t value, bool_array& array) {
    array[satext::to_underlying_type(power_service::power_output::v3v3_1)] =
        value & (1 << satext::to_underlying_type(power_service::power_output::v3v3_1));
    array[satext::to_underlying_type(power_service::power_output::v3v3_2)] =
        value & (1 << satext::to_underlying_type(power_service::power_output::v3v3_2));
    array[satext::to_underlying_type(power_service::power_output::v3v3_3)] =
        value & (1 << satext::to_underlying_type(power_service::power_output::v3v3_3));
    array[satext::to_underlying_type(power_service::power_output::v3v3_4)] =
        value & (1 << satext::to_underlying_type(power_service::power_output::v3v3_4));
    array[satext::to_underlying_type(power_service::power_output::v5_1)] =
        value & (1 << satext::to_underlying_type(power_service::power_output::v5_1));
    array[satext::to_underlying_type(power_service::power_output::v5_2)] =
        value & (1 << satext::to_underlying_type(power_service::power_output::v5_2));
    array[satext::to_underlying_type(power_service::power_output::v12)] =
        value & (1 << satext::to_underlying_type(power_service::power_output::v12));
}

void serializer::deserialize(std::span<const std::byte> buffer,
                             power_service::configuration& desc) {
    satext::unpack("<BBB7H"_fmt, buffer).map([&desc](auto unpacked) {
        auto [enable_on_startup,
              locked,
              watchdog_enabled,
              watchdog_timeout_v3v3_1,
              watchdog_timeout_v3v3_2,
              watchdog_timeout_v3v3_3,
              watchdog_timeout_v3v3_4,
              watchdog_timeout_v5_1,
              watchdog_timeout_v5_2,
              watchdog_timeout_v12] = unpacked;

        deserialize_bool_array(enable_on_startup, desc.enable_on_startup);
        deserialize_bool_array(locked, desc.locked);
        deserialize_bool_array(watchdog_enabled, desc.watchdog_enabled);

        desc.watchdog_timeout[satext::to_underlying_type(power_service::power_output::v3v3_1)] =
            std::chrono::seconds(watchdog_timeout_v3v3_1);
        desc.watchdog_timeout[satext::to_underlying_type(power_service::power_output::v3v3_2)] =
            std::chrono::seconds(watchdog_timeout_v3v3_2);
        desc.watchdog_timeout[satext::to_underlying_type(power_service::power_output::v3v3_3)] =
            std::chrono::seconds(watchdog_timeout_v3v3_3);
        desc.watchdog_timeout[satext::to_underlying_type(power_service::power_output::v3v3_4)] =
            std::chrono::seconds(watchdog_timeout_v3v3_4);
        desc.watchdog_timeout[satext::to_underlying_type(power_service::power_output::v5_1)] =
            std::chrono::seconds(watchdog_timeout_v5_1);
        desc.watchdog_timeout[satext::to_underlying_type(power_service::power_output::v5_2)] =
            std::chrono::seconds(watchdog_timeout_v5_2);
        desc.watchdog_timeout[satext::to_underlying_type(power_service::power_output::v12)] =
            std::chrono::seconds(watchdog_timeout_v12);
    });
}
} // namespace power_configuration
