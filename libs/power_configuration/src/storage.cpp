#include "power_configuration/storage.h"

namespace power_configuration {

storage::storage(power_configuration::storage::read_callback read,
                 power_configuration::storage::write_callback write) :
    read_{std::move(read)},
    write_{std::move(write)} {}

bool storage::read(std::size_t offset, std::span<std::byte> output) const {
    return read_(offset, output);
}

bool storage::write(std::size_t offset, std::span<const std::byte> input) const {
    return write_(offset, input);
}

} // namespace power_configuration
