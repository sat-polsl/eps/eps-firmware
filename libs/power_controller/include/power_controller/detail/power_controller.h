#pragma once
#include <functional>
#include "etl/map.h"
#include "power_controller/detail/types.h"
#include "power_controller/enums.h"
#include "power_controller/power_controller_concept.h"
#include "satext/type_traits.h"
#include "spl/peripherals/gpio/concepts/gpio.h"
#include "telemetry/telemetry_state_concept.h"

namespace power_controller::detail {

/**
 * @ingroup power_controller
 * @{
 */

/**
 * @brief Power controller
 * This class provides simple interface to control EPS power outputs.
 * It manages DC/DC converters state taking into consideration EPS power outputs state.
 * If all power outputs for given voltage are disabled then the corresponding DC/DC converter will
 * be disabled too. Once single EPS output for given voltage is enabled the corresponding DC/DC
 * converter will be enabled too.
 * @tparam GPIO GPIO peripheral.
 * @tparam TelemetryState Telemetry state.
 */
template<spl::peripherals::gpio::concepts::gpio GPIO,
         telemetry::telemetry_state_concept TelemetryState>
class power_controller {
public:
    /**
     * @brief Constructor.
     * @param configuration Power controller configuration.
     * @param telemetry_state Telemetry state.
     * @param output_state_telemetry_id Telemetry ID for power outputs state.
     */
    power_controller(configuration<GPIO>& configuration,
                     TelemetryState& telemetry_state,
                     telemetry::telemetry_id output_state_telemetry_id) :
        outputs_{{output::v3v3_1, &configuration.v3v3_1},
                 {output::v3v3_2, &configuration.v3v3_2},
                 {output::v3v3_3, &configuration.v3v3_3},
                 {output::v3v3_4, &configuration.v3v3_4},
                 {output::v5_1, &configuration.v5_1},
                 {output::v5_2, &configuration.v5_2},
                 {output::v12, &configuration.v12},
                 {output::heater, &configuration.heater}},
        dcdc_enable_{{dcdc::v3v3, &configuration.bus_3v3},
                     {dcdc::v5, &configuration.bus_5v},
                     {dcdc::v12, &configuration.bus_12v}},
        telemetry_state_{telemetry_state},
        output_state_telemetry_id_{output_state_telemetry_id} {}

    /**
     * @brief Enables desired output.
     * @param out Output.
     */
    void enable(output out) {
        dcdc_enable_[map_output_to_dcdc(out)]->set();
        if (is_3v3(out)) {
            outputs_[out]->clear();
        } else {
            outputs_[out]->set();
        }
        outputs_state_[out] = true;
        update_telemetry_state(out, true);
    }

    /**
     * @brief Disables output.gggg
     * @param out Output
     */
    void disable(output out) {
        if (is_3v3(out)) {
            outputs_[out]->set();
        } else {
            outputs_[out]->clear();
        }
        outputs_state_[out] = false;
        update_telemetry_state(out, false);

        if (!is_any_of_3v3_enabled()) {
            dcdc_enable_[dcdc::v3v3]->clear();
        }

        if (!is_any_of_5v_enabled()) {
            dcdc_enable_[dcdc::v5]->clear();
        }

        if (!is_any_of_12v_enabled()) {
            dcdc_enable_[dcdc::v12]->clear();
        }
    }

    /**
     * @brief Checks if a output is enabled.
     * @param out Output
     * @return true if enabled, false otherwise.
     */
    bool is_enabled(output out) { return outputs_state_[out]; }

private:
    enum class dcdc { v3v3 = 0, v5 = 1, v12 = 2 };

    bool is_any_of_3v3_enabled() {
        return outputs_state_[output::v3v3_1] || outputs_state_[output::v3v3_2] ||
               outputs_state_[output::v3v3_3] || outputs_state_[output::v3v3_4];
    }

    bool is_any_of_5v_enabled() {
        return outputs_state_[output::v5_1] || outputs_state_[output::v5_2];
    }

    bool is_any_of_12v_enabled() {
        return outputs_state_[output::v12] || outputs_state_[output::heater];
    }

    static bool is_3v3(output out) {
        return out == output::v3v3_1 || out == output::v3v3_2 || out == output::v3v3_3 ||
               out == output::v3v3_4;
    }

    static dcdc map_output_to_dcdc(output out) {
        switch (out) {
        case output::v3v3_1:
        case output::v3v3_2:
        case output::v3v3_3:
        case output::v3v3_4:
            return dcdc::v3v3;
        case output::v5_1:
        case output::v5_2:
            return dcdc::v5;
        case output::v12:
        case output::heater:
            return dcdc::v12;
        default:
            return dcdc::v3v3;
        }
    }

    void update_telemetry_state(output out, bool state) {
        auto current_state = telemetry_state_.template read<std::byte>(output_state_telemetry_id_)
                                 .value_or(std::byte{0});
        if (state) {
            current_state |= std::byte(1 << satext::to_underlying_type(out));
        } else {
            current_state &= ~std::byte(1 << satext::to_underlying_type(out));
        }
        telemetry_state_.write(output_state_telemetry_id_, current_state);
    }

    etl::map<output, GPIO*, 8> outputs_;
    etl::map<dcdc, GPIO*, 3> dcdc_enable_;
    etl::map<output, bool, 8> outputs_state_{{output::v3v3_1, false},
                                             {output::v3v3_2, false},
                                             {output::v3v3_3, false},
                                             {output::v3v3_4, false},
                                             {output::v5_1, false},
                                             {output::v5_2, false},
                                             {output::v12, false},
                                             {output::heater, false}};
    TelemetryState& telemetry_state_;
    telemetry::telemetry_id output_state_telemetry_id_;
};
/** @} */
} // namespace power_controller::detail
