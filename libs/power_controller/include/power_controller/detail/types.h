#pragma once
#include "spl/peripherals/gpio/concepts/gpio.h"

namespace power_controller::detail {

/**
 * @ingroup power_controller
 * @{
 */

template<spl::peripherals::gpio::concepts::gpio GPIO>
struct configuration {
    GPIO& bus_3v3;
    GPIO& bus_5v;
    GPIO& bus_12v;
    GPIO& v3v3_1;
    GPIO& v3v3_2;
    GPIO& v3v3_3;
    GPIO& v3v3_4;
    GPIO& v5_1;
    GPIO& v5_2;
    GPIO& v12;
    GPIO& heater;
};

/** @} */

} // namespace power_controller::detail
