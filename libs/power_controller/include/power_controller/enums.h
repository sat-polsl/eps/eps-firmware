#pragma once
#include <cstdint>

namespace power_controller {

/**
 * @addtogroup power_controller
 * @{
 */

/**
 * @brief Output enumeration.
 */
enum class output : std::uint32_t {
    v3v3_1 = 0,
    v3v3_2 = 1,
    v3v3_3 = 2,
    v3v3_4 = 3,
    v5_1 = 4,
    v5_2 = 5,
    v12 = 6,
    heater = 7
};
/** @} */

} // namespace power_controller
