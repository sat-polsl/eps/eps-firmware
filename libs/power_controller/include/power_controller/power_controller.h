#include "power_controller/detail/power_controller.h"
#include "spl/peripherals/gpio/gpio.h"
#include "telemetry/telemetry_state.h"

/**
 *  @defgroup power_bus_controller Power bus controller
 */

namespace power_controller {

/**
 * @ingroup power_controller
 * @{
 */

/**
 * @brief Application specialization
 */
using power_controller =
    detail::power_controller<spl::peripherals::gpio::gpio, telemetry::telemetry_state>;

using configuration = detail::configuration<spl::peripherals::gpio::gpio>;

static_assert(power_controller_concept<power_controller>);

/** @} */

} // namespace power_controller
