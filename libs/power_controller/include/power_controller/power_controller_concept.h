#pragma once
#include <concepts>
#include "power_controller/detail/power_controller.h"

namespace power_controller {

// clang-format off
template<typename T>
concept power_controller_concept = requires(T controller, output o) {
    { controller.enable(o) } -> std::same_as<void>;
    { controller.disable(o) } -> std::same_as<void>;
    { controller.is_enabled(o) } -> std::same_as<bool>;
};
// clang-format on

} // namespace power_controller
