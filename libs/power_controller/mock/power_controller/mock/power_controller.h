#pragma once
#include "gmock/gmock.h"
#include "power_controller/power_controller_concept.h"

namespace power_controller::mock {
struct power_controller {
    MOCK_METHOD(void, enable, (output));
    MOCK_METHOD(void, disable, (output));
    MOCK_METHOD(bool, is_enabled, (output), (const));
};

static_assert(power_controller_concept<power_controller>);

} // namespace power_controller::mock
