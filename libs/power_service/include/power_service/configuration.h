#pragma once
#include <array>
#include "satext/array.h"
#include "satos/chrono.h"

namespace power_service {

/**
 * @ingroup power_service
 * @{
 */

/**
 * @brief Number of power outputs.
 */
constexpr std::size_t power_outputs_number = 7;

/**
 * @brief Power Service configuration.
 */
struct configuration {
    /**
     * @brief If set to true enables power output at corresponding index upon initialization of
     * Power Service.
     */
    std::array<bool, power_outputs_number> enable_on_startup;

    /**
     * @brief If set to true then output state, watchdog state and watchdog timeout of power output
     * at corresponding index can be changed.
     */
    std::array<bool, power_outputs_number> locked;

    /**
     * @brief If set to true enables watchdog for power output at corresponding index.
     */
    std::array<bool, power_outputs_number> watchdog_enabled;

    /**
     * @brief Watchdog timeouts for power outputs at corresponding index.
     */
    std::array<satos::chrono::seconds, power_outputs_number> watchdog_timeout{
        satext::create_array<power_outputs_number, satos::chrono::seconds>(
            satos::chrono::seconds ::max())};
};

/** @} */

} // namespace power_service
