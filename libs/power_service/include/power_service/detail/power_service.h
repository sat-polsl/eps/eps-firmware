#pragma once
#include <span>
#include "power_controller/power_controller_concept.h"
#include "power_service/configuration.h"
#include "power_service/enums.h"
#include "satext/array.h"
#include "satext/noncopyable.h"
#include "satext/ranges/enumerate.h"
#include "satos/concepts/timer.h"

namespace power_service::detail {
/**
 * @ingroup power_service
 * @{
 */

/**
 * @brief Power Services
 * Power Service is initialized with configuration that:
 *   - may enable outputs on EPS startup
 *   - may lock outputs state and watchdog
 *   - may enable watchdogs on EPS startup
 *   - sets watchdogs timeout
 *
 *
 * Power Service manages enabling and disabling EPS power outputs with their corresponding
 * watchdogs. Watchdogs can be enabled/disabled/reconfigured with Power Service.
 *
 * @tparam PowerController Power Controller type
 * @tparam Timer Timer type
 */
template<power_controller::power_controller_concept PowerController, satos::concepts::timer Timer>
class power_service : private satext::noncopyable {
public:
    /**
     * @brief Constructor
     * @param power_controller Reference to power controller
     */
    explicit power_service(PowerController& power_controller) :
        power_controller_{power_controller} {}

    /**
     * @brief Initializes Power Service
     * @param cfg Configuration
     */
    void initialize(const configuration& cfg) {
        locked_ = cfg.locked;
        watchdog_enabled_ = cfg.watchdog_enabled;
        watchdog_timeout_ = cfg.watchdog_timeout;

        // TODO: change to std::ranges::enumerate once this feature lands in C++ library
        for (auto&& [index, timer] : satext::ranges::enumerate(watchdog_timers_)) {
            timer.initialize();
            timer.register_callback([this, output = index]() {
                power_controller_.disable(power_controller::output(output));
                enable_timers_[output].start(restart_delay);
            });
        }

        for (auto&& [index, timer] : satext::ranges::enumerate(enable_timers_)) {
            timer.initialize();
            timer.register_callback([this, output = index] {
                power_controller_.enable(power_controller::output(output));

                if (watchdog_enabled_[output]) {
                    watchdog_timers_[output].start(watchdog_timeout_[output]);
                }
            });
        }

        for (auto&& [index, enable] : satext::ranges::enumerate(cfg.enable_on_startup)) {
            if (enable) {
                power_controller_.enable(power_controller::output(index));
            }
        }
    }

    /**
     * @brief Enables power output
     * @param output Power output
     */
    void enable(power_output output) {
        auto output_idx = satext::to_underlying_type(output);
        if (!locked_[output_idx]) {
            power_controller_.enable(power_controller::output(output));

            if (watchdog_enabled_[output_idx]) {
                watchdog_timers_[output_idx].start(watchdog_timeout_[output_idx]);
            }
        }
    }

    /**
     * @brief Disables power output
     * @param output Power output
     */
    void disable(power_output output) {
        auto output_idx = satext::to_underlying_type(output);
        if (!locked_[output_idx]) {
            power_controller_.disable(power_controller::output(output));
            watchdog_timers_[output_idx].stop();
        }
    }

    /**
     * @brief Kicks watchdog for given power output
     * @param output Power output
     */
    void kick_watchdog(power_output output) {
        auto output_idx = satext::to_underlying_type(output);
        if (watchdog_enabled_[output_idx] && watchdog_timers_[output_idx].is_running()) {
            watchdog_timers_[output_idx].start(watchdog_timeout_[output_idx]);
        }
    }

    /**
     * @brief Enables watchdog for given power output
     * @param output Power output
     */
    void enable_watchdog(power_output output) {
        auto output_idx = satext::to_underlying_type(output);
        if (!locked_[output_idx]) {
            watchdog_enabled_[output_idx] = true;
        }
    }

    /**
     * @brief Disables watchdog for given power output
     * @param output Power output
     */
    void disable_watchdog(power_output output) {
        auto output_idx = satext::to_underlying_type(output);
        if (!locked_[output_idx]) {
            watchdog_enabled_[output_idx] = false;
        }
    }

    /**
     * @brief Sets watchdog timeout for given power output
     * @param output Power output
     * @param timeout Watchdog timeout
     */
    void set_watchdog_timeout(power_output output, satos::chrono::seconds timeout) {
        auto output_idx = satext::to_underlying_type(output);
        if (!locked_[output_idx]) {
            watchdog_timeout_[output_idx] = timeout;
        }
    }

    /**
     * @brief Checks whether given power output is enabled.
     * @param output Output.
     * @return true if enabled, false otherwise.
     */
    bool is_enabled(power_output output) {
        return power_controller_.is_enabled(power_controller::output(output));
    }

private:
    static constexpr auto restart_delay = satos::chrono::seconds{1};

    PowerController& power_controller_;
    std::array<bool, power_outputs_number> locked_{};
    std::array<bool, power_outputs_number> watchdog_enabled_{};
    std::array<satos::chrono::seconds, power_outputs_number> watchdog_timeout_{};

    std::array<Timer, power_outputs_number> watchdog_timers_{
        satext::create_array<Timer, power_outputs_number>(satos::clock::duration::max(),
                                                          satos::autoreload::disabled)};
    std::array<Timer, power_outputs_number> enable_timers_{
        satext::create_array<Timer, power_outputs_number>(satos::clock::duration::max(),
                                                          satos::autoreload::disabled)};
};

/** @} */

}; // namespace power_service::detail
