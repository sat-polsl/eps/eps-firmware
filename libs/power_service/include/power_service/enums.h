#pragma once
#include "power_controller/enums.h"
#include "satext/type_traits.h"

namespace power_service {

/**
 * @ingroup power_service
 * @{
 */

/**
 * @brief Power output enumeration.
 */
enum class power_output : std::uint32_t {
    v3v3_1 = satext::to_underlying_type(power_controller::output::v3v3_1),
    v3v3_2 = satext::to_underlying_type(power_controller::output::v3v3_2),
    v3v3_3 = satext::to_underlying_type(power_controller::output::v3v3_3),
    v3v3_4 = satext::to_underlying_type(power_controller::output::v3v3_4),
    v5_1 = satext::to_underlying_type(power_controller::output::v5_1),
    v5_2 = satext::to_underlying_type(power_controller::output::v5_2),
    v12 = satext::to_underlying_type(power_controller::output::v12)
};

/** @} */

} // namespace power_service
