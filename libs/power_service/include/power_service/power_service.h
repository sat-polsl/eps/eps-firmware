#pragma once
#include "power_controller/power_controller.h"
#include "power_service/detail/power_service.h"
#include "power_service/power_service_concept.h"
#include "satos/timer.h"

namespace power_service {

/**
 * @ingroup power_service
 * @{
 */

/**
 * @brief Application specialization
 */
using power_service = detail::power_service<power_controller::power_controller, satos::timer<1>>;

static_assert(power_service_concept<power_service>);

/** @} */

} // namespace power_service
