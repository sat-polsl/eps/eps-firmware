#pragma once
#include <concepts>
#include "power_service/configuration.h"
#include "power_service/enums.h"

namespace power_service {

// clang-format off
template<typename T>
concept power_service_concept = requires(T service, power_output output, configuration cfg,
                                         satos::chrono::seconds timeout) {
    { service.enable(output) } -> std::same_as<void>;
    { service.disable(output) } -> std::same_as<void>;
    { service.kick_watchdog(output) } -> std::same_as<void>;
    { service.enable_watchdog(output) } -> std::same_as<void>;
    { service.disable_watchdog(output) } -> std::same_as<void>;
    { service.set_watchdog_timeout(output, timeout) } -> std::same_as<void>;
    { service.is_enabled(output) } -> std::same_as<bool>;
};
// clang-format on
} // namespace power_service
