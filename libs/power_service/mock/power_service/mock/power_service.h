#pragma once
#include "gmock/gmock.h"
#include "power_service/power_service_concept.h"
#include "satos/chrono.h"

namespace power_service::mock {

struct power_service {
    MOCK_METHOD(void, enable, (power_output));
    MOCK_METHOD(void, disable, (power_output));
    MOCK_METHOD(void, enable_watchdog, (power_output));
    MOCK_METHOD(void, disable_watchdog, (power_output));
    MOCK_METHOD(void, kick_watchdog, (power_output));
    MOCK_METHOD(void, set_watchdog_timeout, (power_output, satos::chrono::seconds));
    MOCK_METHOD(bool, is_enabled, (power_output));
};

static_assert(power_service_concept<power_service>);

} // namespace power_service::mock
