#pragma once
#include "devices/adc/adc_concept.h"
#include "telemetry/enums.h"
#include "telemetry/telemetry_state_concept.h"

namespace telemetry::samplers {

/**
 * @ingroup telemetry_samplers
 * @{
 */

/**
 * @brief ADC channels config.
 */
struct adc_sampler_config {
    telemetry::telemetry_id v3v3_l1;
    telemetry::telemetry_id i3v3_l1;
    telemetry::telemetry_id v3v3_l2;
    telemetry::telemetry_id i3v3_l2;
    telemetry::telemetry_id v3v3_l3;
    telemetry::telemetry_id i3v3_l3;
    telemetry::telemetry_id v3v3_l4;
    telemetry::telemetry_id i3v3_l4;
    telemetry::telemetry_id v5v_l1;
    telemetry::telemetry_id i5v_l1;
    telemetry::telemetry_id v5v_l2;
    telemetry::telemetry_id i5v_l2;
    telemetry::telemetry_id v12v;
    telemetry::telemetry_id i12v;
    telemetry::telemetry_id vbms;
    telemetry::telemetry_id ibms;
    telemetry::telemetry_id vmppt;
    telemetry::telemetry_id imppt1;
    telemetry::telemetry_id imppt2;
};

/**
 * @brief ADC sampler.
 * @tparam VoltageAdc Voltage ADC type.
 * @tparam CurrentAdc Current ADC type.
 * @tparam TelemetryState Telemetry state type.
 * @tparam Config ADC config.
 */
template<devices::adc::adc_concept VoltageAdc,
         devices::adc::adc_concept CurrentAdc,
         telemetry_state_writable TelemetryState,
         adc_sampler_config Config>
class adc_sampler {
public:
    using state = TelemetryState;

    /**
     * @brief Constructor.
     * @param voltage_adc Voltage ADC reference.
     * @param current_adc Current ADC reference.
     */
    adc_sampler(VoltageAdc& voltage_adc, CurrentAdc& current_adc) :
        voltage_adc_{voltage_adc},
        current_adc_{current_adc} {}

    /**
     * @brief Samples telemetry from ADCs and writes value to Telemetry State.
     * @param state Telemetry State reference.
     */
    void operator()(TelemetryState& state) {
        voltage_adc_.read_all()
            .map([&state](auto buffer) {
                state.write(Config.v3v3_l1, buffer[voltage_adc_channel::v3v3_l1]);
                state.write(Config.v3v3_l2, buffer[voltage_adc_channel::v3v3_l2]);
                state.write(Config.v3v3_l3, buffer[voltage_adc_channel::v3v3_l3]);
                state.write(Config.v3v3_l4, buffer[voltage_adc_channel::v3v3_l4]);
                state.write(Config.v5v_l1, buffer[voltage_adc_channel::v5v_l1]);
                state.write(Config.v5v_l2, buffer[voltage_adc_channel::v5v_l2]);
                state.write(Config.v12v, buffer[voltage_adc_channel::v12v]);
            })
            .map_error([&state](auto) {
                state.write(Config.v3v3_l1, std::monostate{});
                state.write(Config.v3v3_l2, std::monostate{});
                state.write(Config.v3v3_l3, std::monostate{});
                state.write(Config.v3v3_l4, std::monostate{});
                state.write(Config.v5v_l1, std::monostate{});
                state.write(Config.v5v_l2, std::monostate{});
                state.write(Config.v12v, std::monostate{});
            });

        current_adc_.read_all()
            .map([&state](auto buffer) {
                state.write(Config.i3v3_l1, buffer[current_adc_channel::i3v3_l1]);
                state.write(Config.i3v3_l2, buffer[current_adc_channel::i3v3_l2]);
                state.write(Config.i3v3_l3, buffer[current_adc_channel::i3v3_l3]);
                state.write(Config.i3v3_l4, buffer[current_adc_channel::i3v3_l4]);
                state.write(Config.i5v_l1, buffer[current_adc_channel::i5v_l1]);
                state.write(Config.i5v_l2, buffer[current_adc_channel::i5v_l2]);
                state.write(Config.i12v, buffer[current_adc_channel::i12v]);
                state.write(Config.vbms, buffer[current_adc_channel::vbms]);
                state.write(Config.ibms, buffer[current_adc_channel::ibms]);
                state.write(Config.vmppt, buffer[current_adc_channel::vmppt]);
                state.write(Config.imppt1, buffer[current_adc_channel::imppt1]);
                state.write(Config.imppt2, buffer[current_adc_channel::imppt2]);
            })
            .map_error([&state](auto) {
                state.write(Config.i3v3_l1, std::monostate{});
                state.write(Config.i3v3_l2, std::monostate{});
                state.write(Config.i3v3_l3, std::monostate{});
                state.write(Config.i3v3_l4, std::monostate{});
                state.write(Config.i5v_l1, std::monostate{});
                state.write(Config.i5v_l2, std::monostate{});
                state.write(Config.i12v, std::monostate{});
                state.write(Config.vbms, std::monostate{});
                state.write(Config.ibms, std::monostate{});
                state.write(Config.vmppt, std::monostate{});
                state.write(Config.imppt1, std::monostate{});
                state.write(Config.imppt2, std::monostate{});
            });
    }

private:
    VoltageAdc& voltage_adc_;
    CurrentAdc& current_adc_;

    struct voltage_adc_channel {
        static constexpr std::size_t v3v3_l1 = 2;
        static constexpr std::size_t v3v3_l2 = 4;
        static constexpr std::size_t v3v3_l3 = 5;
        static constexpr std::size_t v3v3_l4 = 6;
        static constexpr std::size_t v5v_l1 = 1;
        static constexpr std::size_t v5v_l2 = 3;
        static constexpr std::size_t v12v = 0;
    };

    struct current_adc_channel {
        static constexpr std::size_t i3v3_l1 = 9;
        static constexpr std::size_t i3v3_l2 = 8;
        static constexpr std::size_t i3v3_l3 = 11;
        static constexpr std::size_t i3v3_l4 = 10;
        static constexpr std::size_t i5v_l1 = 6;
        static constexpr std::size_t i5v_l2 = 7;
        static constexpr std::size_t i12v = 5;
        static constexpr std::size_t vbms = 3;
        static constexpr std::size_t ibms = 0;
        static constexpr std::size_t vmppt = 4;
        static constexpr std::size_t imppt1 = 2;
        static constexpr std::size_t imppt2 = 1;
    };
};

/** @} */

} // namespace telemetry::samplers
