#pragma once
#include "telemetry/enums.h"
#include "telemetry/telemetry_state_concept.h"

namespace telemetry::samplers {
/**
 * @ingroup telemetry_samplers
 * @{
 */

/**
 * @brief Samples uptime clock value to telemetry state
 * @tparam TelemetryState
 * @tparam Id
 */
template<telemetry_state_writable TelemetryState, telemetry::telemetry_id Id>
class uptime_sampler {
public:
    using state = TelemetryState;
    void operator()(TelemetryState& state) const {
        state.write(Id, satos::clock::now().time_since_epoch().count());
    }
};

/** @} */

} // namespace telemetry::samplers
