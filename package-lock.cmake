# CPM Package Lock
# This file should be committed to version control

# arm-none-eabi-gcc
CPMDeclarePackage(arm-none-eabi-gcc
  VERSION 13.2.1
  GITLAB_REPOSITORY sat-polsl/software/packages/arm-none-eabi-gcc
  SYSTEM YES
  EXCLUDE_FROM_ALL YES
)
# doxygen
CPMDeclarePackage(doxygen
  VERSION 1.9.6
  GITLAB_REPOSITORY sat-polsl/software/packages/doxygen
  SYSTEM YES
  EXCLUDE_FROM_ALL YES
)
# qemu
CPMDeclarePackage(qemu
  VERSION 8.1.2
  GITLAB_REPOSITORY sat-polsl/software/packages/qemu
  SYSTEM YES
  EXCLUDE_FROM_ALL YES
)
# libopencm3 (unversioned)
# CPMDeclarePackage(libopencm3
#  GIT_TAG sat-main
#  GITLAB_REPOSITORY sat-polsl/software/libopencm3
#  SYSTEM YES
#  EXCLUDE_FROM_ALL YES
#)
# semihosting
CPMDeclarePackage(semihosting
  VERSION 1.2.0
  GITLAB_REPOSITORY sat-polsl/software/semihosting
  SYSTEM YES
  EXCLUDE_FROM_ALL YES
)
# satext
CPMDeclarePackage(satext
  VERSION 2.4.1
  GITLAB_REPOSITORY sat-polsl/software/satext
  SYSTEM YES
  EXCLUDE_FROM_ALL YES
)
# googletest
CPMDeclarePackage(googletest
  NAME googletest
  GIT_TAG v1.13.0
  GITHUB_REPOSITORY google/googletest
  EXCLUDE_FROM_ALL YES
  OPTIONS
    "gtest_disable_pthreads ON"
)
# baremetal
CPMDeclarePackage(baremetal
  VERSION 1.1.0
  GITLAB_REPOSITORY sat-polsl/software/baremetal
  SYSTEM YES
  EXCLUDE_FROM_ALL YES
)
# satos
CPMDeclarePackage(satos
  VERSION 5.2.0
  GITLAB_REPOSITORY sat-polsl/software/satos
  SYSTEM YES
  EXCLUDE_FROM_ALL YES
)
# fff (unversioned)
# CPMDeclarePackage(fff
#  NAME fff
#  GIT_TAG master
#  GITHUB_REPOSITORY silesian-aerospace-technologies/fff
#  EXCLUDE_FROM_ALL YES
#  OPTIONS
#    "FFF_BUILD_TESTS OFF"
#    "FFF_BUILD_EXAMPLES OFF"
#    "FFF_GENERATE OFF"
#)
# sat-peripheral-library
CPMDeclarePackage(sat-peripheral-library
  VERSION 0.17.0
  GITLAB_REPOSITORY sat-polsl/software/sat-peripheral-library
  SYSTEM YES
  EXCLUDE_FROM_ALL YES
)
# etl
CPMDeclarePackage(etl
  GIT_TAG 20.38.6
  GITHUB_REPOSITORY etlcpp/etl
  SYSTEM YES
  EXCLUDE_FROM_ALL YES
)
# fmt
CPMDeclarePackage(fmt
  GIT_TAG 10.1.1
  GITHUB_REPOSITORY fmtlib/fmt
  SYSTEM YES
  EXCLUDE_FROM_ALL YES
)
# terminal
CPMDeclarePackage(terminal
  VERSION 2.1.0
  GITLAB_REPOSITORY sat-polsl/software/packages/terminal
  SYSTEM YES
  EXCLUDE_FROM_ALL YES
)
# telemetry
CPMDeclarePackage(telemetry
  VERSION 2.1.0
  GITLAB_REPOSITORY sat-polsl/software/packages/telemetry
  SYSTEM YES
  EXCLUDE_FROM_ALL YES
)
# safe
CPMDeclarePackage(safe
  VERSION 1.1.0
  GITLAB_REPOSITORY sat-polsl/software/packages/safe
  SYSTEM YES
  EXCLUDE_FROM_ALL YES
)
