#include <array>
#include <cstddef>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "communication/can/decoders/echo_message_decoder.h"
#include "communication/can/encoders/echo_message_encoder.h"
#include "communication/can/handlers/echo_message_handler.h"

namespace {
using namespace ::testing;
using namespace communication::can;
using namespace spl::peripherals::can;

TEST(EchoMessageDecoderTest, DecodeSuccess) {
    std::array<std::byte, 8> buffer{std::byte{0xde},
                                    std::byte{0xad},
                                    std::byte{0xc0},
                                    std::byte{0xde},
                                    std::byte{0xde},
                                    std::byte{0xad},
                                    std::byte{0xbe},
                                    std::byte{0xef}};
    concepts::message msg{
        .data = buffer, .size = buffer.size(), .matched_filter = concepts::filter{0}};
    auto result = decoders::echo_message_decoder::decode(msg);

    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result->buffer(), ContainerEq(buffer));
    ASSERT_THAT(result->size(), Eq(buffer.size()));
}

TEST(EchoMessageEncoderTest, EncodeSuccess) {
    std::array<std::byte, 8> buffer{std::byte{0xde},
                                    std::byte{0xad},
                                    std::byte{0xc0},
                                    std::byte{0xde},
                                    std::byte{0xde},
                                    std::byte{0xad},
                                    std::byte{0xbe},
                                    std::byte{0xef}};
    messages::echo_message msg{buffer, buffer.size()};
    concepts::message output;
    auto result = encoders::echo_message_encoder::encode(msg, output);

    ASSERT_THAT(result, Eq(communication::status::ok));
    ASSERT_THAT(output.data, ContainerEq(buffer));
    ASSERT_THAT(output.size, Eq(buffer.size()));
}

TEST(EchoMessageHandlerTest, HandleSuccess) {
    handlers::echo_message_handler handler;
    messages::echo_message msg{std::array<std::byte, 8>{std::byte{0xde},
                                                        std::byte{0xad},
                                                        std::byte{0xc0},
                                                        std::byte{0xde},
                                                        std::byte{0xde},
                                                        std::byte{0xad},
                                                        std::byte{0xbe},
                                                        std::byte{0xef}},
                               8};

    auto result = handler.handle(msg);

    ASSERT_THAT(result.buffer(), ContainerEq(msg.buffer()));
    ASSERT_THAT(result.size(), Eq(msg.size()));
}

} // namespace
