#include <array>
#include <cstddef>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "communication/can/decoders/get_telemetry_request_decoder.h"
#include "communication/can/encoders/get_telemetry_response_encoder.h"
#include "communication/can/handlers/get_telemetry_handler.h"
#include "telemetry/mock/telemetry_state.h"

namespace {
using namespace ::testing;
using namespace communication::can;
using namespace spl::peripherals::can;

TEST(GetTelemetryDecoderTest, DecodeSuccess) {
    std::array<std::byte, 8> buffer{std::byte{0x12}, std::byte{0x34}};
    concepts::message msg{.data = buffer, .size = 2, .matched_filter = concepts::filter{2}};
    auto result = decoders::get_telemetry_request_decoder::decode(msg);

    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result->id(), telemetry::telemetry_id{0x1234});
}

TEST(GetTelemetryDecoderTest, DecodeFailure) {
    std::array<std::byte, 8> buffer{};
    concepts::message msg{.data = buffer, .size = 3, .matched_filter = concepts::filter{2}};
    auto result = decoders::get_telemetry_request_decoder::decode(msg);

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(communication::status::decoder_error));
}

TEST(GetTelemetryEncoderTest, EncodeU32Success) {
    messages::get_telemetry_response msg{telemetry::telemetry_id{11}, std::uint32_t{0xdeadbeef}};
    concepts::message output{};
    auto result = encoders::get_telemetry_response_encoder::encode(msg, output);

    ASSERT_THAT(result, Eq(communication::status::ok));
    ASSERT_THAT(output.size, Eq(6));
    ASSERT_THAT(output.data[0], Eq(std::byte{0xb3}));
    ASSERT_THAT(output.data[1], Eq(std::byte{0x00}));
    ASSERT_THAT(output.data[2], Eq(std::byte{0xef}));
    ASSERT_THAT(output.data[3], Eq(std::byte{0xbe}));
    ASSERT_THAT(output.data[4], Eq(std::byte{0xad}));
    ASSERT_THAT(output.data[5], Eq(std::byte{0xde}));
}

using handler_type = handlers::get_telemetry_handler<telemetry::mock::telemetry_state>;

struct GetTelemetryHandlerTest : public Test {
    StrictMock<telemetry::mock::telemetry_state> state_;
};

TEST_F(GetTelemetryHandlerTest, HandleSuccess) {
    handler_type handler(state_);

    EXPECT_CALL(state_, read(telemetry::telemetry_id{1})).WillOnce(Return(std::int32_t{2137}));

    auto result = handler.handle(messages::get_telemetry_request{telemetry::telemetry_id{1}});
    ASSERT_THAT(result.id(), Eq(telemetry::telemetry_id{1}));
    auto value = result.value();
    auto* p = std::get_if<std::int32_t>(&value);
    ASSERT_THAT(p, Ne(nullptr));
    ASSERT_THAT(*p, Eq(2137));
}

} // namespace
