#include <array>
#include <cstddef>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "communication/can/decoders/power_output_on_decoder.h"
#include "communication/can/encoders/power_output_on_encoder.h"
#include "communication/can/handlers/power_output_on_handler.h"
#include "power_service/mock/power_service.h"

namespace {
using namespace ::testing;
using namespace communication::can;
using namespace spl::peripherals::can;

TEST(PowerOutputOnDecoderTest, DecodeSuccess) {
    std::array<std::byte, 8> buffer{std::byte{0x02}};
    concepts::message msg{.data = buffer, .size = 1, .matched_filter = concepts::filter{1}};
    auto result = decoders::power_output_on_decoder::decode(msg);

    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result->output(), power_service::power_output::v3v3_3);
}

TEST(PowerOutputOnDecoderTest, DecodeFailure) {
    std::array<std::byte, 8> buffer{std::byte{0x02}};
    concepts::message msg{.data = buffer, .size = 2, .matched_filter = concepts::filter{1}};
    auto result = decoders::power_output_on_decoder::decode(msg);

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(communication::status::decoder_error));
}

TEST(PowerOutputOnEncoderTest, EncodeSuccess) {
    messages::power_output_on_message msg{power_service::power_output::v12};
    concepts::message output;
    auto result = encoders::power_output_on_encoder::encode(msg, output);

    ASSERT_THAT(result, Eq(communication::status::ok));
    ASSERT_THAT(output.data[0], Eq(std::byte{0x06}));
    ASSERT_THAT(output.size, Eq(1));
}

using handler_type = handlers::power_output_on_handler<power_service::mock::power_service>;

struct PowerOutputOnHandlerTest : public Test {
    StrictMock<power_service::mock::power_service> power_service_;
};

TEST_F(PowerOutputOnHandlerTest, HandleSuccess) {
    handler_type handler(power_service_);

    EXPECT_CALL(power_service_, enable(power_service::power_output::v12));

    handler.handle(messages::power_output_on_message{power_service::power_output::v12});
}

} // namespace
