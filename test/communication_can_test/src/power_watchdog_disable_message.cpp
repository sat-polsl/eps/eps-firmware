#include <array>
#include <cstddef>
#include "communication/can/decoders/power_watchdog_disable_decoder.h"
#include "communication/can/encoders/power_watchdog_disable_encoder.h"
#include "communication/can/enums.h"
#include "communication/can/handlers/power_watchdog_disable_handler.h"
#include "power_service/mock/power_service.h"

#include "gtest/gtest.h"
#include "gmock/gmock.h"

namespace {

using namespace ::testing;
using namespace communication::can;
using namespace power_service;
using namespace spl::peripherals::can;

std::array<power_output, 7> params{power_output::v3v3_1,
                                   power_output::v3v3_2,
                                   power_output::v3v3_3,
                                   power_output::v3v3_4,
                                   power_output::v5_1,
                                   power_output::v5_2,
                                   power_output::v12};

class PowerWatchdogDisableDecoderTest : public TestWithParam<power_output> {
public:
    PowerWatchdogDisableDecoderTest() :
        msg_buffer_{std::byte(GetParam())},
        watchdog_disable_msg_{
            .data = msg_buffer_, .size = 1, .matched_filter = concepts::filter{2}} {}

    std::array<std::byte, 8> msg_buffer_;
    concepts::message watchdog_disable_msg_{0};
};

TEST_P(PowerWatchdogDisableDecoderTest, DecodeSuccess) {
    auto result = decoders::power_watchdog_disable_decoder::decode(watchdog_disable_msg_);

    ASSERT_TRUE(result.has_value());
    ASSERT_THAT(result->output(), power_output(GetParam()));
}

TEST(PowerWatchdogDisableDecoderSingleTest, DecodeFailure) {
    constexpr std::uint8_t Test_output = 0x02;
    std::array<std::byte, 8> buffer{std::byte{Test_output}};
    concepts::message faulty_watchdog_disable_msg{
        .data = buffer, .size = 2, .matched_filter = concepts::filter{2}};
    auto result = decoders::power_watchdog_disable_decoder::decode(faulty_watchdog_disable_msg);

    ASSERT_FALSE(result.has_value());
    ASSERT_THAT(result.error(), Eq(communication::status::decoder_error));
}

INSTANTIATE_TEST_SUITE_P(power_watchdog_disable_decoder_test,
                         PowerWatchdogDisableDecoderTest,
                         ValuesIn(params));

class PowerWatchdogDisableEncoderTest : public TestWithParam<power_output> {
public:
    messages::power_watchdog_disable_message watchdog_disable_msg_{GetParam()};
};

TEST_P(PowerWatchdogDisableEncoderTest, EncodeSuccess) {
    concepts::message output;
    auto result = encoders::power_watchdog_disable_encoder::encode(watchdog_disable_msg_, output);

    ASSERT_THAT(result, Eq(communication::status::ok));
    ASSERT_THAT(output.data[0], Eq(std::byte(GetParam())));
    ASSERT_THAT(output.size, Eq(1));
}

INSTANTIATE_TEST_SUITE_P(power_watchdog_disable_decoder_test,
                         PowerWatchdogDisableEncoderTest,
                         ValuesIn(params));

using handler_type = handlers::power_watchdog_disable_handler<mock::power_service>;

struct PowerWatchdogDisableHandlerTest : public TestWithParam<power_output> {
    StrictMock<mock::power_service> power_service_;
};

TEST_P(PowerWatchdogDisableHandlerTest, HandleSuccess) {
    handler_type handler(power_service_);

    EXPECT_CALL(power_service_, disable_watchdog(GetParam()));

    handler.handle(messages::power_watchdog_disable_message{GetParam()});
}

INSTANTIATE_TEST_SUITE_P(power_watchdog_disable_decoder_test,
                         PowerWatchdogDisableHandlerTest,
                         ValuesIn(params));

} // namespace
