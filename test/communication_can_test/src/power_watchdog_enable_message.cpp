#include <array>
#include <cstddef>
#include "communication/can/decoders/power_watchdog_enable_decoder.h"
#include "communication/can/encoders/power_watchdog_enable_encoder.h"
#include "communication/can/enums.h"
#include "communication/can/handlers/power_watchdog_enable_handler.h"
#include "power_service/mock/power_service.h"

#include "gtest/gtest.h"
#include "gmock/gmock.h"

namespace {

using namespace ::testing;
using namespace communication::can;
using namespace power_service;
using namespace spl::peripherals::can;

std::array<power_output, 7> params{power_output::v3v3_1,
                                   power_output::v3v3_2,
                                   power_output::v3v3_3,
                                   power_output::v3v3_4,
                                   power_output::v5_1,
                                   power_output::v5_2,
                                   power_output::v12};

class PowerWatchdogEnableDecoderTest : public TestWithParam<power_output> {
public:
    PowerWatchdogEnableDecoderTest() :
        msg_buffer_{std::byte(GetParam())},
        watchdog_enable_msg_{
            .data = msg_buffer_, .size = 1, .matched_filter = concepts::filter{2}} {}

    std::array<std::byte, 8> msg_buffer_;
    concepts::message watchdog_enable_msg_{0};
};

TEST_P(PowerWatchdogEnableDecoderTest, DecodeSuccess) {
    auto result = decoders::power_watchdog_enable_decoder::decode(watchdog_enable_msg_);

    ASSERT_TRUE(result.has_value());
    ASSERT_THAT(result->output(), power_output(GetParam()));
}

TEST(PowerWatchdogEnableDecoderSingleTest, DecodeFailure) {
    constexpr std::uint8_t Test_output = 0x02;
    std::array<std::byte, 8> buffer{std::byte{Test_output}};
    concepts::message faulty_watchdog_enable_msg{
        .data = buffer, .size = 2, .matched_filter = concepts::filter{2}};
    auto result = decoders::power_watchdog_enable_decoder::decode(faulty_watchdog_enable_msg);

    ASSERT_FALSE(result.has_value());
    ASSERT_THAT(result.error(), Eq(communication::status::decoder_error));
}

INSTANTIATE_TEST_SUITE_P(power_watchdog_enable_decoder_test,
                         PowerWatchdogEnableDecoderTest,
                         ValuesIn(params));

class PowerWatchdogEnableEncoderTest : public TestWithParam<power_output> {
public:
    messages::power_watchdog_enable_message watchdog_enable_msg_{GetParam()};
};

TEST_P(PowerWatchdogEnableEncoderTest, EncodeSuccess) {
    concepts::message output;
    auto result = encoders::power_watchdog_enable_encoder::encode(watchdog_enable_msg_, output);

    ASSERT_THAT(result, Eq(communication::status::ok));
    ASSERT_THAT(output.data[0], Eq(std::byte(GetParam())));
    ASSERT_THAT(output.size, Eq(1));
}

INSTANTIATE_TEST_SUITE_P(power_watchdog_enable_decoder_test,
                         PowerWatchdogEnableEncoderTest,
                         ValuesIn(params));

using handler_type = handlers::power_watchdog_enable_handler<mock::power_service>;

struct PowerWatchdogEnableHandlerTest : public TestWithParam<power_output> {
    StrictMock<mock::power_service> power_service_;
};

TEST_P(PowerWatchdogEnableHandlerTest, HandleSuccess) {
    handler_type handler(power_service_);

    EXPECT_CALL(power_service_, enable_watchdog(GetParam()));

    handler.handle(messages::power_watchdog_enable_message{GetParam()});
}

INSTANTIATE_TEST_SUITE_P(power_watchdog_enable_decoder_test,
                         PowerWatchdogEnableHandlerTest,
                         ValuesIn(params));

} // namespace
