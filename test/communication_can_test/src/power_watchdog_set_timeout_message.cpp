#include <array>
#include <cstddef>
#include <stdlib.h>
#include "communication/can/decoders/power_watchdog_set_timeout_decoder.h"
#include "communication/can/encoders/power_watchdog_set_timeout_encoder.h"
#include "communication/can/enums.h"
#include "communication/can/handlers/power_watchdog_set_timeout_handler.h"
#include "power_service/mock/power_service.h"

#include "gtest/gtest.h"
#include "gmock/gmock.h"

namespace {

using namespace ::testing;
using namespace communication::can;
using namespace power_service;
using namespace spl::peripherals::can;
using namespace satos::chrono;

std::array<power_output, 7> params{power_output::v3v3_1,
                                   power_output::v3v3_2,
                                   power_output::v3v3_3,
                                   power_output::v3v3_4,
                                   power_output::v5_1,
                                   power_output::v5_2,
                                   power_output::v12};

class PowerWatchdogSetTimeoutDecoderTest : public TestWithParam<power_output> {
public:
    PowerWatchdogSetTimeoutDecoderTest() :
        msg_buffer_{
            std::byte(GetParam()), std::byte(test_timeout_ >> 8), std::byte(test_timeout_ & 0xFF)},
        watchdog_set_timeout_msg_{
            .data = msg_buffer_, .size = 3, .matched_filter = concepts::filter{2}} {}

    uint16_t test_timeout_ = rand();
    std::array<std::byte, 8> msg_buffer_;
    concepts::message watchdog_set_timeout_msg_{0};
};

TEST_P(PowerWatchdogSetTimeoutDecoderTest, DecodeSuccess) {
    auto result = decoders::power_watchdog_set_timeout_decoder::decode(watchdog_set_timeout_msg_);

    ASSERT_TRUE(result.has_value());
    ASSERT_THAT(result->output(), GetParam());
    ASSERT_EQ(result->timeout(), seconds(test_timeout_));
}

TEST(PowerWatchdogSetTimeoutDecoderSingleTest, DecodeFailure) {
    constexpr std::uint8_t Test_output = 0x02;
    std::array<std::byte, 8> buffer{std::byte{Test_output}, std::byte{0}, std::byte{1}};
    concepts::message faulty_watchdog_set_timeout_msg{
        .data = buffer, .size = 5, .matched_filter = concepts::filter{2}};

    auto result =
        decoders::power_watchdog_set_timeout_decoder::decode(faulty_watchdog_set_timeout_msg);

    ASSERT_FALSE(result.has_value());
    ASSERT_THAT(result.error(), Eq(communication::status::decoder_error));
}

INSTANTIATE_TEST_SUITE_P(power_watchdog_set_timeout_decoder_test,
                         PowerWatchdogSetTimeoutDecoderTest,
                         ValuesIn(params));

class PowerWatchdogSetTimeoutEncoderTest : public TestWithParam<power_output> {
public:
    messages::power_watchdog_set_timeout_message watchdog_set_timeout_msg_{GetParam(), seconds(0)};
};

TEST_P(PowerWatchdogSetTimeoutEncoderTest, EncodeSuccess) {
    concepts::message output;
    auto result =
        encoders::power_watchdog_set_timeout_encoder::encode(watchdog_set_timeout_msg_, output);

    ASSERT_THAT(result, Eq(communication::status::ok));
    ASSERT_THAT(output.data[0], Eq(std::byte(GetParam())));
    ASSERT_THAT(output.size, Eq(1));
}

INSTANTIATE_TEST_SUITE_P(power_watchdog_set_timeout_decoder_test,
                         PowerWatchdogSetTimeoutEncoderTest,
                         ValuesIn(params));

using handler_type = handlers::power_watchdog_set_timeout_handler<mock::power_service>;

struct PowerWatchdogSetTimeoutHandlerTest : public TestWithParam<power_output> {
    StrictMock<mock::power_service> power_service_;
};

TEST_P(PowerWatchdogSetTimeoutHandlerTest, HandleSuccess) {
    seconds test_timeout_ = seconds(rand());
    handler_type handler(power_service_);

    EXPECT_CALL(power_service_, set_watchdog_timeout(GetParam(), _));

    handler.handle(messages::power_watchdog_set_timeout_message{GetParam(), test_timeout_});
}

INSTANTIATE_TEST_SUITE_P(power_watchdog_set_timeout_decoder_test,
                         PowerWatchdogSetTimeoutHandlerTest,
                         ValuesIn(params));

} // namespace
