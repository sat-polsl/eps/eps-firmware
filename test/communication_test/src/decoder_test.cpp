#include <array>
#include <cstdint>
#include <span>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "communication/decoder.h"
#include "communication/decoder_base.h"

namespace {
using namespace ::testing;

struct test_message {
    std::array<std::byte, 4> buffer;
};

struct another_test_message {
    std::array<std::byte, 2> buffer;
};

class message_decoder
    : public communication::decoder_base<test_message, std::byte, std::byte{0xab}> {
public:
    static decoded_return_type decode(std::span<std::byte> buffer) {
        type result;
        if (buffer.size() != result.buffer.size()) {
            return satext::unexpected{communication::status::decoder_error};
        }
        std::copy_n(buffer.begin(), buffer.size(), result.buffer.begin());
        return result;
    }
};

class another_message_decoder
    : public communication::decoder_base<another_test_message, std::byte, std::byte{0xac}> {
public:
    static decoded_return_type decode(std::span<std::byte> buffer) {
        type result;
        if (buffer.size() != result.buffer.size()) {
            return satext::unexpected{communication::status::decoder_error};
        }
        std::copy_n(buffer.begin(), buffer.size(), result.buffer.begin());
        return result;
    }
};

struct DecoderTest : public Test {
    using decoder = communication::
        decoder<std::span<std::byte>, std::byte, message_decoder, another_message_decoder>;
    decoder decoder_{
        [](const std::span<std::byte> buffer, std::byte id) -> std::optional<std::span<std::byte>> {
            if (buffer[0] == id) {
                return buffer.subspan(1);
            } else {
                return std::nullopt;
            }
        }};
};

TEST_F(DecoderTest, DecodeSuccess) {
    std::array<std::byte, 5> buffer = {
        std::byte{0xab}, std::byte{0xde}, std::byte{0xad}, std::byte{0xc0}, std::byte{0xde}};
    auto decoded = decoder_.decode(buffer);

    ASSERT_THAT(decoded.has_value(), Eq(true));

    auto* decoded_ptr = std::get_if<test_message>(&decoded.value());

    ASSERT_THAT(decoded_ptr, Ne(nullptr));

    for (std::size_t i = 0; i < decoded_ptr->buffer.size(); i++) {
        ASSERT_THAT(decoded_ptr->buffer[i], Eq(buffer[i + 1]));
    }
}

TEST_F(DecoderTest, DecodeAnotherSuccess) {
    std::array<std::byte, 3> buffer = {std::byte{0xac}, std::byte{0xde}, std::byte{0xad}};
    auto decoded = decoder_.decode(buffer);

    ASSERT_THAT(decoded.has_value(), Eq(true));

    auto* decoded_ptr = std::get_if<another_test_message>(&decoded.value());

    ASSERT_THAT(decoded_ptr, Ne(nullptr));

    for (std::size_t i = 0; i < decoded_ptr->buffer.size(); i++) {
        ASSERT_THAT(decoded_ptr->buffer[i], Eq(buffer[i + 1]));
    }
}

TEST_F(DecoderTest, DecodeUnsupported) {
    std::array<std::byte, 1> buffer = {std::byte{0xff}};
    auto decoded = decoder_.decode(buffer);

    ASSERT_THAT(decoded.has_value(), Eq(false));
    ASSERT_THAT(decoded.error(), Eq(communication::status::unsupported_message));
}

TEST_F(DecoderTest, DecodeError) {
    std::array<std::byte, 2> buffer = {std::byte{0xab}, std::byte{0xde}};
    auto decoded = decoder_.decode(buffer);

    ASSERT_THAT(decoded.has_value(), Eq(false));
    ASSERT_THAT(decoded.error(), Eq(communication::status::decoder_error));
}

} // namespace
