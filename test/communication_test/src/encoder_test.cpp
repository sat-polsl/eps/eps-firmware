#include <array>
#include <span>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "communication/encoder.h"
#include "communication/encoder_base.h"

namespace {
using namespace ::testing;

struct test_message {
    std::array<std::byte, 4> buffer;
};

struct another_test_message {
    std::array<std::byte, 2> buffer;
};

class message_encoder
    : public communication::encoder_base<test_message, std::byte, std::byte{0xab}> {
public:
    static communication::status encode(const type& msg, std::span<std::byte> buffer) {
        if (buffer.size() != msg.buffer.size()) {
            return communication::status::encoder_error;
        }
        std::copy_n(msg.buffer.begin(), msg.buffer.size(), buffer.begin());
        return communication::status::ok;
    }
};

class another_message_encoder
    : public communication::encoder_base<another_test_message, std::byte, std::byte{0xac}> {
public:
    static communication::status encode(const type& msg, std::span<std::byte> buffer) {
        if (buffer.size() != msg.buffer.size()) {
            return communication::status::encoder_error;
        }
        std::copy_n(msg.buffer.begin(), msg.buffer.size(), buffer.begin());
        return communication::status::ok;
    }
};

struct EncoderTest : public Test {
    using encoder = communication::
        encoder<std::span<std::byte>, std::byte, message_encoder, another_message_encoder>;
    encoder encoder_{
        [](std::span<std::byte> buffer, std::byte id) -> std::optional<std::span<std::byte>> {
            if (buffer.empty()) {
                return std::nullopt;
            }
            buffer[0] = id;
            return buffer.subspan(1);
        }};
};

TEST_F(EncoderTest, EncodeSuccess) {
    std::array<std::byte, 4> buffer{
        std::byte{0xde}, std::byte{0xad}, std::byte{0xc0}, std::byte{0xde}};
    encoder::encodable to_encode{test_message{.buffer = buffer}};

    std::array<std::byte, 5> output{};

    auto result = encoder_.encode(to_encode, output);

    ASSERT_THAT(result, Eq(communication::status::ok));
    ASSERT_THAT(output[0], Eq(std::byte{0xab}));
    for (int i = 0; i < 4; i++) {
        ASSERT_THAT(output[i + 1], Eq(buffer[i]));
    }
}

TEST_F(EncoderTest, EncodeAnotherSuccess) {
    std::array<std::byte, 2> buffer{std::byte{0xde}, std::byte{0xad}};
    encoder::encodable to_encode{another_test_message{.buffer = buffer}};

    std::array<std::byte, 3> output{};

    auto result = encoder_.encode(to_encode, output);

    ASSERT_THAT(result, Eq(communication::status::ok));
    ASSERT_THAT(output[0], Eq(std::byte{0xac}));
    for (int i = 0; i < 2; i++) {
        ASSERT_THAT(output[i + 1], Eq(buffer[i]));
    }
}

TEST_F(EncoderTest, EncodeError) {
    std::array<std::byte, 2> buffer{std::byte{0xde}, std::byte{0xad}};
    encoder::encodable to_encode{another_test_message{.buffer = buffer}};

    std::array<std::byte, 1> output{};

    auto result = encoder_.encode(to_encode, output);

    ASSERT_THAT(result, Eq(communication::status::encoder_error));
}

} // namespace
