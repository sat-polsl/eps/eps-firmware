#include <array>
#include <cstdint>
#include <span>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "communication/decoder.h"
#include "communication/decoder_base.h"
#include "communication/encoder.h"
#include "communication/encoder_base.h"
#include "communication/message_handler.h"

namespace {
using namespace ::testing;

struct test_message {
    std::array<std::byte, 4> buffer;
};

struct another_test_message {
    std::array<std::byte, 2> buffer;
};

struct void_message {};

class message_decoder
    : public communication::decoder_base<test_message, std::byte, std::byte{0xab}> {
public:
    static decoded_return_type decode(std::span<std::byte> buffer) {
        type result;
        if (buffer.size() != result.buffer.size()) {
            return satext::unexpected{communication::status::decoder_error};
        }
        std::copy_n(buffer.begin(), buffer.size(), result.buffer.begin());
        return result;
    }
};

class another_message_decoder
    : public communication::decoder_base<another_test_message, std::byte, std::byte{0xac}> {
public:
    static decoded_return_type decode(std::span<std::byte> buffer) {
        type result;
        if (buffer.size() != result.buffer.size()) {
            return satext::unexpected{communication::status::decoder_error};
        }
        std::copy_n(buffer.begin(), buffer.size(), result.buffer.begin());
        return result;
    }
};

class void_message_decoder
    : public communication::decoder_base<void_message, std::byte, std::byte{0xbb}> {
public:
    static decoded_return_type decode(std::span<std::byte> buffer) { return void_message{}; }
};

class message_encoder
    : public communication::encoder_base<test_message, std::byte, std::byte{0xad}> {
public:
    static communication::status encode(const type& msg, std::span<std::byte> buffer) {
        if (buffer.size() != msg.buffer.size()) {
            return communication::status::encoder_error;
        }
        std::copy_n(msg.buffer.begin(), msg.buffer.size(), buffer.begin());
        return communication::status::ok;
    }
};

class another_message_encoder
    : public communication::encoder_base<another_test_message, std::byte, std::byte{0xae}> {
public:
    static communication::status encode(const type& msg, std::span<std::byte> buffer) {
        if (buffer.size() != msg.buffer.size()) {
            return communication::status::encoder_error;
        }
        std::copy_n(msg.buffer.begin(), msg.buffer.size(), buffer.begin());
        return communication::status::ok;
    }
};

class message_handler {
public:
    test_message handle(const test_message& msg) {
        auto result = msg;
        for (auto& v : result.buffer) {
            v = std::byte(static_cast<std::uint8_t>(v) + 1);
        }
        return result;
    }
};

class another_message_handler {
public:
    another_test_message handle(const another_test_message& msg) {
        auto result = msg;
        for (auto& v : result.buffer) {
            v = std::byte(static_cast<std::uint8_t>(v) + 1);
        }
        return result;
    }
};

class void_message_handler {
public:
    void handle(const void_message& msg) {}
};

struct MessageHandlerTest : public Test {
    using decoder = communication::decoder<std::span<std::byte>,
                                           std::byte,
                                           message_decoder,
                                           another_message_decoder,
                                           void_message_decoder>;
    using encoder = communication::
        encoder<std::span<std::byte>, std::byte, message_encoder, another_message_encoder>;
    using handler = communication::message_handler<std::span<std::byte>,
                                                   decoder,
                                                   encoder,
                                                   message_handler,
                                                   another_message_handler,
                                                   void_message_handler>;
    handler handler_{decoder{[](const std::span<std::byte> buffer,
                                std::byte id) -> std::optional<std::span<std::byte>> {
                         if (buffer[0] == id) {
                             return buffer.subspan(1);
                         } else {
                             return std::nullopt;
                         }
                     }},
                     encoder{[](std::span<std::byte> buffer,
                                std::byte id) -> std::optional<std::span<std::byte>> {
                         if (buffer.empty()) {
                             return std::nullopt;
                         }
                         buffer[0] = id;
                         return buffer.subspan(1);
                     }},
                     message_handler{},
                     another_message_handler{},
                     void_message_handler{}};
};

TEST_F(MessageHandlerTest, HandleSuccess) {
    std::array<std::byte, 5> input = {
        std::byte{0xab}, std::byte{0xde}, std::byte{0xad}, std::byte{0xc0}, std::byte{0xde}};
    std::array<std::byte, 5> output{};
    std::array<std::byte, 5> expected_output = {
        std::byte{0xad}, std::byte{0xdf}, std::byte{0xae}, std::byte{0xc1}, std::byte{0xdf}};

    auto result = handler_.handle(input, output);
    ASSERT_THAT(result, Eq(communication::status::ok));
    ASSERT_THAT(output, ContainerEq(expected_output));
}

TEST_F(MessageHandlerTest, HandleSuccessAnother) {
    std::array<std::byte, 3> input = {std::byte{0xac}, std::byte{0xde}, std::byte{0xad}};
    std::array<std::byte, 3> output{};
    std::array<std::byte, 3> expected_output = {std::byte{0xae}, std::byte{0xdf}, std::byte{0xae}};

    auto result = handler_.handle(input, output);
    ASSERT_THAT(result, Eq(communication::status::ok));
    ASSERT_THAT(output, ContainerEq(expected_output));
}

TEST_F(MessageHandlerTest, HandleSuccessVoid) {
    std::array<std::byte, 1> input = {std::byte{0xbb}};
    std::array<std::byte, 1> output{};

    auto result = handler_.handle(input, output);
    ASSERT_THAT(result, Eq(communication::status::void_handle));
}

TEST_F(MessageHandlerTest, HandleUnsupported) {
    std::array<std::byte, 3> input = {std::byte{0xa0}, std::byte{0xde}, std::byte{0xad}};
    std::array<std::byte, 3> output{};

    auto result = handler_.handle(input, output);
    ASSERT_THAT(result, Eq(communication::status::unsupported_message));
}

} // namespace
