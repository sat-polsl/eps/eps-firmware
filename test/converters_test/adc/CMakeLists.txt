set(TARGET adc_converters_test)

add_executable(${TARGET})

target_sources(${TARGET} PRIVATE
    src/adc_converters_test.cpp
    )

target_link_libraries(${TARGET}
    PRIVATE
    adc_converters
    )

add_unit_test(${TARGET})
