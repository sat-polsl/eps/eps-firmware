#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "converters/adc/current_adc.h"
#include "converters/adc/enums.h"
#include "converters/adc/voltage_adc.h"

namespace {
using namespace ::testing;
using namespace converters::adc;

TEST(AdcConvertersTest, VoltageAdc) {
    std::array<std::uint16_t, 7> input = {2000, 2000, 2000, 2000, 2000, 2000, 2000};
    std::array<std::uint16_t, 7> expected = {12000, 5030, 3298, 5030, 3298, 3298, 3298};

    convert_voltage_adc(input);

    ASSERT_THAT(input, ContainerEq(expected));
}

TEST(AdcConvertersTest, VoltageAdcChannel) {
    auto result = convert_voltage_adc_channel(2000, voltage_channel::v5_l1);

    ASSERT_THAT(result, 5030);
}

TEST(AdcConvertersTest, CurrentAdc) {
    std::array<std::uint16_t, 12> input = {
        2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000};
    std::array<std::uint16_t, 12> expected = {
        1000, 1000, 1000, 5030, 4222, 1000, 1000, 1000, 1000, 1000, 1000, 1000};

    convert_current_adc(input);

    ASSERT_THAT(input, ContainerEq(expected));
}

TEST(AdcConvertersTest, CurrentAdcChannel) {
    auto result = convert_current_adc_channel(2000, current_channel::i12);

    ASSERT_THAT(result, 1000);
}
} // namespace
