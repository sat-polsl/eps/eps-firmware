#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/adc/adc.h"
#include "devices/adc/adc_concept.h"
#include "satos/chrono.h"
#include "spl/drivers/spi/mock/spi.h"
#include "spl/peripherals/gpio/mock/gpio.h"

namespace {

using namespace ::testing;
using namespace devices::adc;
using namespace satos::chrono_literals;

using adc8_type =
    adc<spl::peripherals::gpio::mock::gpio, spl::drivers::spi::mock::spi_vectored, 8, 8>;
using adc12_type =
    adc<spl::peripherals::gpio::mock::gpio, spl::drivers::spi::mock::spi_vectored, 8, 12>;

static_assert(adc_concept<adc8_type>);
static_assert(adc_concept<adc12_type>);

struct AdcTest : Test {
    StrictMock<spl::peripherals::gpio::mock::gpio> gpio_;
    StrictMock<spl::drivers::spi::mock::spi_vectored> spi_;
};

TEST_F(AdcTest, Initialize) {
    adc8_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(1));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0x70)));

            EXPECT_THAT(timeout, Eq(1_s));

            return spl::drivers::spi::status::ok;
        }));

    adc.initialize();
}

TEST_F(AdcTest, Reset) {
    adc8_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(1));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0x18)));

            EXPECT_THAT(timeout, Eq(1_s));

            return spl::drivers::spi::status::ok;
        }));

    adc.reset();
}

TEST_F(AdcTest, ReadChannelRawSuccess) {
    adc8_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0xfe)));

            EXPECT_THAT(timeout, Eq(1_s));

            buffer[0][1] = std::byte{0x0f};
            buffer[0][2] = std::byte{0xf0};

            return spl::drivers::spi::status::ok;
        }));

    auto result = adc.read_channel_raw(devices::adc::channel::channel15);
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(0xff));
}

TEST_F(AdcTest, ReadChannelRawFailure) {
    adc8_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0xbe)));

            EXPECT_THAT(timeout, Eq(1_s));

            return spl::drivers::spi::status::busy;
        }));

    auto result = adc.read_channel_raw(devices::adc::channel::channel7);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::spi::status::busy));
}

TEST_F(AdcTest, ReadAllRawSuccess) {
    adc8_type adc{spi_, gpio_, 1_s};
    std::array<std::byte, 17> expected_buffer{std::byte{0x86},
                                              std::byte{0}, // A0
                                              std::byte{0x8e},
                                              std::byte{0}, // A1
                                              std::byte{0x96},
                                              std::byte{0}, // A2
                                              std::byte{0x9e},
                                              std::byte{0}, // A3
                                              std::byte{0xa6},
                                              std::byte{0}, // A4
                                              std::byte{0xae},
                                              std::byte{0}, // A5
                                              std::byte{0xb6},
                                              std::byte{0}, // A6
                                              std::byte{0xbe},
                                              std::byte{0}, // A7
                                              std::byte{0x0}};

    std::array<std::uint16_t, 8> expected_result{0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this, expected_buffer](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(expected_buffer.size()));
            for (std::size_t i = 0; i < expected_buffer.size(); i++) {
                EXPECT_THAT(buffer[0][i], Eq(expected_buffer[i]));
            }

            buffer[0][1] = std::byte{0x00};
            buffer[0][2] = std::byte{0x10};
            buffer[0][3] = std::byte{0x00};
            buffer[0][4] = std::byte{0x30};
            buffer[0][5] = std::byte{0x00};
            buffer[0][6] = std::byte{0x70};
            buffer[0][7] = std::byte{0x00};
            buffer[0][8] = std::byte{0xf0};
            buffer[0][9] = std::byte{0x01};
            buffer[0][10] = std::byte{0xf0};
            buffer[0][11] = std::byte{0x03};
            buffer[0][12] = std::byte{0xf0};
            buffer[0][13] = std::byte{0x07};
            buffer[0][14] = std::byte{0xf0};
            buffer[0][15] = std::byte{0x0f};
            buffer[0][16] = std::byte{0xf0};

            EXPECT_THAT(timeout, Eq(1_s));

            return spl::drivers::spi::status::ok;
        }));

    auto result = adc.read_all_raw();
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), ContainerEq(expected_result));
}

TEST_F(AdcTest, ReadAllRawFailure) {
    adc8_type adc{spi_, gpio_, 1_s};
    std::array<std::byte, 17> expected_buffer{std::byte{0x86},
                                              std::byte{0}, // A0
                                              std::byte{0x8e},
                                              std::byte{0}, // A1
                                              std::byte{0x96},
                                              std::byte{0}, // A2
                                              std::byte{0x9e},
                                              std::byte{0}, // A3
                                              std::byte{0xa6},
                                              std::byte{0}, // A4
                                              std::byte{0xae},
                                              std::byte{0}, // A5
                                              std::byte{0xb6},
                                              std::byte{0}, // A6
                                              std::byte{0xbe},
                                              std::byte{0}, // A7
                                              std::byte{0x0}};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this, expected_buffer](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(expected_buffer.size()));
            for (std::size_t i = 0; i < expected_buffer.size(); i++) {
                EXPECT_THAT(buffer[0][i], Eq(expected_buffer[i]));
            }

            EXPECT_THAT(timeout, Eq(1_s));

            return spl::drivers::spi::status::busy;
        }));

    auto result = adc.read_all_raw();
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::spi::status::busy));
}

TEST_F(AdcTest, ReadChannelSuccess) {
    adc8_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0x8e)));

            EXPECT_THAT(timeout, Eq(1_s));

            buffer[0][1] = std::byte{0x0f};
            buffer[0][2] = std::byte{0xf0};

            return spl::drivers::spi::status::ok;
        }));

    auto result = adc.read_channel(devices::adc::channel::channel1);
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(2500));
}

TEST_F(AdcTest, ReadAllSuccess) {
    adc8_type adc{spi_, gpio_, 1_s};
    std::array<std::byte, 17> expected_buffer{std::byte{0x86},
                                              std::byte{0}, // A0
                                              std::byte{0x8e},
                                              std::byte{0}, // A1
                                              std::byte{0x96},
                                              std::byte{0}, // A2
                                              std::byte{0x9e},
                                              std::byte{0}, // A3
                                              std::byte{0xa6},
                                              std::byte{0}, // A4
                                              std::byte{0xae},
                                              std::byte{0}, // A5
                                              std::byte{0xb6},
                                              std::byte{0}, // A6
                                              std::byte{0xbe},
                                              std::byte{0}, // A7
                                              std::byte{0x0}};

    std::array<std::uint16_t, 8> expected_ressult{2500, 2500, 2500, 2500, 2500, 2500, 2500, 2500};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this, expected_buffer](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(expected_buffer.size()));
            for (std::size_t i = 0; i < expected_buffer.size(); i++) {
                EXPECT_THAT(buffer[0][i], Eq(expected_buffer[i]));
            }

            buffer[0][1] = std::byte{0x0f};
            buffer[0][2] = std::byte{0xf0};
            buffer[0][3] = std::byte{0x0f};
            buffer[0][4] = std::byte{0xf0};
            buffer[0][5] = std::byte{0x0f};
            buffer[0][6] = std::byte{0xf0};
            buffer[0][7] = std::byte{0x0f};
            buffer[0][8] = std::byte{0xf0};
            buffer[0][9] = std::byte{0x0f};
            buffer[0][10] = std::byte{0xf0};
            buffer[0][11] = std::byte{0x0f};
            buffer[0][12] = std::byte{0xf0};
            buffer[0][13] = std::byte{0x0f};
            buffer[0][14] = std::byte{0xf0};
            buffer[0][15] = std::byte{0x0f};
            buffer[0][16] = std::byte{0xf0};

            EXPECT_THAT(timeout, Eq(1_s));

            return spl::drivers::spi::status::ok;
        }));

    auto result = adc.read_all();
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), ContainerEq(expected_ressult));
}

TEST_F(AdcTest, Adc8ConversionZero) {
    adc8_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0x86)));

            EXPECT_THAT(timeout, Eq(1_s));

            buffer[0][1] = std::byte{0x00};
            buffer[0][2] = std::byte{0x00};

            return spl::drivers::spi::status::ok;
        }));

    auto result = adc.read_channel(devices::adc::channel::channel0);
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(0));
}

TEST_F(AdcTest, Adc8ConversionStep) {
    adc8_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0x86)));

            EXPECT_THAT(timeout, Eq(1_s));

            buffer[0][1] = std::byte{0x00};
            buffer[0][2] = std::byte{0x10};

            return spl::drivers::spi::status::ok;
        }));

    auto result = adc.read_channel(devices::adc::channel::channel0);
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(9));
}

TEST_F(AdcTest, Adc8ConversionMaximumMinusStep) {
    adc8_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0x86)));

            EXPECT_THAT(timeout, Eq(1_s));

            buffer[0][1] = std::byte{0x0f};
            buffer[0][2] = std::byte{0xe0};

            return spl::drivers::spi::status::ok;
        }));

    auto result = adc.read_channel(devices::adc::channel::channel0);
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(2490));
}

TEST_F(AdcTest, Adc8ConversionMaximum) {
    adc8_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0x86)));

            EXPECT_THAT(timeout, Eq(1_s));

            buffer[0][1] = std::byte{0x0f};
            buffer[0][2] = std::byte{0xf0};

            return spl::drivers::spi::status::ok;
        }));

    auto result = adc.read_channel(devices::adc::channel::channel0);
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(2500));
}

TEST_F(AdcTest, Adc12ConversionZero) {
    adc12_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0x86)));

            EXPECT_THAT(timeout, Eq(1_s));

            buffer[0][1] = std::byte{0x00};
            buffer[0][2] = std::byte{0x00};

            return spl::drivers::spi::status::ok;
        }));

    auto result = adc.read_channel(devices::adc::channel::channel0);
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(0));
}

TEST_F(AdcTest, Adc12ConversionStep) {
    adc12_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0x86)));

            EXPECT_THAT(timeout, Eq(1_s));

            buffer[0][1] = std::byte{0x00};
            buffer[0][2] = std::byte{0x01};

            return spl::drivers::spi::status::ok;
        }));

    auto result = adc.read_channel(devices::adc::channel::channel0);
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(0));
}

TEST_F(AdcTest, Adc12Conversion2Steps) {
    adc12_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0x86)));

            EXPECT_THAT(timeout, Eq(1_s));

            buffer[0][1] = std::byte{0x00};
            buffer[0][2] = std::byte{0x02};

            return spl::drivers::spi::status::ok;
        }));

    auto result = adc.read_channel(devices::adc::channel::channel0);
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(1));
}

TEST_F(AdcTest, Adc12ConversionMaximumMinusStep) {
    adc12_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0x86)));

            EXPECT_THAT(timeout, Eq(1_s));

            buffer[0][1] = std::byte{0x0f};
            buffer[0][2] = std::byte{0xfe};

            return spl::drivers::spi::status::ok;
        }));

    auto result = adc.read_channel(devices::adc::channel::channel0);
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(2499));
}

TEST_F(AdcTest, Adc12ConversionMaximum) {
    adc12_type adc{spi_, gpio_, 1_s};

    EXPECT_CALL(spi_, transfer_mock(_, _, _))
        .WillOnce(Invoke([this](auto& cs, auto buffer, auto timeout) {
            EXPECT_THAT(&gpio_, Eq(&cs));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte(0x86)));

            EXPECT_THAT(timeout, Eq(1_s));

            buffer[0][1] = std::byte{0x0f};
            buffer[0][2] = std::byte{0xff};

            return spl::drivers::spi::status::ok;
        }));

    auto result = adc.read_channel(devices::adc::channel::channel0);
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(2500));
}

} // namespace
