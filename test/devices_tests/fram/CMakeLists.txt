set(TARGET fram_test)

add_executable(${TARGET})

target_sources(${TARGET} PRIVATE
    src/fram_test.cpp
    )

target_include_directories(${TARGET} PUBLIC
    include
    )

target_link_libraries(${TARGET}
    PRIVATE
    spl::spi_mock
    spl::gpio_ll_mock
    fram
    )

add_unit_test(${TARGET})
