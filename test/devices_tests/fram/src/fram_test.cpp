#include <algorithm>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/fram/fram.h"
#include "devices/fram/fram_concept.h"
#include "spl/drivers/spi/mock/spi.h"
#include "spl/peripherals/gpio/mock/gpio.h"

namespace {

using namespace ::testing;
using namespace devices::fram;

using fram_type = fram<spl::peripherals::gpio::mock::gpio,
                       spl::drivers::spi::mock::spi_vectored,
                       memory_id::cy15b104q>;

static_assert(devices::fram::fram_concept<fram_type>);

struct FramTest : Test {
    NiceMock<spl::drivers::spi::mock::spi_vectored> spi_;
    NiceMock<spl::peripherals::gpio::mock::gpio> chip_select_;
};

TEST_F(FramTest, ProbeSuccess) {
    fram_type fram{spi_, chip_select_};

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto& cs, auto in, auto out, auto) {
            EXPECT_THAT(&chip_select_, Eq(&cs));
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));
            EXPECT_THAT(in[0].size(), Eq(1));
            EXPECT_THAT(in[0][0], Eq(std::byte(opcode::read_id)));
            EXPECT_THAT(out[0].size(), Eq(9));

            std::copy(memory_id::cy15b104q.begin(), memory_id::cy15b104q.end(), out[0].begin());

            return spl::drivers::spi::status::ok;
        }));

    auto result = fram.probe();

    ASSERT_THAT(result, Eq(true));
}

TEST_F(FramTest, ProbeWrongID) {
    fram_type fram{spi_, chip_select_};

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto& cs, auto in, auto out, auto) {
            EXPECT_THAT(&chip_select_, Eq(&cs));
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));
            EXPECT_THAT(in[0].size(), Eq(1));
            EXPECT_THAT(in[0][0], Eq(std::byte(opcode::read_id)));
            EXPECT_THAT(out[0].size(), Eq(9));

            return spl::drivers::spi::status::ok;
        }));

    auto result = fram.probe();

    ASSERT_THAT(result, Eq(false));
}

TEST_F(FramTest, ProbeSPIError) {
    fram_type fram{spi_, chip_select_};

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto& cs, auto in, auto out, auto) {
            EXPECT_THAT(&chip_select_, Eq(&cs));
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));
            EXPECT_THAT(in[0].size(), Eq(1));
            EXPECT_THAT(in[0][0], Eq(std::byte(opcode::read_id)));
            EXPECT_THAT(out[0].size(), Eq(9));

            return spl::drivers::spi::status::transfer_error;
        }));

    auto result = fram.probe();

    ASSERT_THAT(result, Eq(false));
}

TEST_F(FramTest, WriteEnableSuccess) {
    fram_type fram{spi_, chip_select_};

    EXPECT_CALL(spi_, write_mock(_, _, _)).WillOnce(Invoke([this](auto& cs, auto in, auto) {
        EXPECT_THAT(&chip_select_, Eq(&cs));
        EXPECT_THAT(in.size(), Eq(1));
        EXPECT_THAT(in[0].size(), Eq(1));
        EXPECT_THAT(in[0][0], Eq(std::byte(opcode::write_enable)));

        return spl::drivers::spi::status::ok;
    }));

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto& cs, auto in, auto out, auto) {
            EXPECT_THAT(&chip_select_, Eq(&cs));
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));
            EXPECT_THAT(in[0].size(), Eq(1));
            EXPECT_THAT(in[0][0], Eq(std::byte(opcode::read_status)));
            EXPECT_THAT(out[0].size(), Eq(1));

            out[0][0] = std::byte{0x42};

            return spl::drivers::spi::status::ok;
        }));

    auto result = fram.write_enable();

    ASSERT_THAT(result, Eq(true));
}

TEST_F(FramTest, WriteEnableTransferError) {
    fram_type fram{spi_, chip_select_};

    EXPECT_CALL(spi_, write_mock(_, _, _)).WillOnce(Invoke([this](auto& cs, auto in, auto) {
        EXPECT_THAT(&chip_select_, Eq(&cs));
        EXPECT_THAT(in.size(), Eq(1));
        EXPECT_THAT(in[0].size(), Eq(1));
        EXPECT_THAT(in[0][0], Eq(std::byte(opcode::write_enable)));

        return spl::drivers::spi::status::transfer_error;
    }));

    auto result = fram.write_enable();

    ASSERT_THAT(result, Eq(false));
}

TEST_F(FramTest, WriteEnableVerifyError) {
    fram_type fram{spi_, chip_select_};

    EXPECT_CALL(spi_, write_mock(_, _, _)).WillOnce(Invoke([this](auto& cs, auto in, auto) {
        EXPECT_THAT(&chip_select_, Eq(&cs));
        EXPECT_THAT(in.size(), Eq(1));
        EXPECT_THAT(in[0].size(), Eq(1));
        EXPECT_THAT(in[0][0], Eq(std::byte(opcode::write_enable)));

        return spl::drivers::spi::status::ok;
    }));

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto& cs, auto in, auto out, auto) {
            EXPECT_THAT(&chip_select_, Eq(&cs));
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));
            EXPECT_THAT(in[0].size(), Eq(1));
            EXPECT_THAT(in[0][0], Eq(std::byte(opcode::read_status)));
            EXPECT_THAT(out[0].size(), Eq(1));

            out[0][0] = std::byte{0x40};

            return spl::drivers::spi::status::ok;
        }));

    auto result = fram.write_enable();

    ASSERT_THAT(result, Eq(false));
}

TEST_F(FramTest, WriteEnableVerifyTransferError) {
    fram_type fram{spi_, chip_select_};

    EXPECT_CALL(spi_, write_mock(_, _, _)).WillOnce(Invoke([this](auto& cs, auto in, auto) {
        EXPECT_THAT(&chip_select_, Eq(&cs));
        EXPECT_THAT(in.size(), Eq(1));
        EXPECT_THAT(in[0].size(), Eq(1));
        EXPECT_THAT(in[0][0], Eq(std::byte(opcode::write_enable)));

        return spl::drivers::spi::status::ok;
    }));

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto& cs, auto in, auto out, auto) {
            EXPECT_THAT(&chip_select_, Eq(&cs));
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));
            EXPECT_THAT(in[0].size(), Eq(1));
            EXPECT_THAT(in[0][0], Eq(std::byte(opcode::read_status)));
            EXPECT_THAT(out[0].size(), Eq(1));

            out[0][0] = std::byte{0x42};

            return spl::drivers::spi::status::transfer_error;
        }));

    auto result = fram.write_enable();

    ASSERT_THAT(result, Eq(false));
}

TEST_F(FramTest, WriteDisableSuccess) {
    fram_type fram{spi_, chip_select_};

    EXPECT_CALL(spi_, write_mock(_, _, _)).WillOnce(Invoke([this](auto& cs, auto in, auto) {
        EXPECT_THAT(&chip_select_, Eq(&cs));
        EXPECT_THAT(in.size(), Eq(1));
        EXPECT_THAT(in[0].size(), Eq(1));
        EXPECT_THAT(in[0][0], Eq(std::byte(opcode::write_disable)));

        return spl::drivers::spi::status::ok;
    }));

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto& cs, auto in, auto out, auto) {
            EXPECT_THAT(&chip_select_, Eq(&cs));
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));
            EXPECT_THAT(in[0].size(), Eq(1));
            EXPECT_THAT(in[0][0], Eq(std::byte(opcode::read_status)));
            EXPECT_THAT(out[0].size(), Eq(1));

            out[0][0] = std::byte{0x40};

            return spl::drivers::spi::status::ok;
        }));

    auto result = fram.write_disable();

    ASSERT_THAT(result, Eq(true));
}

TEST_F(FramTest, WriteDisableTransferError) {
    fram_type fram{spi_, chip_select_};

    EXPECT_CALL(spi_, write_mock(_, _, _)).WillOnce(Invoke([this](auto& cs, auto in, auto) {
        EXPECT_THAT(&chip_select_, Eq(&cs));
        EXPECT_THAT(in.size(), Eq(1));
        EXPECT_THAT(in[0].size(), Eq(1));
        EXPECT_THAT(in[0][0], Eq(std::byte(opcode::write_disable)));

        return spl::drivers::spi::status::transfer_error;
    }));

    auto result = fram.write_disable();

    ASSERT_THAT(result, Eq(false));
}

TEST_F(FramTest, WriteDisableVerifyError) {
    fram_type fram{spi_, chip_select_};

    EXPECT_CALL(spi_, write_mock(_, _, _)).WillOnce(Invoke([this](auto& cs, auto in, auto) {
        EXPECT_THAT(&chip_select_, Eq(&cs));
        EXPECT_THAT(in.size(), Eq(1));
        EXPECT_THAT(in[0].size(), Eq(1));
        EXPECT_THAT(in[0][0], Eq(std::byte(opcode::write_disable)));

        return spl::drivers::spi::status::ok;
    }));

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto& cs, auto in, auto out, auto) {
            EXPECT_THAT(&chip_select_, Eq(&cs));
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));
            EXPECT_THAT(in[0].size(), Eq(1));
            EXPECT_THAT(in[0][0], Eq(std::byte(opcode::read_status)));
            EXPECT_THAT(out[0].size(), Eq(1));

            out[0][0] = std::byte{0x42};

            return spl::drivers::spi::status::ok;
        }));

    auto result = fram.write_disable();

    ASSERT_THAT(result, Eq(false));
}

TEST_F(FramTest, WriteDisableVerifyTransferError) {
    fram_type fram{spi_, chip_select_};

    EXPECT_CALL(spi_, write_mock(_, _, _)).WillOnce(Invoke([this](auto& cs, auto in, auto) {
        EXPECT_THAT(&chip_select_, Eq(&cs));
        EXPECT_THAT(in.size(), Eq(1));
        EXPECT_THAT(in[0].size(), Eq(1));
        EXPECT_THAT(in[0][0], Eq(std::byte(opcode::write_disable)));

        return spl::drivers::spi::status::ok;
    }));

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto& cs, auto in, auto out, auto) {
            EXPECT_THAT(&chip_select_, Eq(&cs));
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));
            EXPECT_THAT(in[0].size(), Eq(1));
            EXPECT_THAT(in[0][0], Eq(std::byte(opcode::read_status)));
            EXPECT_THAT(out[0].size(), Eq(1));

            out[0][0] = std::byte{0x40};

            return spl::drivers::spi::status::transfer_error;
        }));

    auto result = fram.write_disable();

    ASSERT_THAT(result, Eq(false));
}

TEST_F(FramTest, ReadSuccess) {
    fram_type fram{spi_, chip_select_};

    std::array<std::byte, 8> buffer;

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto& cs, auto in, auto out, auto) {
            EXPECT_THAT(&chip_select_, Eq(&cs));
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));
            EXPECT_THAT(in[0].size(), Eq(4));
            EXPECT_THAT(in[0][0], Eq(std::byte(opcode::read)));
            EXPECT_THAT(in[0][1], Eq(std::byte{}));
            EXPECT_THAT(in[0][2], Eq(std::byte{0x12}));
            EXPECT_THAT(in[0][3], Eq(std::byte{0x34}));
            EXPECT_THAT(out[0].size(), Eq(8));

            for (unsigned int i = 0; i < out[0].size(); i++) {
                out[0][i] = std::byte(i);
            }

            return spl::drivers::spi::status::ok;
        }));

    auto result = fram.read(0x1234, buffer);

    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result->size(), Eq(buffer.size()));

    for (unsigned int i = 0; i < result->size(); i++) {
        ASSERT_THAT(result->data()[i], Eq(std::byte(i)));
    }
}

TEST_F(FramTest, ReadFailure) {
    fram_type fram{spi_, chip_select_};

    std::array<std::byte, 8> buffer;

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto& cs, auto in, auto out, auto) {
            EXPECT_THAT(&chip_select_, Eq(&cs));
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));
            EXPECT_THAT(in[0].size(), Eq(4));
            EXPECT_THAT(in[0][0], Eq(std::byte(opcode::read)));
            EXPECT_THAT(in[0][1], Eq(std::byte{}));
            EXPECT_THAT(in[0][2], Eq(std::byte{0x12}));
            EXPECT_THAT(in[0][3], Eq(std::byte{0x34}));
            EXPECT_THAT(out[0].size(), Eq(8));

            return spl::drivers::spi::status::transfer_error;
        }));

    auto result = fram.read(0x1234, buffer);

    ASSERT_THAT(result.has_value(), Eq(false));
}

TEST_F(FramTest, WriteSuccess) {
    fram_type fram{spi_, chip_select_};

    std::array<std::byte, 8> buffer;
    for (unsigned int i = 0; i < buffer.size(); i++) {
        buffer[i] = std::byte(i);
    }

    EXPECT_CALL(spi_, write_mock(_, _, _)).WillOnce(Invoke([this](auto& cs, auto in, auto) {
        EXPECT_THAT(&chip_select_, Eq(&cs));
        EXPECT_THAT(in.size(), Eq(2));
        EXPECT_THAT(in[0].size(), Eq(4));
        EXPECT_THAT(in[0][0], Eq(std::byte(opcode::write)));
        EXPECT_THAT(in[0][1], Eq(std::byte{}));
        EXPECT_THAT(in[0][2], Eq(std::byte{0x12}));
        EXPECT_THAT(in[0][3], Eq(std::byte{0x34}));
        EXPECT_THAT(in[1].size(), Eq(8));

        for (unsigned int i = 0; i < in[1].size(); i++) {
            EXPECT_THAT(std::byte(i), Eq(in[1][i]));
        }

        return spl::drivers::spi::status::ok;
    }));

    auto result = fram.write(0x1234, buffer);

    ASSERT_THAT(result, Eq(true));
}

TEST_F(FramTest, WriteFailure) {
    fram_type fram{spi_, chip_select_};

    std::array<std::byte, 8> buffer;
    for (unsigned int i = 0; i < buffer.size(); i++) {
        buffer[i] = std::byte(i);
    }

    EXPECT_CALL(spi_, write_mock(_, _, _)).WillOnce(Invoke([this](auto& cs, auto in, auto) {
        EXPECT_THAT(&chip_select_, Eq(&cs));
        EXPECT_THAT(in.size(), Eq(2));
        EXPECT_THAT(in[0].size(), Eq(4));
        EXPECT_THAT(in[0][0], Eq(std::byte(opcode::write)));
        EXPECT_THAT(in[0][1], Eq(std::byte{}));
        EXPECT_THAT(in[0][2], Eq(std::byte{0x12}));
        EXPECT_THAT(in[0][3], Eq(std::byte{0x34}));
        EXPECT_THAT(in[1].size(), Eq(8));

        for (unsigned int i = 0; i < in[1].size(); i++) {
            EXPECT_THAT(std::byte(i), Eq(in[1][i]));
        }

        return spl::drivers::spi::status::transfer_error;
    }));

    auto result = fram.write(0x1234, buffer);

    ASSERT_THAT(result, Eq(false));
}

TEST_F(FramTest, ReadStatusSuccess) {
    fram_type fram{spi_, chip_select_};

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto& cs, auto in, auto out, auto) {
            EXPECT_THAT(&chip_select_, Eq(&cs));
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));
            EXPECT_THAT(in[0].size(), Eq(1));
            EXPECT_THAT(in[0][0], Eq(std::byte(opcode::read_status)));
            EXPECT_THAT(out[0].size(), Eq(1));

            out[0][0] = std::byte{0x42};

            return spl::drivers::spi::status::ok;
        }));

    auto result = fram.read_status();

    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result->write_enabled(), Eq(true));
}

TEST_F(FramTest, ReadStatusFailure) {
    fram_type fram{spi_, chip_select_};

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto& cs, auto in, auto out, auto) {
            EXPECT_THAT(&chip_select_, Eq(&cs));
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));
            EXPECT_THAT(in[0].size(), Eq(1));
            EXPECT_THAT(in[0][0], Eq(std::byte(opcode::read_status)));
            EXPECT_THAT(out[0].size(), Eq(1));

            out[0][0] = std::byte{0x40};

            return spl::drivers::spi::status::transfer_error;
        }));

    auto result = fram.read_status();

    ASSERT_THAT(result.has_value(), Eq(false));
}
} // namespace
