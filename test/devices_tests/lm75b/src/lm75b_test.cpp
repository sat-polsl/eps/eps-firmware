#include <algorithm>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/lm75b/lm75b.h"
#include "devices/lm75b/lm75b_concept.h"
#include "satos/chrono.h"
#include "spl/drivers/i2c/mock/i2c.h"

namespace {

using namespace ::testing;
using namespace devices::lm75b;
using namespace satos::chrono_literals;

using lm75b_type = lm75b<spl::drivers::i2c::mock::i2c>;

static_assert(devices::lm75b::lm75b_concept<lm75b_type>);

struct Lm75bTest : Test {
    NiceMock<spl::drivers::i2c::mock::i2c> i2c_;
    std::uint8_t i2c_address_ = 0xaa;
};

TEST_F(Lm75bTest, SetOperationModeNormalSuccess) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(out.size(), Eq(1));
            out[0] = std::byte{0xff};
            return out;
        }));

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(2));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(in[1], Eq(std::byte{0xfe}));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_operation_mode(device_mode::normal);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOperationModeShutdownSuccess) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(out.size(), Eq(1));
            out[0] = std::byte{};
            return out;
        }));

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(2));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(in[1], Eq(std::byte{0x1}));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_operation_mode(device_mode::shutdown);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOperationModeWriteReadFailure) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(out.size(), Eq(1));
            return satext::unexpected{spl::drivers::i2c::status::timeout};
        }));

    auto result = lm75b.set_operation_mode(device_mode::shutdown);
    ASSERT_THAT(result, Eq(false));
}

TEST_F(Lm75bTest, SetOperationModeWriteFailure) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(out.size(), Eq(1));
            out[0] = std::byte{};
            return out;
        }));

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(2));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(in[1], Eq(std::byte{0x1}));
            return spl::drivers::i2c::status::timeout;
        }));

    auto result = lm75b.set_operation_mode(device_mode::shutdown);
    ASSERT_THAT(result, Eq(false));
}

TEST_F(Lm75bTest, SetOsModeComparatorSuccess) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(out.size(), Eq(1));
            out[0] = std::byte{0xff};
            return out;
        }));

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(2));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(in[1], Eq(std::byte{0xfd}));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_mode(os_mode::comparator);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsModeInterruptSuccess) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(out.size(), Eq(1));
            out[0] = std::byte{0x0};
            return out;
        }));

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(2));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(in[1], Eq(std::byte{0x2}));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_mode(os_mode::interrupt);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsModeFailure) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(out.size(), Eq(1));
            return out;
        }));

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(2));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(in[1], Eq(std::byte{0x2}));
            return spl::drivers::i2c::status::timeout;
        }));

    auto result = lm75b.set_os_mode(os_mode::interrupt);
    ASSERT_THAT(result, Eq(false));
}

TEST_F(Lm75bTest, SetOsPolarityActiveLowSuccess) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(out.size(), Eq(1));
            out[0] = std::byte{0xff};
            return out;
        }));

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(2));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(in[1], Eq(std::byte{0xfb}));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_polarity(os_polarity::active_low);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsPolarityActiveHighSuccess) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(out.size(), Eq(1));
            out[0] = std::byte{0x0};
            return out;
        }));

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(2));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(in[1], Eq(std::byte{0x4}));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_polarity(os_polarity::active_high);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsPolarityActiveHighFailure) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(out.size(), Eq(1));
            out[0] = std::byte{0x0};
            return out;
        }));

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(2));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::configuration)));
            EXPECT_THAT(in[1], Eq(std::byte{0x4}));
            return spl::drivers::i2c::status::timeout;
        }));

    auto result = lm75b.set_os_polarity(os_polarity::active_high);
    ASSERT_THAT(result, Eq(false));
}

TEST_F(Lm75bTest, ReadRawTemperatureSuccess) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::temperature)));
            EXPECT_THAT(out.size(), Eq(2));
            out[0] = std::byte{0xde};
            out[1] = std::byte{0xad};
            return out;
        }));

    auto result = lm75b.read_temperature_raw();
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(static_cast<std::int16_t>(0xdead)));
}

TEST_F(Lm75bTest, ReadRawTemperatureFailure) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::temperature)));
            EXPECT_THAT(out.size(), Eq(2));
            return satext::unexpected{spl::drivers::i2c::status::timeout};
        }));

    auto result = lm75b.read_temperature_raw();
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::i2c::status::timeout));
}

TEST_F(Lm75bTest, ReadTemperatureHighest) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::temperature)));
            EXPECT_THAT(out.size(), Eq(2));
            out[0] = std::byte{0x7f};
            out[1] = std::byte{0x00};
            return out;
        }));

    auto result = lm75b.read_temperature();
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(static_cast<std::int16_t>(1270)));
}

TEST_F(Lm75bTest, ReadTemperaturePositive) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::temperature)));
            EXPECT_THAT(out.size(), Eq(2));
            out[0] = std::byte{0x19};
            out[1] = std::byte{0x00};
            return out;
        }));

    auto result = lm75b.read_temperature();
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(static_cast<std::int16_t>(250)));
}

TEST_F(Lm75bTest, ReadTemperaturePositiveStep) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::temperature)));
            EXPECT_THAT(out.size(), Eq(2));
            out[0] = std::byte{0x00};
            out[1] = std::byte{0x20};
            return out;
        }));

    auto result = lm75b.read_temperature();
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(static_cast<std::int16_t>(1)));
}

TEST_F(Lm75bTest, ReadTemperatureZero) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::temperature)));
            EXPECT_THAT(out.size(), Eq(2));
            out[0] = std::byte{0x00};
            out[1] = std::byte{0x00};
            return out;
        }));

    auto result = lm75b.read_temperature();
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(static_cast<std::int16_t>(0)));
}

TEST_F(Lm75bTest, ReadTemperatureNegativeStep) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::temperature)));
            EXPECT_THAT(out.size(), Eq(2));
            out[0] = std::byte{0xff};
            out[1] = std::byte{0xe0};
            return out;
        }));

    auto result = lm75b.read_temperature();
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(static_cast<std::int16_t>(-1)));
}

TEST_F(Lm75bTest, ReadTemperatureNegative) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::temperature)));
            EXPECT_THAT(out.size(), Eq(2));
            out[0] = std::byte{0xe7};
            out[1] = std::byte{0x00};
            return out;
        }));

    auto result = lm75b.read_temperature();
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(static_cast<std::int16_t>(-250)));
}

TEST_F(Lm75bTest, ReadTemperatureLowest) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::temperature)));
            EXPECT_THAT(out.size(), Eq(2));
            out[0] = std::byte{0xc9};
            out[1] = std::byte{0x00};
            return out;
        }));

    auto result = lm75b.read_temperature();
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(static_cast<std::int16_t>(-550)));
}

TEST_F(Lm75bTest, ReadTemperatureFailure) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_read_mock(i2c_address_, _, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto out, auto) {
            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::temperature)));
            EXPECT_THAT(out.size(), Eq(2));
            return satext::unexpected{spl::drivers::i2c::status::timeout};
        }));

    auto result = lm75b.read_temperature();
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::i2c::status::timeout));
}

TEST_F(Lm75bTest, SetOsThresholdHighest) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::os)));
            EXPECT_THAT(in[1], Eq(std::byte(0x7d)));
            EXPECT_THAT(in[2], Eq(std::byte(0x00)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_threshold(1250);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsThresholdPositve) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::os)));
            EXPECT_THAT(in[1], Eq(std::byte(0x19)));
            EXPECT_THAT(in[2], Eq(std::byte(0x00)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_threshold(250);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsThresholdPositveStep) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::os)));
            EXPECT_THAT(in[1], Eq(std::byte(0x00)));
            EXPECT_THAT(in[2], Eq(std::byte(0x80)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_threshold(5);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsThresholdZero) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::os)));
            EXPECT_THAT(in[1], Eq(std::byte(0x00)));
            EXPECT_THAT(in[2], Eq(std::byte(0x00)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_threshold(0);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsThresholdNegativeStep) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::os)));
            EXPECT_THAT(in[1], Eq(std::byte(0xff)));
            EXPECT_THAT(in[2], Eq(std::byte(0x80)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_threshold(-5);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsThresholdNegative) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::os)));
            EXPECT_THAT(in[1], Eq(std::byte(0xe7)));
            EXPECT_THAT(in[2], Eq(std::byte(0x00)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_threshold(-250);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsThresholdLowest) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::os)));
            EXPECT_THAT(in[1], Eq(std::byte(0xc9)));
            EXPECT_THAT(in[2], Eq(std::byte(0x00)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_threshold(-550);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsThresholdFailure) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::os)));
            EXPECT_THAT(in[1], Eq(std::byte(0xc9)));
            EXPECT_THAT(in[2], Eq(std::byte(0x00)));
            return spl::drivers::i2c::status::timeout;
        }));

    auto result = lm75b.set_os_threshold(-550);
    ASSERT_THAT(result, Eq(false));
}

TEST_F(Lm75bTest, SetOsHysteresisHighest) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::hysteresis)));
            EXPECT_THAT(in[1], Eq(std::byte(0x7d)));
            EXPECT_THAT(in[2], Eq(std::byte(0x00)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_hysteresis(1250);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsHysteresisPositve) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::hysteresis)));
            EXPECT_THAT(in[1], Eq(std::byte(0x19)));
            EXPECT_THAT(in[2], Eq(std::byte(0x00)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_hysteresis(250);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsHysteresisPositveStep) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::hysteresis)));
            EXPECT_THAT(in[1], Eq(std::byte(0x00)));
            EXPECT_THAT(in[2], Eq(std::byte(0x80)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_hysteresis(5);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsHysteresisZero) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::hysteresis)));
            EXPECT_THAT(in[1], Eq(std::byte(0x00)));
            EXPECT_THAT(in[2], Eq(std::byte(0x00)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_hysteresis(0);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsHysteresisNegativeStep) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::hysteresis)));
            EXPECT_THAT(in[1], Eq(std::byte(0xff)));
            EXPECT_THAT(in[2], Eq(std::byte(0x80)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_hysteresis(-5);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsHysteresisNegative) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::hysteresis)));
            EXPECT_THAT(in[1], Eq(std::byte(0xe7)));
            EXPECT_THAT(in[2], Eq(std::byte(0x00)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_hysteresis(-250);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsHysteresisLowest) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::hysteresis)));
            EXPECT_THAT(in[1], Eq(std::byte(0xc9)));
            EXPECT_THAT(in[2], Eq(std::byte(0x00)));
            return spl::drivers::i2c::status::ok;
        }));

    auto result = lm75b.set_os_hysteresis(-550);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(Lm75bTest, SetOsHysteresisFailure) {
    lm75b_type lm75b{i2c_, i2c_address_, 1_s};

    EXPECT_CALL(i2c_, write_mock(i2c_address_, _, 1000_ms))
        .WillOnce(Invoke([](auto, auto in, auto) {
            EXPECT_THAT(in.size(), Eq(3));
            EXPECT_THAT(in[0], Eq(std::byte(pointer_register::hysteresis)));
            EXPECT_THAT(in[1], Eq(std::byte(0xc9)));
            EXPECT_THAT(in[2], Eq(std::byte(0x00)));
            return spl::drivers::i2c::status::timeout;
        }));

    auto result = lm75b.set_os_hysteresis(-550);
    ASSERT_THAT(result, Eq(false));
}

} // namespace
