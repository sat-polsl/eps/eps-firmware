#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/mcp73871/mcp73871.h"
#include "devices/mcp73871/mcp73871_concept.h"
#include "spl/peripherals/gpio/mock/gpio.h"

namespace {

using namespace ::testing;
using namespace devices::mcp73871;

using mcp_type = mcp73871<spl::peripherals::gpio::mock::gpio>;

static_assert(mcp73871_concept<mcp_type>);

struct McpTest : Test {
    StrictMock<spl::peripherals::gpio::mock::gpio> charge_enable_;
    StrictMock<spl::peripherals::gpio::mock::gpio> power_good_;
    StrictMock<spl::peripherals::gpio::mock::gpio> stat1_;
    StrictMock<spl::peripherals::gpio::mock::gpio> stat2_;
    configuration<spl::peripherals::gpio::mock::gpio> cfg_{.charge_enable = charge_enable_,
                                                           .power_good = power_good_,
                                                           .stat1 = stat1_,
                                                           .stat2 = stat2_};
};

TEST_F(McpTest, Enable) {
    mcp_type mcp{cfg_};

    EXPECT_CALL(charge_enable_, set());

    mcp.charge_enable();
}

TEST_F(McpTest, Disable) {
    mcp_type mcp{cfg_};

    EXPECT_CALL(charge_enable_, clear());

    mcp.charge_disable();
}

TEST_F(McpTest, PowerGood) {
    mcp_type mcp{cfg_};

    EXPECT_CALL(power_good_, read()).WillOnce(Return(true));

    ASSERT_THAT(mcp.power_good(), Eq(true));
}

TEST_F(McpTest, PowerBad) {
    mcp_type mcp{cfg_};

    EXPECT_CALL(power_good_, read()).WillOnce(Return(false));

    ASSERT_THAT(mcp.power_good(), Eq(false));
}

TEST_F(McpTest, Stat1High) {
    mcp_type mcp{cfg_};

    EXPECT_CALL(stat1_, read()).WillOnce(Return(true));

    ASSERT_THAT(mcp.stat1(), Eq(true));
}

TEST_F(McpTest, Stat1Low) {
    mcp_type mcp{cfg_};

    EXPECT_CALL(stat1_, read()).WillOnce(Return(false));

    ASSERT_THAT(mcp.stat1(), Eq(false));
}

TEST_F(McpTest, Stat2High) {
    mcp_type mcp{cfg_};

    EXPECT_CALL(stat2_, read()).WillOnce(Return(true));

    ASSERT_THAT(mcp.stat2(), Eq(true));
}

TEST_F(McpTest, Stat2Low) {
    mcp_type mcp{cfg_};

    EXPECT_CALL(stat2_, read()).WillOnce(Return(false));

    ASSERT_THAT(mcp.stat2(), Eq(false));
}

} // namespace
