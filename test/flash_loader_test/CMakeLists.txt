set(TARGET flash_loader_test)

add_executable(${TARGET})

target_sources(${TARGET} PRIVATE
    src/flash_loader_test.cpp
    )

target_include_directories(${TARGET} PUBLIC
    include
    )

target_link_libraries(${TARGET}
    PRIVATE
    flash_loader
    fram_mock
    spl::flash_ll_mock
    spl::crc_ll_mock
    satext
    )

add_unit_test(${TARGET})
