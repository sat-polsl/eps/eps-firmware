#include <array>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "bootloader/flash_loader/flash_loader.h"
#include "bootloader/utils/boot_slot.h"
#include "devices/fram/mock/fram.h"
#include "satext/struct.h"
#include "spl/peripherals/crc/mock/crc.h"
#include "spl/peripherals/flash/mock/flash.h"

namespace {

using namespace ::testing;
using namespace satext::struct_literals;

using flash_loader_type =
    bootloader::flash_loader::flash_loader<spl::peripherals::flash::mock::flash,
                                           ::devices::fram::mock::fram,
                                           spl::peripherals::crc::mock::crc,
                                           0x1000>;

struct FlashLoaderTest : Test {
    FlashLoaderTest();
    NiceMock<::devices::fram::mock::fram> fram_;
    NiceMock<spl::peripherals::flash::mock::flash> flash_;
    NiceMock<spl::peripherals::crc::mock::crc> crc_;
    static inline bootloader::utils::boot_slot slot_ = {
        .address = 0x1000, .magic_a = 0xdeadc0de, .magic_b = 0xdeadbeef};
};

FlashLoaderTest::FlashLoaderTest() {
    spl::peripherals::flash::mock::setup_default_return_value(flash_);
    spl::peripherals::crc::mock::setup_default_return_value(crc_);
}

TEST_F(FlashLoaderTest, BadMagic) {
    flash_loader_type flash{flash_, fram_, crc_};

    InSequence s;
    EXPECT_CALL(fram_, read(0x1000, _)).WillOnce(Invoke([](auto, auto buffer) {
        EXPECT_THAT(buffer.size(), 16);
        return buffer;
    }));

    EXPECT_CALL(flash_, unlock()).Times(1);
    EXPECT_CALL(flash_, lock()).Times(1);

    auto result = flash.load_slot(slot_);
    ASSERT_THAT(result, Eq(bootloader::flash_loader::status::bad_magic));
}

TEST_F(FlashLoaderTest, BootHeaderReadError) {
    flash_loader_type flash{flash_, fram_, crc_};

    InSequence s;
    EXPECT_CALL(fram_, read(0x1000, _)).WillOnce(Invoke([](auto, auto buffer) {
        EXPECT_THAT(buffer.size(), 16);
        return satext::unexpected{std::monostate{}};
    }));

    auto result = flash.load_slot(slot_);
    ASSERT_THAT(result, Eq(bootloader::flash_loader::status::read_error));
}

TEST_F(FlashLoaderTest, LoadFlashSinglePageSuccess) {
    flash_loader_type flash{flash_, fram_, crc_};

    InSequence s;
    EXPECT_CALL(fram_, read(0x1000, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        satext::pack_to("<IIII"_fmt, buffer, 0xdeadc0de, 0xdeadbeef, 0xfeedc0de, 128);
        return buffer;
    }));

    EXPECT_CALL(flash_, unlock()).Times(1);

    EXPECT_CALL(fram_, read(0x1010, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        EXPECT_THAT(buffer.size(), Eq(128));
        return buffer;
    }));

    EXPECT_CALL(flash_, erase_page(32)).Times(1);
    EXPECT_CALL(flash_,
                program_buffer(spl::peripherals::flash::mock::flash::memory_base + 0x1000, _))
        .Times(1)
        .WillOnce(Invoke([this](auto, auto buffer) -> spl::peripherals::flash::mock::flash& {
            EXPECT_THAT(buffer.size(), Eq(128));
            return flash_;
        }));
    EXPECT_CALL(crc_, feed_span(SizeIs(128))).WillOnce(Return(0xfeedc0de));
    EXPECT_CALL(flash_, lock()).Times(1);

    auto result = flash.load_slot(slot_);
    ASSERT_THAT(result, Eq(bootloader::flash_loader::status::ok));
}

TEST_F(FlashLoaderTest, LoadFlashCrcMismatch) {
    flash_loader_type flash{flash_, fram_, crc_};

    InSequence s;
    EXPECT_CALL(fram_, read(0x1000, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        satext::pack_to("<IIII"_fmt, buffer, 0xdeadc0de, 0xdeadbeef, 0xfeedc0de, 128);
        return buffer;
    }));

    EXPECT_CALL(flash_, unlock()).Times(1);

    EXPECT_CALL(fram_, read(0x1010, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        EXPECT_THAT(buffer.size(), Eq(128));
        return buffer;
    }));

    EXPECT_CALL(flash_, erase_page(32)).Times(1);
    EXPECT_CALL(flash_,
                program_buffer(spl::peripherals::flash::mock::flash::memory_base + 0x1000, _))
        .Times(1)
        .WillOnce(Invoke([this](auto, auto buffer) -> spl::peripherals::flash::mock::flash& {
            EXPECT_THAT(buffer.size(), Eq(128));
            return flash_;
        }));
    EXPECT_CALL(crc_, feed_span(SizeIs(128))).WillOnce(Return(0x0));
    EXPECT_CALL(flash_, lock()).Times(1);

    auto result = flash.load_slot(slot_);
    ASSERT_THAT(result, Eq(bootloader::flash_loader::status::crc_error));
}

TEST_F(FlashLoaderTest, LoadFlashTriplePageSuccess) {
    flash_loader_type flash{flash_, fram_, crc_};

    InSequence s;
    EXPECT_CALL(fram_, read(0x1000, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        satext::pack_to("<IIII"_fmt, buffer, 0xdeadc0de, 0xdeadbeef, 0xfeedc0de, 3 * 128);
        return buffer;
    }));

    EXPECT_CALL(flash_, unlock()).Times(1);

    EXPECT_CALL(fram_, read(0x1010, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        EXPECT_THAT(buffer.size(), Eq(128));
        return buffer;
    }));

    EXPECT_CALL(flash_, erase_page(32)).Times(1);
    EXPECT_CALL(flash_,
                program_buffer(spl::peripherals::flash::mock::flash::memory_base + 0x1000, _))
        .Times(1)
        .WillOnce(Invoke([this](auto, auto buffer) -> spl::peripherals::flash::mock::flash& {
            EXPECT_THAT(buffer.size(), Eq(128));
            return flash_;
        }));

    EXPECT_CALL(fram_, read(0x1090, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        EXPECT_THAT(buffer.size(), Eq(128));
        return buffer;
    }));

    EXPECT_CALL(flash_, erase_page(33)).Times(1);
    EXPECT_CALL(flash_,
                program_buffer(spl::peripherals::flash::mock::flash::memory_base + 0x1080, _))
        .Times(1)
        .WillOnce(Invoke([this](auto, auto buffer) -> spl::peripherals::flash::mock::flash& {
            EXPECT_THAT(buffer.size(), Eq(128));
            return flash_;
        }));

    EXPECT_CALL(fram_, read(0x1110, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        EXPECT_THAT(buffer.size(), Eq(128));
        return buffer;
    }));

    EXPECT_CALL(flash_, erase_page(34)).Times(1);
    EXPECT_CALL(flash_,
                program_buffer(spl::peripherals::flash::mock::flash::memory_base + 0x1100, _))
        .Times(1)
        .WillOnce(Invoke([this](auto, auto buffer) -> spl::peripherals::flash::mock::flash& {
            EXPECT_THAT(buffer.size(), Eq(128));
            return flash_;
        }));
    EXPECT_CALL(crc_, feed_span(SizeIs(3 * 128))).WillOnce(Return(0xfeedc0de));
    EXPECT_CALL(flash_, lock()).Times(1);

    auto result = flash.load_slot(slot_);
    ASSERT_THAT(result, Eq(bootloader::flash_loader::status::ok));
}

TEST_F(FlashLoaderTest, LoadFlashOneAndHalfPageSuccess) {
    flash_loader_type flash{flash_, fram_, crc_};

    InSequence s;
    EXPECT_CALL(fram_, read(0x1000, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        satext::pack_to("<IIII"_fmt, buffer, 0xdeadc0de, 0xdeadbeef, 0xfeedc0de, 192);
        return buffer;
    }));

    EXPECT_CALL(flash_, unlock()).Times(1);

    EXPECT_CALL(fram_, read(0x1010, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        EXPECT_THAT(buffer.size(), Eq(128));
        return buffer;
    }));

    EXPECT_CALL(flash_, erase_page(32)).Times(1);
    EXPECT_CALL(flash_,
                program_buffer(spl::peripherals::flash::mock::flash::memory_base + 0x1000, _));

    EXPECT_CALL(fram_, read(0x1090, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        EXPECT_THAT(buffer.size(), Eq(64));
        return buffer;
    }));

    EXPECT_CALL(flash_, erase_page(33)).Times(1);
    EXPECT_CALL(flash_,
                program_buffer(spl::peripherals::flash::mock::flash::memory_base + 0x1080, _))
        .Times(1)
        .WillOnce(Invoke([this](auto, auto buffer) -> spl::peripherals::flash::mock::flash& {
            EXPECT_THAT(buffer.size(), Eq(64));
            return flash_;
        }));

    EXPECT_CALL(crc_, feed_span(SizeIs(192))).WillOnce(Return(0xfeedc0de));
    EXPECT_CALL(flash_, lock()).Times(1);

    auto result = flash.load_slot(slot_);
    ASSERT_THAT(result, Eq(bootloader::flash_loader::status::ok));
}

TEST_F(FlashLoaderTest, LoadFlashLengthNotDivisibleBy8Success) {
    flash_loader_type flash{flash_, fram_, crc_};

    InSequence s;
    EXPECT_CALL(fram_, read(0x1000, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        satext::pack_to("<IIII"_fmt, buffer, 0xdeadc0de, 0xdeadbeef, 0xfeedc0de, 189);
        return buffer;
    }));

    EXPECT_CALL(flash_, unlock()).Times(1);

    EXPECT_CALL(fram_, read(0x1010, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        EXPECT_THAT(buffer.size(), Eq(128));
        return buffer;
    }));

    EXPECT_CALL(flash_, erase_page(32)).Times(1);
    EXPECT_CALL(flash_,
                program_buffer(spl::peripherals::flash::mock::flash::memory_base + 0x1000, _));

    EXPECT_CALL(fram_, read(0x1090, _)).Times(1).WillOnce(Invoke([](auto, auto buffer) {
        EXPECT_THAT(buffer.size(), Eq(61));
        return buffer;
    }));

    EXPECT_CALL(flash_, erase_page(33)).Times(1);
    EXPECT_CALL(flash_,
                program_buffer(spl::peripherals::flash::mock::flash::memory_base + 0x1080, _))
        .Times(1)
        .WillOnce(Invoke([this](auto, auto buffer) -> spl::peripherals::flash::mock::flash& {
            EXPECT_THAT(buffer.size(), Eq(64));
            EXPECT_THAT(buffer[61], Eq(std::byte{0xff}));
            EXPECT_THAT(buffer[62], Eq(std::byte{0xff}));
            EXPECT_THAT(buffer[63], Eq(std::byte{0xff}));
            return flash_;
        }));

    EXPECT_CALL(crc_, feed_span(SizeIs(189))).WillOnce(Return(0xfeedc0de));
    EXPECT_CALL(flash_, lock()).Times(1);

    auto result = flash.load_slot(slot_);
    ASSERT_THAT(result, Eq(bootloader::flash_loader::status::ok));
}

} // namespace
