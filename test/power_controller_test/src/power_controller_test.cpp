#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "power_controller/detail/power_controller.h"
#include "spl/peripherals/gpio/mock/gpio.h"
#include "telemetry/mock/telemetry_state.h"

using namespace power_controller;
using namespace ::testing;

using power_controller_type =
    detail::power_controller<spl::peripherals::gpio::mock::gpio, telemetry::mock::telemetry_state>;

static_assert(power_controller_concept<power_controller_type>);

struct PowerControllerTest : Test {
    NiceMock<spl::peripherals::gpio::mock::gpio> bus_3v3_;
    NiceMock<spl::peripherals::gpio::mock::gpio> bus_5v_;
    NiceMock<spl::peripherals::gpio::mock::gpio> bus_12v_;
    NiceMock<spl::peripherals::gpio::mock::gpio> v3v3_l1_;
    NiceMock<spl::peripherals::gpio::mock::gpio> v3v3_l2_;
    NiceMock<spl::peripherals::gpio::mock::gpio> v3v3_l3_;
    NiceMock<spl::peripherals::gpio::mock::gpio> v3v3_l4_;
    NiceMock<spl::peripherals::gpio::mock::gpio> v5_l1_;
    NiceMock<spl::peripherals::gpio::mock::gpio> v5_l2_;
    NiceMock<spl::peripherals::gpio::mock::gpio> v12_;
    NiceMock<spl::peripherals::gpio::mock::gpio> heater_;

    power_controller::detail::configuration<spl::peripherals::gpio::mock::gpio> configuration{
        .bus_3v3 = bus_3v3_,
        .bus_5v = bus_5v_,
        .bus_12v = bus_12v_,
        .v3v3_1 = v3v3_l1_,
        .v3v3_2 = v3v3_l2_,
        .v3v3_3 = v3v3_l3_,
        .v3v3_4 = v3v3_l4_,
        .v5_1 = v5_l1_,
        .v5_2 = v5_l2_,
        .v12 = v12_,
        .heater = heater_};

    NiceMock<telemetry::mock::telemetry_state> telemetry_state_;
    telemetry::telemetry_id telemetry_id_{12};
};

TEST_F(PowerControllerTest, Enable_3v3_L1) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};
    EXPECT_CALL(bus_3v3_, set());
    EXPECT_CALL(v3v3_l1_, clear());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0x10}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0x11}));
    power_controller.enable(power_controller::output::v3v3_1);
}

TEST_F(PowerControllerTest, Enable_3v3_L2) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(bus_3v3_, set());
    EXPECT_CALL(v3v3_l2_, clear());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0x10}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0x12}));
    power_controller.enable(power_controller::output::v3v3_2);
}

TEST_F(PowerControllerTest, Enable_3v3_L3) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(bus_3v3_, set());
    EXPECT_CALL(v3v3_l3_, clear());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0x10}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0x14}));
    power_controller.enable(power_controller::output::v3v3_3);
}

TEST_F(PowerControllerTest, Enable_3v3_L4) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(bus_3v3_, set());
    EXPECT_CALL(v3v3_l4_, clear());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0x10}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0x18}));
    power_controller.enable(power_controller::output::v3v3_4);
}

TEST_F(PowerControllerTest, Enable_5v_L1) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(bus_5v_, set());
    EXPECT_CALL(v5_l1_, set());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0x01}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0x11}));
    power_controller.enable(power_controller::output::v5_1);
}

TEST_F(PowerControllerTest, Enable_5v_L2) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(bus_5v_, set());
    EXPECT_CALL(v5_l2_, set());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0x01}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0x21}));
    power_controller.enable(power_controller::output::v5_2);
}

TEST_F(PowerControllerTest, Enable12v) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(bus_12v_, set());
    EXPECT_CALL(v12_, set());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0x01}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0x41}));
    power_controller.enable(power_controller::output::v12);
}

TEST_F(PowerControllerTest, Disable3v3_L1) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(v3v3_l1_, set());
    EXPECT_CALL(bus_3v3_, clear());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0xff}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0xfe}));
    power_controller.disable(power_controller::output::v3v3_1);
}

TEST_F(PowerControllerTest, Disable3v3_L2) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(v3v3_l2_, set());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0xff}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0xfd}));
    power_controller.disable(power_controller::output::v3v3_2);
}

TEST_F(PowerControllerTest, Disable3v3_L3) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(v3v3_l3_, set());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0xff}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0xfb}));
    power_controller.disable(power_controller::output::v3v3_3);
}

TEST_F(PowerControllerTest, Disable3v3_L4) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(v3v3_l4_, set());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0xff}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0xf7}));
    power_controller.disable(power_controller::output::v3v3_4);
}

TEST_F(PowerControllerTest, Disable5v_L1) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(v5_l1_, clear());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0xff}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0xef}));
    power_controller.disable(power_controller::output::v5_1);
}

TEST_F(PowerControllerTest, Disable5v_L2) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(v5_l2_, clear());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0xff}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0xdf}));
    power_controller.disable(power_controller::output::v5_2);
}

TEST_F(PowerControllerTest, Disable12v) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(v12_, clear());
    EXPECT_CALL(bus_12v_, clear());
    EXPECT_CALL(telemetry_state_, templated_read(telemetry_id_)).WillOnce(Return(std::byte{0xff}));
    EXPECT_CALL(telemetry_state_, write_byte(telemetry_id_, std::byte{0xbf}));
    power_controller.disable(power_controller::output::v12);
}

TEST_F(PowerControllerTest, DisableWhole3v3Bus) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(bus_3v3_, clear());
    power_controller.enable(output::v3v3_1);
    power_controller.enable(output::v3v3_2);
    power_controller.enable(output::v3v3_3);
    power_controller.enable(output::v3v3_4);

    power_controller.disable(output::v3v3_1);
    power_controller.disable(output::v3v3_2);
    power_controller.disable(output::v3v3_3);
    power_controller.disable(output::v3v3_4);
}

TEST_F(PowerControllerTest, DisableWhole5vBus) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    EXPECT_CALL(bus_5v_, clear());
    power_controller.enable(output::v5_1);
    power_controller.enable(output::v5_2);

    power_controller.disable(output::v5_1);
    power_controller.disable(output::v5_2);
}

TEST_F(PowerControllerTest, DisableWhole12vBus) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};
    EXPECT_CALL(bus_12v_, clear());
    power_controller.enable(output::v12);

    power_controller.disable(output::v12);
}

TEST_F(PowerControllerTest, Is3v3_L1Enabled) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    power_controller.disable(power_controller::output::v3v3_1);
    bool res = power_controller.is_enabled(power_controller::output::v3v3_1);
    ASSERT_THAT(res, Eq(false));
    power_controller.enable(power_controller::output::v3v3_1);
    res = power_controller.is_enabled(power_controller::output::v3v3_1);
    ASSERT_THAT(res, Eq(true));
}

TEST_F(PowerControllerTest, Is3v3_L2Enabled) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    bool res = power_controller.is_enabled(power_controller::output::v3v3_2);
    ASSERT_THAT(res, Eq(false));
    power_controller.enable(power_controller::output::v3v3_2);
    res = power_controller.is_enabled(power_controller::output::v3v3_2);
    ASSERT_THAT(res, Eq(true));
}

TEST_F(PowerControllerTest, Is3v3_L3Enabled) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    bool res = power_controller.is_enabled(power_controller::output::v3v3_3);
    ASSERT_THAT(res, Eq(false));
    power_controller.enable(power_controller::output::v3v3_3);
    res = power_controller.is_enabled(power_controller::output::v3v3_3);
    ASSERT_THAT(res, Eq(true));
}

TEST_F(PowerControllerTest, Is3v3_L4Enabled) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    bool res = power_controller.is_enabled(power_controller::output::v3v3_4);
    ASSERT_THAT(res, Eq(false));
    power_controller.enable(power_controller::output::v3v3_4);
    res = power_controller.is_enabled(power_controller::output::v3v3_4);
    ASSERT_THAT(res, Eq(true));
}

TEST_F(PowerControllerTest, Is5v_L1Enabled) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    bool res = power_controller.is_enabled(power_controller::output::v5_1);
    ASSERT_THAT(res, Eq(false));
    power_controller.enable(power_controller::output::v5_1);
    res = power_controller.is_enabled(power_controller::output::v5_1);
    ASSERT_THAT(res, Eq(true));
}

TEST_F(PowerControllerTest, Is5v_L2Enabled) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    bool res = power_controller.is_enabled(power_controller::output::v5_2);
    ASSERT_THAT(res, Eq(false));
    power_controller.enable(power_controller::output::v5_2);
    res = power_controller.is_enabled(power_controller::output::v5_2);
    ASSERT_THAT(res, Eq(true));
}

TEST_F(PowerControllerTest, Is12vEnabled) {
    power_controller_type power_controller{configuration, telemetry_state_, telemetry_id_};

    bool res = power_controller.is_enabled(power_controller::output::v12);
    ASSERT_THAT(res, Eq(false));
    power_controller.enable(power_controller::output::v12);
    res = power_controller.is_enabled(power_controller::output::v12);
    ASSERT_THAT(res, Eq(true));
}
