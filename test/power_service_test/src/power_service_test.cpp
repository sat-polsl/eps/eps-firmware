#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "power_controller/mock/power_controller.h"
#include "power_service/detail/power_service.h"
#include "satos/mock/timer.h"

namespace {
using namespace power_service;
using namespace ::testing;
using namespace satos::chrono_literals;

using power_service_type =
    power_service::detail::power_service<power_controller::mock::power_controller,
                                         satos::mock::timer>;

struct PowerServiceTest : Test {
    StrictMock<power_controller::mock::power_controller> power_controller_;
};

TEST_F(PowerServiceTest, DefaultConfiguration) {
    power_service_type power_service{power_controller_};

    for (std::size_t i = 0; i < power_outputs_number; i++) {
        EXPECT_CALL(*satos::mock::timer::timers[i], initialize());
        EXPECT_CALL(*satos::mock::timer::timers[i], register_callback_inner(_));
        EXPECT_CALL(*satos::mock::timer::timers[i + power_outputs_number], initialize());
        EXPECT_CALL(*satos::mock::timer::timers[i + power_outputs_number],
                    register_callback_inner(_));
    }

    power_service.initialize(configuration{});
}

TEST_F(PowerServiceTest, EnableOnStartupSingleOutput) {
    power_service_type power_service{power_controller_};

    for (std::size_t i = 0; i < power_outputs_number; i++) {
        EXPECT_CALL(*satos::mock::timer::timers[i], initialize());
        EXPECT_CALL(*satos::mock::timer::timers[i], register_callback_inner(_));
        EXPECT_CALL(*satos::mock::timer::timers[i + power_outputs_number], initialize());
        EXPECT_CALL(*satos::mock::timer::timers[i + power_outputs_number],
                    register_callback_inner(_));
    }

    auto cfg = configuration{};
    cfg.enable_on_startup[satext::to_underlying_type(power_output::v5_1)] = true;

    EXPECT_CALL(power_controller_, enable(power_controller::output::v5_1));

    power_service.initialize(cfg);
}

TEST_F(PowerServiceTest, EnableOnStartupAllOutput) {
    power_service_type power_service{power_controller_};

    for (std::size_t i = 0; i < power_outputs_number; i++) {
        EXPECT_CALL(*satos::mock::timer::timers[i], initialize());
        EXPECT_CALL(*satos::mock::timer::timers[i], register_callback_inner(_));
        EXPECT_CALL(*satos::mock::timer::timers[i + power_outputs_number], initialize());
        EXPECT_CALL(*satos::mock::timer::timers[i + power_outputs_number],
                    register_callback_inner(_));
    }

    auto cfg = configuration{};
    for (auto&& enabled : cfg.enable_on_startup) {
        enabled = true;
    }

    EXPECT_CALL(power_controller_, enable(power_controller::output::v3v3_1));
    EXPECT_CALL(power_controller_, enable(power_controller::output::v3v3_2));
    EXPECT_CALL(power_controller_, enable(power_controller::output::v3v3_3));
    EXPECT_CALL(power_controller_, enable(power_controller::output::v3v3_4));
    EXPECT_CALL(power_controller_, enable(power_controller::output::v5_1));
    EXPECT_CALL(power_controller_, enable(power_controller::output::v5_2));
    EXPECT_CALL(power_controller_, enable(power_controller::output::v12));

    power_service.initialize(cfg);
}

TEST_F(PowerServiceTest, EnableOutput) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{});

    EXPECT_CALL(power_controller_, enable(power_controller::output::v3v3_1));

    power_service.enable(power_output::v3v3_1);
}

TEST_F(PowerServiceTest, EnableOutputLocked) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{.locked = {true}});
    power_service.enable(power_output::v3v3_1);
}

TEST_F(PowerServiceTest, EnableOutputWithWatchdog) {
    power_service_type power_service{power_controller_};
    power_service.initialize(
        configuration{.watchdog_enabled = {false, true},
                      .watchdog_timeout = {satos::chrono::seconds{}, satos::chrono::seconds{10}}});

    auto expected_timeout =
        std::chrono::duration_cast<satos::chrono::milliseconds>(satos::chrono::seconds{10});

    EXPECT_CALL(power_controller_, enable(power_controller::output::v3v3_2));
    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_2)],
                start_duration(expected_timeout));

    power_service.enable(power_output::v3v3_2);
}

TEST_F(PowerServiceTest, DisableOutput) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{});

    EXPECT_CALL(power_controller_, disable(power_controller::output::v5_2));
    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v5_2)],
                stop());

    power_service.disable(power_output::v5_2);
}

TEST_F(PowerServiceTest, DisableOutputLocked) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{.locked = {false, false, true}});
    power_service.disable(power_output::v3v3_3);
}

TEST_F(PowerServiceTest, KickEnabledWatchdog) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{.watchdog_enabled = {false, false, false, true},
                                           .watchdog_timeout{satos::chrono::seconds{},
                                                             satos::chrono::seconds{},
                                                             satos::chrono::seconds{},
                                                             satos::chrono::seconds{100}}});

    auto expected_timeout =
        std::chrono::duration_cast<satos::chrono::milliseconds>(satos::chrono::seconds{100});

    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_4)],
                is_running())
        .WillOnce(Return(true));
    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_4)],
                start_duration(expected_timeout));

    power_service.kick_watchdog(power_output::v3v3_4);
}

TEST_F(PowerServiceTest, KickDisabledWatchdog) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{});

    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v12)],
                is_running())
        .Times(0);
    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v12)],
                start_duration(_))
        .Times(0);

    power_service.kick_watchdog(power_output::v12);
}

TEST_F(PowerServiceTest, EnableWatchdog) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{.watchdog_timeout = {satos::chrono::seconds{10}}});
    power_service.enable_watchdog(power_output::v3v3_1);

    auto expected_timeout =
        std::chrono::duration_cast<satos::chrono::milliseconds>(satos::chrono::seconds{10});

    EXPECT_CALL(power_controller_, enable(power_controller::output::v3v3_1));
    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1)],
                start_duration(expected_timeout));

    power_service.enable(power_output::v3v3_1);
}

TEST_F(PowerServiceTest, EnableLockedWatchdog) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{.locked = {true}});
    power_service.enable_watchdog(power_output::v3v3_1);

    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1)],
                is_running())
        .Times(0);
    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1)],
                start_duration(_))
        .Times(0);

    power_service.kick_watchdog(power_output::v3v3_1);
}

TEST_F(PowerServiceTest, DisableWatchdog) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{.watchdog_enabled = {true}});
    power_service.disable_watchdog(power_output::v3v3_1);

    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1)],
                is_running())
        .Times(0);
    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1)],
                start_duration(_))
        .Times(0);

    power_service.kick_watchdog(power_output::v3v3_1);
}

TEST_F(PowerServiceTest, DisableLockedWatchdog) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{.locked = {true},
                                           .watchdog_enabled = {true},
                                           .watchdog_timeout = {satos::chrono::seconds{10}}});
    power_service.disable_watchdog(power_output::v3v3_1);

    auto expected_timeout =
        std::chrono::duration_cast<satos::chrono::milliseconds>(satos::chrono::seconds{10});

    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1)],
                is_running())
        .WillOnce(Return(true));
    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1)],
                start_duration(expected_timeout));

    power_service.kick_watchdog(power_output::v3v3_1);
}

TEST_F(PowerServiceTest, SetWatchdogiTimeout) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{.watchdog_enabled = {true},
                                           .watchdog_timeout = {satos::chrono::seconds{10}}});
    power_service.set_watchdog_timeout(power_output::v3v3_1, satos::chrono::seconds{100});

    auto expected_timeout =
        std::chrono::duration_cast<satos::chrono::milliseconds>(satos::chrono::seconds{100});

    EXPECT_CALL(power_controller_, enable(power_controller::output::v3v3_1));
    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1)],
                start_duration(expected_timeout));

    power_service.enable(power_output::v3v3_1);
}

TEST_F(PowerServiceTest, SetWatchdogiTimeoutLocked) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{.locked = {true},
                                           .watchdog_enabled = {true},
                                           .watchdog_timeout = {satos::chrono::seconds{10}}});
    power_service.set_watchdog_timeout(power_output::v3v3_1, satos::chrono::seconds{100});

    auto expected_timeout =
        std::chrono::duration_cast<satos::chrono::milliseconds>(satos::chrono::seconds{10});

    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1)],
                is_running())
        .WillOnce(Return(true));
    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1)],
                start_duration(expected_timeout));

    power_service.kick_watchdog(power_output::v3v3_1);
}

TEST_F(PowerServiceTest, WatchdogCallback) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{});

    EXPECT_CALL(power_controller_, disable(power_controller::output::v3v3_1));
    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1) +
                                            power_outputs_number],
                start_duration(1000_ms));

    satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1)]
        ->callbacks.front()();
}

TEST_F(PowerServiceTest, EnableCallback) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{});

    EXPECT_CALL(power_controller_, enable(power_controller::output::v3v3_1));

    satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1) +
                               power_outputs_number]
        ->callbacks.front()();
}

TEST_F(PowerServiceTest, EnableCallbackWithWatchdog) {
    power_service_type power_service{power_controller_};
    power_service.initialize(configuration{.watchdog_enabled = {true},
                                           .watchdog_timeout = {satos::chrono::seconds{100}}});

    auto expected_timeout =
        std::chrono::duration_cast<satos::chrono::milliseconds>(satos::chrono::seconds{100});

    EXPECT_CALL(power_controller_, enable(power_controller::output::v3v3_1));
    EXPECT_CALL(*satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1)],
                start_duration(expected_timeout));

    satos::mock::timer::timers[satext::to_underlying_type(power_output::v3v3_1) +
                               power_outputs_number]
        ->callbacks.front()();
}

} // namespace
