#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/adc/mock/adc.h"
#include "telemetry/mock/telemetry_state.h"
#include "telemetry/sampler_concept.h"
#include "telemetry/samplers/adc_sampler.h"

namespace {
using namespace testing;
using namespace telemetry::samplers;

using voltage_adc = devices::adc::mock::adc<7>;
using current_adc = devices::adc::mock::adc<12>;

constexpr auto config =
    telemetry::samplers::adc_sampler_config{.v3v3_l1 = telemetry::telemetry_id{0},
                                            .i3v3_l1 = telemetry::telemetry_id{1},
                                            .v3v3_l2 = telemetry::telemetry_id{2},
                                            .i3v3_l2 = telemetry::telemetry_id{3},
                                            .v3v3_l3 = telemetry::telemetry_id{4},
                                            .i3v3_l3 = telemetry::telemetry_id{5},
                                            .v3v3_l4 = telemetry::telemetry_id{6},
                                            .i3v3_l4 = telemetry::telemetry_id{7},
                                            .v5v_l1 = telemetry::telemetry_id{8},
                                            .i5v_l1 = telemetry::telemetry_id{9},
                                            .v5v_l2 = telemetry::telemetry_id{10},
                                            .i5v_l2 = telemetry::telemetry_id{11},
                                            .v12v = telemetry::telemetry_id{12},
                                            .i12v = telemetry::telemetry_id{13},
                                            .vbms = telemetry::telemetry_id{14},
                                            .ibms = telemetry::telemetry_id{15},
                                            .vmppt = telemetry::telemetry_id{16},
                                            .imppt1 = telemetry::telemetry_id{17},
                                            .imppt2 = telemetry::telemetry_id{18}};

using adc_sampler = telemetry::samplers::
    adc_sampler<voltage_adc, current_adc, telemetry::mock::telemetry_state, config>;

static_assert(telemetry::sampler_concept<adc_sampler>);

struct AdcSamplerTest : public Test {
    StrictMock<voltage_adc> voltage_adc_;
    StrictMock<current_adc> current_adc_;
    StrictMock<telemetry::mock::telemetry_state> state_;
};

TEST_F(AdcSamplerTest, SampleSucces) {
    adc_sampler sampler{voltage_adc_, current_adc_};

    EXPECT_CALL(voltage_adc_, read_all())
        .WillOnce(Return(std::array<std::uint16_t, 7>{0, 1, 2, 3, 4, 5, 6}));
    EXPECT_CALL(current_adc_, read_all())
        .WillOnce(
            Return(std::array<std::uint16_t, 12>{7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18}));

    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{12}, 0));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{8}, 1));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{0}, 2));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{10}, 3));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{2}, 4));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{4}, 5));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{6}, 6));

    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{15}, 7));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{18}, 8));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{17}, 9));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{14}, 10));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{16}, 11));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{13}, 12));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{9}, 13));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{11}, 14));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{3}, 15));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{1}, 16));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{7}, 17));
    EXPECT_CALL(state_, write_u16(telemetry::telemetry_id{5}, 18));

    sampler(state_);
}

TEST_F(AdcSamplerTest, SampleFailure) {
    adc_sampler sampler{voltage_adc_, current_adc_};

    EXPECT_CALL(voltage_adc_, read_all())
        .WillOnce(Return(satext::unexpected{spl::drivers::spi::status::timeout}));
    EXPECT_CALL(current_adc_, read_all())
        .WillOnce(Return(satext::unexpected{spl::drivers::spi::status::timeout}));

    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{12}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{8}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{0}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{10}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{2}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{4}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{6}, std::monostate{}));

    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{15}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{18}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{17}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{14}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{16}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{13}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{9}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{11}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{3}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{1}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{7}, std::monostate{}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{5}, std::monostate{}));

    sampler(state_);
}

} // namespace
