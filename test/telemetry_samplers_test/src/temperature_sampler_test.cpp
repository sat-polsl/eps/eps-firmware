#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/lm75b/mock/lm75b.h"
#include "telemetry/mock/telemetry_state.h"
#include "telemetry/sampler_concept.h"
#include "telemetry/samplers/temperature_sampler.h"

namespace {
using namespace testing;
using namespace telemetry::samplers;

using sampler = telemetry::samplers::temperature_sampler<devices::lm75b::mock::lm75b,
                                                         telemetry::mock::telemetry_state,
                                                         telemetry::telemetry_id{2}>;

static_assert(telemetry::sampler_concept<sampler>);

struct TemperatureSamplerTest : Test {
    StrictMock<telemetry::mock::telemetry_state> state_;
    StrictMock<devices::lm75b::mock::lm75b> temperature_sensor_;
};

TEST_F(TemperatureSamplerTest, SampleTemperatureSuccess) {
    sampler temperature_sampler{temperature_sensor_};

    EXPECT_CALL(temperature_sensor_, read_temperature()).WillOnce(Return(1234));
    EXPECT_CALL(state_, write_i16(telemetry::telemetry_id{2}, 1234));

    temperature_sampler(state_);
}

TEST_F(TemperatureSamplerTest, SampleTemperatureFailure) {
    sampler temperature_sampler{temperature_sensor_};

    EXPECT_CALL(temperature_sensor_, read_temperature())
        .WillOnce(Return(satext::unexpected{spl::drivers::i2c::status::timeout}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{2}, std::monostate{}));

    temperature_sampler(state_);
}

} // namespace
