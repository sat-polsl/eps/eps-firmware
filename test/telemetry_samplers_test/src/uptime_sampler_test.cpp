#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "satos/chrono.h"
#include "satos/clock.h"
#include "satos/mock/clock.h"
#include "telemetry/mock/telemetry_state.h"
#include "telemetry/sampler_concept.h"
#include "telemetry/samplers/uptime_sampler.h"

namespace {
using namespace testing;
using namespace telemetry::samplers;

using sampler = telemetry::samplers::uptime_sampler<telemetry::mock::telemetry_state,
                                                    telemetry::telemetry_id{12}>;

static_assert(telemetry::sampler_concept<sampler>);

struct UptimeSamplerTest : Test {
    satos::mock::clock& clock_{satos::mock::clock::instance()};
    StrictMock<telemetry::mock::telemetry_state> state_;
};

TEST_F(UptimeSamplerTest, SampleUptime) {
    sampler uptime_sampler;

    EXPECT_CALL(clock_, now())
        .WillOnce(Return(satos::clock::time_point{satos::chrono::milliseconds{1234}}));
    EXPECT_CALL(state_, write_u32(telemetry::telemetry_id{12}, 1234));

    uptime_sampler(state_);
}

} // namespace
