import argparse
from struct import pack
from os.path import getsize
from zlib import crc32

MAGIC_A = 0x746f6f62
MAGIC_B = 0x746f6c73


def main(input_path: str, output_path: str) -> None:
    with open(input_path, "rb") as input_file, open(output_path, "wb") as output_file:
        data = input_file.read()
        header = pack("<IIII", MAGIC_A, MAGIC_B, crc32(data), getsize(input_path))
        output_file.write(header)
        output_file.write(data)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', help='input file')
    parser.add_argument('output_file', help='output file')
    args = parser.parse_args()
    main(args.input_file, args.output_file)
